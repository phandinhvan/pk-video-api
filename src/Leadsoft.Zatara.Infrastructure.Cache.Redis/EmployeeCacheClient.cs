﻿using Base.Infrastructure.Cache.Redis;
using StackExchange.Redis;

namespace Leadsoft.Zatara.Infrastructure.Cache.Redis
{
    public class EmployeeCacheClient : BaseRedisCacheClient
    {
        public EmployeeCacheClient(ConnectionMultiplexer _redis) :
            base(_redis)
        {
        }

        //public async Task SetExchangeAuthAsync(ExchangeAuth exchangeAuth, TimeSpan? expiry = null)
        //{
        //    var db = GetDatabase();
        //    string key = GetKey(exchangeAuth);
        //    try
        //    {
        //        exchangeAuth.ApiKey = string.Empty;
        //        exchangeAuth.ApiSecret = string.Empty;
        //        await db.StringSetAsync(key, JsonConvert.SerializeObject(exchangeAuth), expiry);
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //}

        //public async Task DeleteExchangeAuthAsync()
        //{
        //}
        //public async Task<ExchangeAuth> GetExchangeAuthAsync(int userId, int exchangeId)
        //{
        //}
    }
}