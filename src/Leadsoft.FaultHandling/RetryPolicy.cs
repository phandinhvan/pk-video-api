﻿using Polly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Leadsoft.FaultHandling
{
    public class RetryPolicy
    {
        public static void DoRetry(Action action, int numberOfRetry)
        {
            var policy = Policy
                        .Handle<Exception>()
                        .WaitAndRetry(numberOfRetry, i =>
                        {
                            return TimeSpan.FromSeconds(2);
                        });

            policy.Execute(action);
        }

        public static T DoRetry<T>(Func<T> func, int numberOfRetry, int waitSeconds = 2)
        {
            var policy = Policy
                        .Handle<Exception>()
                        .WaitAndRetry(numberOfRetry, i =>
                        {
                            return TimeSpan.FromSeconds(waitSeconds);
                        });

            return policy.Execute<T>(func);
        }

        public static async Task DoRetryAsync(Func<Task> action, int numberOfRetry)
        {
            var policy = Policy
                        .Handle<Exception>()
                        .WaitAndRetryAsync(numberOfRetry, i =>
                        {
                            return TimeSpan.FromSeconds(2);
                        });

            await policy.ExecuteAsync(action);
        }

        public static async Task<T> DoRetryAsync<T>(Func<Task<T>> func, int numberOfRetry)
        {
            var policy = Policy
                        .Handle<Exception>()
                        .WaitAndRetryAsync(numberOfRetry, i =>
                        {
                            return TimeSpan.FromSeconds(2);
                        });

            return await policy.ExecuteAsync<T>(func);
        }
    }
}
