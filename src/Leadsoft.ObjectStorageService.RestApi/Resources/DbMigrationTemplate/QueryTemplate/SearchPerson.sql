﻿#SELECT#
FROM Persons WITH (NOLOCK)
LEFT JOIN dbo.PersonStatus PersonStatus ON Persons.PersonStatus = PersonStatus.Id           
LEFT JOIN (SELECT pm.ObjectID, MAX(pm.Value) AS Value FROM PersonMult pm WITH (NOLOCK) WHERE pm.Main = 1 AND pm.FieldType = 6 GROUP BY pm.ObjectID) perMult ON perMult.ObjectID = Persons.PersonID
LEFT JOIN Categories Categories WITH (NOLOCK) ON perMult.Value = Categories.ID
#EXTENDJOIN#
WHERE 1=1 
	#CONDITION#
#ORDERBY#
