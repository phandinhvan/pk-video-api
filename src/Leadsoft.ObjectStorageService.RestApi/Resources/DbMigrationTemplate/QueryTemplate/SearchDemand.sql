﻿#SELECT#
FROM Demands WITH (NOLOCK)
LEFT JOIN Persons WITH (NOLOCK) ON Demands.PID = Persons.PersonID
-- to exlude CRE object
LEFT OUTER JOIN CREDemands WITH (NOLOCK) ON Demands.DemandID = CREDemands.DemandID
#EXTENDJOIN#
WHERE CREDemands.DemandID IS NULL
	#CONDITION#
#ORDERBY#
