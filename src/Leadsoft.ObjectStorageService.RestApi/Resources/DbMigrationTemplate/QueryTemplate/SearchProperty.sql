﻿#SELECT#
FROM Objects
LEFT JOIN Users ON Objects.[User] = Users.UserID
LEFT JOIN TypeOfMandate ON Objects.ContractType = TypeOfMandate.ID
LEFT JOIN SubStatus ON Objects.SubStatus = SubStatus.ID
LEFT JOIN [Status] ON Objects.Status = Status.ID
LEFT JOIN Users uAgent ON Objects.Getter = uAgent.UserID
LEFT JOIN v_ObjectCreatedDate v_c ON Objects.ID = v_c.ObjectID
LEFT JOIN v_PropertyBuyers pBuyers ON Objects.ID = pBuyers.ID
LEFT JOIN v_PropertyRenters pRenters ON Objects.ID = pRenters.ID
LEFT JOIN Users uSigner ON Objects.Signer = uSigner.UserID
-- to exlude CRE object
LEFT JOIN CREProperty ON Objects.ID = dbo.CREProperty.ObjectId
#EXTENDJOIN#
WHERE CREProperty.ObjectId IS NULL 
	#CONDITION#
#ORDERBY#
