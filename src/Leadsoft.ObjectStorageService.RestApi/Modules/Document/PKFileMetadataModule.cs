﻿using Leadsoft.Zatara.Entities;
using Nancy.Swagger;
using Nancy.Swagger.Services;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class PKFileMetadataModule : BaseResourceMetadataModule<PKFile>
    {
        public PKFileMetadataModule(ISwaggerModelCatalog modelCatalog, ISwaggerTagCatalog tagCatalog)
            : base("pkvideos", modelCatalog, tagCatalog)
        {
        }
    }
}