﻿using Leadsoft.Zatara.Entities;
using Nancy.Swagger;
using Nancy.Swagger.Services;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class ObjectBucketMetadataModule : BaseResourceMetadataModule<ObjectBucket>
    {
        public ObjectBucketMetadataModule(ISwaggerModelCatalog modelCatalog, ISwaggerTagCatalog tagCatalog)
            : base("objectbuckets", modelCatalog, tagCatalog)
        {
        }
    }
}