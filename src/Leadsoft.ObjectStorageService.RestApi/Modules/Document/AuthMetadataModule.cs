﻿using Base.Interface.Dto;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using Nancy.Swagger;
using Nancy.Swagger.Services;
using Nancy.Swagger.Services.RouteUtils;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class AuthMetadataModule : BaseResourceMetadataModule<LoginRequest>
    {
        public AuthMetadataModule(ISwaggerModelCatalog modelCatalog, ISwaggerTagCatalog tagCatalog)
            : base("auth", modelCatalog, tagCatalog)
        {

            CreateDocument<TaskResult<LoginResponse>>("LocalLogin", "", $@"LocalLogin", new[]
           {
                 new HttpResponseMetadata<TaskResult<LoginResponse>> {Code = 200, Message = "OK"}
             }, new[]
           {
                 new BodyParameter<LoginRequest>(ModelCatalog) {Name = "Login request",  Required = true, Description = $@"Login request"}
             });
        }
    }
}