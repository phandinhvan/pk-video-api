﻿using Base.Interface.Dto;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using Nancy.Swagger;
using Nancy.Swagger.Services;
using Nancy.Swagger.Services.RouteUtils;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class UserMetadataModule : BaseResourceMetadataModule<LoginRequest>
    {
        public UserMetadataModule(ISwaggerModelCatalog modelCatalog, ISwaggerTagCatalog tagCatalog)
            : base("users", modelCatalog, tagCatalog)
        {

            CreateDocument<TaskResult<UserDto>>("me", "", $@"me", new[]
            {
                 new HttpResponseMetadata<TaskResult<UserDto>> {Code = 200, Message = "OK"}
            });
        }
    }
}