﻿using Base.Interface.Dto;
using Leadsoft.RestApi.NetCorePlatform.Document;
using Nancy.Swagger;
using Nancy.Swagger.Services;
using Nancy.Swagger.Services.RouteUtils;
using Swagger.ObjectModel;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public abstract class BaseResourceMetadataModule<T> : BaseMetadataModule<T> where T : class
    {
        public BaseResourceMetadataModule(string resorceName, ISwaggerModelCatalog modelCatalog, ISwaggerTagCatalog tagCatalog)
            : base(resorceName, modelCatalog, tagCatalog)
        {
            RouteDescriber.AddAdditionalModels(typeof(T));

            //CreateDefaultDocument();
        }

        protected virtual void CreateDefaultDocument()
        {
            CreateAddDocument();
            CreateDeleteDocument();
            CreateGetListDocument();
            CreateGetDocument();
            CreateUpdateDocument();
        }

        protected virtual void CreateAddDocument()
        {
            CreateDocument<TaskResult<int>>("Add", "", $@"Add", new[]
           {
                 new HttpResponseMetadata<TaskResult<int>> {Code = 200, Message = "OK"}
             }, new[]
           {
                 new BodyParameter<T>(ModelCatalog) {Name = "Data",  Required = true, Description = $@"Data"}
             });
        }

        protected virtual void CreateDeleteDocument()
        {
            CreateDocument<TaskResult<bool>>("Delete", "", $@"Delete", new[]
           {
                 new HttpResponseMetadata<TaskResult<bool>> {Code = 200, Message = "OK"}
             }, new[]
           {
                 new Parameter{Name = "id", In = ParameterIn.Path, Required = true, Description = "id", Default = "1", Type = "string" },
             });
        }

        protected virtual void CreateGetListDocument()
        {
            CreateDocument<PagingResult<T>>("GetList", "", $@"Get all", new[]
            {
                new HttpResponseMetadata<PagingResult<T>> {Code = 200, Message = "OK"}
            }, new[]
            {
                new Parameter{Name = "pagesize", In = ParameterIn.Query, Required = false, Description = "Page size", Default = "30", Type = "string" },
                new Parameter{Name = "pageindex", In = ParameterIn.Query, Required = false, Description = "Page index", Default = "1", Type = "string" },
            });
        }

        protected virtual void CreateGetDocument()
        {
            CreateDocument<TaskResult<T>>("Get", "", $@"Get", new[]
            {
                new HttpResponseMetadata<TaskResult<T>> {Code = 200, Message = "OK"}
            }, new[]
            {
                new Parameter{Name = "id", In = ParameterIn.Path, Required = true, Description = "id", Default = "1", Type = "string" },
            });
        }

        protected virtual void CreateUpdateDocument()
        {
            CreateDocument<TaskResult<bool>>("Put", "", $@"Put", new[]
            {
                new HttpResponseMetadata<TaskResult<bool>> {Code = 200, Message = "OK"}
            }, new[]
            {
                new Parameter{Name = "id", In = ParameterIn.Path, Required = true, Description = "id", Default = "1", Type = "string" },
                new BodyParameter<T>(ModelCatalog) {Name = "Data",  Required = true, Description = $@"Data"}
            });
        }
    }
}