using Nancy;
using Serilog;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
            Get("/", _ => GetPetStoreUrl(), null, "Home");
        }
        private Response GetPetStoreUrl()
        {
            var port = Request.Url.Port ?? 80;
            //return Response.AsRedirect($"http://petstore.swagger.io/?url=http://localhost:{port}/api-docs");
            string url = Request.Url;
            if (Request.Url.Port != null && url.EndsWith(Request.Url.Port.ToString()) == false)
                url = $@"{url}:{Request.Url.Port}";
            //return new RedirectResponse(Context.ToFullPath("~/"));
            string path = $@"{url}/api-docs";
            Log.Information($@"PATH = {path}");

            return Response.AsRedirect($@"http://petstore.swagger.io/?url={path}");
            //return Response.AsText("Home");
        }
    }
}