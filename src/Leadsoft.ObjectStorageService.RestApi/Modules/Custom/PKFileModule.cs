using System.Threading.Tasks;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using Nancy.ModelBinding;
using System.Text.RegularExpressions;
using Nancy.Responses;
using Nancy;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Common;
using System.IO;
using Base.Interface.Dto;
using Leadsoft.RestApi.NetCorePlatform;
using Base.Services.Interface;
using Nancy.Security;
using System;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class PKFileModule : BaseModule
    {
        private readonly IPKFileService _pkFileService;

        public PKFileModule(LeadsoftUserContext userContext, IPKFileService pkFileService)
            : base("pkvideos", userContext, false)
        {
            _pkFileService = pkFileService;

            Post("/", AddChunkObjectAsync, null, "AddChunkObject");
            Put("/{id}", PutAsync, null, "UpdateObjectInfo");
            Get("/{fileid}", GetObjectAsync, null, "GetObject");
            Get("/{fileid}/info", GetFileInfoAsync, null, "GetFileInfo");

            Get("/{fileid}/comments", GetCommentAsync, null, "GetComment");
            Get("/", GetListAsync, null, "GetList");

            Delete("/{id}", DeleteAsync, null, "Delete");
            Post("/sharelink", ShareLinkAsync, null, "ShareLink");

            Get("/style-file-list", GetListManageStyleFileAsync, null, "GetListManageStyle");
            Get("/list-video-manage-style", GetListVideoManageStyleFileAsync, null, "GetListVideoManageStyle");
            Get("/project-list", GetProjectListAsync, null, "GetProjectList");
            Get("/list-item-project", GetProjectListItemAsync, null, "GetProjectListItem");

            Post("/more-video", AddMoreVideoAsync, null, "AddMoreVideo");
            Get("/new-video", GetNewVideoAsync, null, "GetNewVideo");
        }

        public async Task<object> PutAsync(dynamic parameters)
        {
            this.RequiresAuthentication();

            //check authorization
            var request = this.Bind<UpdateFileRequest>();

            var id = parameters.id;
            request.fileid = id;
            // var id = request.FILEID;
            var dtoResult = await _pkFileService.GetDtoAsync(id);
            if(dtoResult.IsSuccess  == false)
                return new FailTaskResult<string>($@"File id {id} not found");

            if(_userContext == null || _userContext.UserName != dtoResult.Result.UPLOADID){
                return new FailTaskResult<string>($@"You do not have permission to update this file");
            }

            return await _pkFileService.UpdateAsync(request);
        }

        public async Task<object> DeleteAsync(dynamic parameters)
        {
            this.RequiresAuthentication();

                  var id = parameters.id;
            // var id = request.FILEID;
            var dtoResult = await _pkFileService.GetDtoAsync(id);
            if(dtoResult.IsSuccess  == false)
                return new FailTaskResult<string>($@"File id {id} not found");

            if(dtoResult.Result.UPLOADID != _userContext.UserName)
                return new FailTaskResult<string>($@"Ypu do not have permission to delete this file");

            return await _pkFileService.DeleteAsync(id);
        }

        public async Task<object> ShareLinkAsync(dynamic parameters)
        {
            this.RequiresAuthentication();

            var request = this.Bind<ShareLinkRequest>();

            return await _pkFileService.ShareLinkAsync(request);
        }

        public async Task<object> GetCommentAsync(dynamic parameters)
        {
            var pagingRequest = GetPagingRequest();

            var fileId = parameters.fileid;

            var newRequest = pagingRequest.CloneTo<GetCommentPagingRequest>();
            newRequest.FileId = fileId;

            return await _pkFileService.GetListCommentPagingAsync(newRequest);
        }

        public async Task<object> GetListAsync(dynamic parameters)
        {
            //this.RequiresAuthentication();

            var pagingRequest = GetPagingRequest();

            var fileName = GetQueryParameter<string>("filename");

            //specify case
            if(string.IsNullOrWhiteSpace(fileName) == false && fileName.ToUpper() == "FILENAME")
                fileName = string.Empty;


            var companyCode = GetQueryParameter<string>("companycode");
            var departmentCode = GetQueryParameter<string>("departmentcode");
            var isLoadAll = GetQueryParameter<bool>("isloadall");
            //var afterday = GetQueryParameter<int>("afterday");
            var startDate = GetQueryParameter<string>("start_date");
            var endDate = GetQueryParameter<string>("end_date");
            var catalogue = GetQueryParameter<string>("catalogue");
            var category = GetQueryParameter<string>("categorycode");

            var newRequest = pagingRequest.CloneTo<GetVideoPagingRequest>();

            newRequest.FileName = fileName;
            newRequest.CompanyCode = companyCode;
            newRequest.DepartmentCode = departmentCode;
            //newRequest.AfterDay = afterday;
            newRequest.StartDate = startDate;
            newRequest.EndDate = endDate;
            newRequest.Catalogue = catalogue;
            newRequest.Category = category;

            if (isLoadAll == false)
                newRequest.UserId = _userContext.UserName;

            return await _pkFileService.GetListPagingAsync(newRequest);
        }

        

        public async Task<object> AddChunkObjectAsync(dynamic parameters)
        {
            this.RequiresAuthentication();

            var contentTypeRegex = new Regex("^multipart/form-data;\\s*boundary=(.*)$", RegexOptions.IgnoreCase);
            //System.IO.Stream fileStream = null;

            if (contentTypeRegex.IsMatch(this.Request.Headers.ContentType))//Yes, it is multi part
            {
                var boundary = contentTypeRegex.Match(this.Request.Headers.ContentType).Groups[1].Value;
                var multipart = new HttpMultipart(this.Request.Body, boundary);
                //bodyStream = multipart.GetBoundaries().First(b => b.ContentType.Equals("application/json")).Value;
                var boundaries = multipart.GetBoundaries();

                AddChunkObjectStorageRequest request = new AddChunkObjectStorageRequest();
                foreach (HttpMultipartBoundary param in boundaries)
                {
                    if (string.IsNullOrWhiteSpace(param.Name))
                        continue;

                    if (param.Name.ToUpper() == "FILE")
                    {
                        request.DataStream = param.Value;
                    }
                    else if (param.Name.ToUpper() == "KEY")//unique key for each file
                    {
                        request.Key = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "FILENAME")
                    {
                        request.FileName = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "TITLE")
                    {
                        request.Title = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "TOTALPART")
                    {
                        var intText = StringHelper.GetStringFromStream(param.Value);
                        request.TotalPart = ConvertHelper.GetInt(intText);
                    }
                    else if (param.Name.ToUpper() == "PARTINDEX")
                    {
                        var intText = StringHelper.GetStringFromStream(param.Value);
                        request.PartIndex = ConvertHelper.GetInt(intText);
                    }
                    else if (param.Name.ToUpper() == "COMPANYCODE")
                    {
                        request.CompanyCode = StringHelper.GetStringFromStream(param.Value);
                    }else if (param.Name.ToUpper() == "TEAMCODE")
                    {
                        request.TeamCode = StringHelper.GetStringFromStream(param.Value);
                    }else if (param.Name.ToUpper() == "DESCRIPTION")
                    {
                        request.Description = StringHelper.GetStringFromStream(param.Value);
                    }else if (param.Name.ToUpper() == "PRIVACY")
                    {
                        var privacyText = StringHelper.GetStringFromStream(param.Value);
                        if(string.IsNullOrWhiteSpace(privacyText) == false)
                        {
                            privacyText = privacyText.ToUpper();
                            if(privacyText.Contains("PUBLIC"))
                                request.IsPublic = true;
                            else if(privacyText.Contains("PRIVATE"))
                                request.IsPrivated = true;
                            else if(privacyText.Contains("DEPARTMENT"))
                                request.IsOnlyForDepartment = true;
                        }
                    }else if (param.Name.ToUpper() == "ISONLYFORDEPARTMENT")
                    {
                        var isOnlyForDepartmentText = StringHelper.GetStringFromStream(param.Value);
                        request.IsOnlyForDepartment = ConvertHelper.GetBool(isOnlyForDepartmentText);
                    }else if (param.Name.ToUpper() == "ISPRIVATED")
                    {
                        var isPrivatedText = StringHelper.GetStringFromStream(param.Value);
                        request.IsPrivated = ConvertHelper.GetBool(isPrivatedText);
                    }else if (param.Name.ToUpper() == "CATALOGUE")
                    {
                        request.Catalogue = StringHelper.GetStringFromStream(param.Value);
                    }else if (param.Name.ToUpper() == "CATEGORY")
                    {
                        request.Category = StringHelper.GetStringFromStream(param.Value);
                    }
                }

                return await _pkFileService.AddChunkObjectAsync(request);
            }
            else//ignore
            {
            }

            return false;
        }

        public async Task<object> GetFileInfoAsync(dynamic parameters)
        {
            var fileid = parameters.fileid;
            var dtoResult = await _pkFileService.GetDtoAsync(fileid);
            if (dtoResult == null || dtoResult.IsSuccess == false)
                return new FailTaskResult<string>($@"File id {fileid} not found");

            //check permission
            if (_userContext == null || _userContext.IsValid() == false)
            {
               if (dtoResult.Result != null && dtoResult.Result.PUBLICHCHECK != "Y")
                   return new FailTaskResult<string>($@"You have not permission to access this content");
            }

            return dtoResult;
        }
        public async Task<object> GetObjectAsync(dynamic parameters)
        {
            string fileId = parameters.fileid;

            if(string.IsNullOrWhiteSpace(fileId))
                return new FailTaskResult<string>("Empty file id");

            //in SAFARY, we must add .mp4 extension to the url
            //We should remove it first
            fileId = Path.GetFileNameWithoutExtension(fileId);//remove filename extension

            var fileInfoResult = await _pkFileService.GetFileInfoByIdAsync(fileId);
            if (fileInfoResult == null || fileInfoResult.IsSuccess == false)
                return new FailTaskResult<string>($@"File id {fileId} not found");

            //check permission
            // if (_userContext == null || _userContext.IsValid() == false)
            // {
            //    if (fileInfoResult.Result != null && fileInfoResult.Result.PUBLICHCHECK != "Y")
            //        return new FailTaskResult<string>($@"You have not permission to access this content");
            // }
            //var result = await _objectStorageService.GetObjectByKeyAsync(bucketKey, objectkey);
            //if (result.IsSuccess == false)
            //    return result;

            var fileDataResult = await _pkFileService.GetFileDataByIdAsync(fileId);
            if (fileDataResult == null || fileDataResult.IsSuccess == false)
                return new FailTaskResult<string>($@"File data  not found");

            var mimeType = "application/octet-stream";
            if (string.IsNullOrWhiteSpace(fileInfoResult.Result.CONTENTTYPE) == false)
                mimeType = fileInfoResult.Result.CONTENTTYPE;

            // var response = new StreamResponse(() => result.Result, mimeType);
            // return response.WithContentType(mimeType);
            return Response.FromPartialStream(this.Request, fileDataResult.Result, mimeType);
        }

        /*----- Code by Dinh Van -----*/
        public async Task<object> GetListManageStyleFileAsync(dynamic parameters)
        {
            this.RequiresAuthentication();

            var pagingRequest = GetPagingRequest();

            var buyer = GetQueryParameter<string>("buyer");

            ////specify case
            if (string.IsNullOrWhiteSpace(buyer) == false && buyer.ToUpper() == "BUYER")
                buyer = string.Empty;

            var fileName = GetQueryParameter<string>("filename");
            //var isLoadAll = GetQueryParameter<bool>("isloadall");hu
            var afterday = GetQueryParameter<int>("afterday");

            var newRequest = pagingRequest.CloneTo<GetVideoManageStylePagingRequest>();

            newRequest.Buyer = buyer;
            newRequest.FileName = fileName;
            newRequest.AfterDay = afterday;

            //if (isLoadAll == false)
            //    newRequest.UserId = _userContext.UserName;

            return await _pkFileService.GetListManageStylePagingAsync(newRequest);
        }

        /*----- Code by Dinh Van -----*/
        public async Task<object> GetListVideoManageStyleFileAsync(dynamic parameters)
        {
            this.RequiresAuthentication();

            var pagingRequest = GetPagingRequest();

            //var fileName = GetQueryParameter<string>("filename");

            ////specify case
            //if (string.IsNullOrWhiteSpace(fileName) == false && fileName.ToUpper() == "FILENAME")
            //    fileName = string.Empty;


            var companyCode = GetQueryParameter<string>("companycode");
            var departmentCode = GetQueryParameter<string>("departmentcode");
            var styleCode = GetQueryParameter<string>("stylecode");
            var styleSize = GetQueryParameter<string>("stylesize");
            var styleColorSerial = GetQueryParameter<string>("stylecolorserial");
            //var isLoadAll = GetQueryParameter<bool>("isloadall");
            var afterday = GetQueryParameter<int>("afterday");

            var newRequest = pagingRequest.CloneTo<GetListVideoManageStylePagingRequest>();

            newRequest.CompanyCode = companyCode;
            newRequest.DepartmentCode = departmentCode;
            newRequest.StyleCode = styleCode;
            newRequest.StyleSize = styleSize;
            newRequest.StyleColorSerial = styleColorSerial;
            newRequest.AfterDay = afterday;

            //if (isLoadAll == false)
            //    newRequest.UserId = _userContext.UserName;

            return await _pkFileService.GetListVideoManageStyleAsync(newRequest);
        }

        public async Task<object> GetProjectListAsync (dynamic parameters)
        {
            this.RequiresAuthentication();

            var pagingRequest = GetPagingRequest();

            var labCode = GetQueryParameter<string>("labcode");
            var labRoomName = GetQueryParameter<string>("labroomname");
            var startDate = GetQueryParameter<string>("start_date");
            var endDate = GetQueryParameter<string>("end_date");
            var useStatus = GetQueryParameter<string>("usestatus");

            var newRequest = pagingRequest.CloneTo<GetListProjectSetupPagingRequest>();

            newRequest.LabCode = labCode;
            newRequest.LabRoomName = labRoomName;
            newRequest.StartDate = startDate;
            newRequest.EndDate = endDate;
            newRequest.UseStatus = useStatus;

            return await _pkFileService.GetListProjectSetupAsync(newRequest);
        }

        public async Task<object> GetProjectListItemAsync(dynamic parameters)
        {
            this.RequiresAuthentication();

            var pagingRequest = GetPagingRequest();

            var labCode = GetQueryParameter<string>("labcode");

            var newRequest = pagingRequest.CloneTo<GetListItemProjectNamePagingResult>();

            newRequest.LabCode = labCode;

            return await _pkFileService.GetListItemProjectAsync(newRequest);
        }

        public async Task<object> AddMoreVideoAsync(dynamic parameters)
        {
            this.RequiresAuthentication();

            var contentTypeRegex = new Regex("^multipart/form-data;\\s*boundary=(.*)$", RegexOptions.IgnoreCase);
            System.IO.Stream fileStream = null;

            if (contentTypeRegex.IsMatch(this.Request.Headers.ContentType))//Yes, it is multi part
            {
                var boundary = contentTypeRegex.Match(this.Request.Headers.ContentType).Groups[1].Value;
                var multipart = new HttpMultipart(this.Request.Body, boundary);
                //bodyStream = multipart.GetBoundaries().First(b => b.ContentType.Equals("application/json")).Value;
                var boundaries = multipart.GetBoundaries();

                AddChunkObjectStorageRequest request = new AddChunkObjectStorageRequest();
                foreach (HttpMultipartBoundary param in boundaries)
                {
                    if (string.IsNullOrWhiteSpace(param.Name))
                        continue;

                    if (param.Name.ToUpper() == "FILE")
                    {
                        request.DataStream = param.Value;
                    }
                    else if (param.Name.ToUpper() == "KEY")//unique key for each file
                    {
                        request.Key = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "FILENAME")
                    {
                        request.FileName = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "TITLE")
                    {
                        request.Title = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "TOTALPART")
                    {
                        var intText = StringHelper.GetStringFromStream(param.Value);
                        request.TotalPart = ConvertHelper.GetInt(intText);
                    }
                    else if (param.Name.ToUpper() == "PARTINDEX")
                    {
                        var intText = StringHelper.GetStringFromStream(param.Value);
                        request.PartIndex = ConvertHelper.GetInt(intText);
                    }
                    else if (param.Name.ToUpper() == "COMPANYCODE")
                    {
                        request.CompanyCode = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "TEAMCODE")
                    {
                        request.TeamCode = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "DESCRIPTION")
                    {
                        request.Description = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "PRIVACY")
                    {
                        var privacyText = StringHelper.GetStringFromStream(param.Value);
                        if (string.IsNullOrWhiteSpace(privacyText) == false)
                        {
                            privacyText = privacyText.ToUpper();
                            if (privacyText.Contains("PUBLIC"))
                                request.IsPublic = true;
                            else if (privacyText.Contains("PRIVATE"))
                                request.IsPrivated = true;
                            else if (privacyText.Contains("DEPARTMENT"))
                                request.IsOnlyForDepartment = true;
                        }
                    }
                    else if (param.Name.ToUpper() == "ISONLYFORDEPARTMENT")
                    {
                        var isOnlyForDepartmentText = StringHelper.GetStringFromStream(param.Value);
                        request.IsOnlyForDepartment = ConvertHelper.GetBool(isOnlyForDepartmentText);
                    }
                    else if (param.Name.ToUpper() == "ISPRIVATED")
                    {
                        var isPrivatedText = StringHelper.GetStringFromStream(param.Value);
                        request.IsPrivated = ConvertHelper.GetBool(isPrivatedText);
                    }
                    else if (param.Name.ToUpper() == "CATALOGUE")
                    {
                        request.Catalogue = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "CATEGORY")
                    {
                        request.Category = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "TIMESTART")
                    {
                        request.TimeStart = StringHelper.GetStringFromStream(param.Value);
                    }
                    else if (param.Name.ToUpper() == "TIMEEND")
                    {
                        request.TimeEnd = StringHelper.GetStringFromStream(param.Value);
                    }
                }

                return await _pkFileService.AddMoreVideoAsync(request);
            }
            else//ignore
            {
            }

            return false;
        }

        
        public async Task<object> GetNewVideoAsync(dynamic parameters)
        {
            return await _pkFileService.GetNewVideoPagingAsync();
        }


    }
}