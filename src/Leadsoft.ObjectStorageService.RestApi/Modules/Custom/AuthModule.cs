using System;
using System.Threading.Tasks;
using Base.Interface.Dto;
using Leadsoft.RestApi.NetCorePlatform.Auth;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using Leadsoft.Zatara.Services.Interface;
using Nancy;
using Nancy.ModelBinding;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class AuthModule : BaseModule
    {
        private readonly IPKUserRepository _pkUserRepo;
        private readonly IIdentityProvider _identityProvider;
        public AuthModule(IPKUserRepository pkUserRepo, IIdentityProvider identityProvider)
            : base("auth", false)
        {
            _pkUserRepo = pkUserRepo;
            _identityProvider = identityProvider;

            Post("/local", LocalLoginAsync, null, "LocalLogin");
        }

        public async Task<object> LocalLoginAsync(dynamic parameters)
        {
            var request = this.Bind<LoginRequest>();
            
            if(string.IsNullOrWhiteSpace(request.UserName) || string.IsNullOrWhiteSpace(request.Password))
                return HttpStatusCode.Unauthorized;

            var user = await _pkUserRepo.GetUserAsync(request.UserName, request.Password);
            if(user == null)
                return HttpStatusCode.Unauthorized;

            //generate token

            string token = _identityProvider.GenerateToken(new AuthToken()
            {
                UserName = user.USERID,
                Scopes = new string[] { "admin" },
                ExpirationDateTime = DateTime.UtcNow.AddDays(1000)
            });

            return new SuccessTaskResult<LoginResponse>(new LoginResponse()
            {
                roles = new string[] { "admin" },
                token = $@"{token}",
                introduction = user.NAME,
                // avatar = "https://avatars3.githubusercontent.com/u/2161321?s=460&v=4",
                name = user.NAME
            });
        }
    }
}