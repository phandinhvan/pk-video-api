using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Services.Interface;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class ObjectBucketModule : BaseResourceModule<ObjectBucket>
    {
        public ObjectBucketModule(IObjectBucketService service)
            : base("objectbuckets", service, true)
        {
        }
    }
}