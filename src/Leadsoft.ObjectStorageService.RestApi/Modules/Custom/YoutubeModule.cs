using System.Threading.Tasks;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using Nancy.ModelBinding;
using System.Text.RegularExpressions;
using Nancy.Responses;
using Nancy;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Common;
using System.IO;
using Base.Interface.Dto;
using Leadsoft.RestApi.NetCorePlatform;
using Base.Services.Interface;
using Nancy.Security;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class YoutubeModule : BaseModule
    {
        private readonly IYoutubeService _service;

        public YoutubeModule(LeadsoftUserContext userContext, IYoutubeService service)
            : base("youtubes", userContext, false)
        {
            _service = service;

            Post("/info", GetVideoInfoAsync, null, "GetVideoInfo");
        }

        public async Task<object> GetVideoInfoAsync(dynamic parameters)
        {
            var request = this.Bind<GetYoutubeInfoRequest>();

            return await _service.GetVideoInfoAsync(request.Link);
        }
    }
}