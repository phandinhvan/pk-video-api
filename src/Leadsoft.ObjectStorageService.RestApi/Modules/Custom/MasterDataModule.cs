using System.Threading.Tasks;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using Nancy.ModelBinding;
using System.Text.RegularExpressions;
using Nancy.Responses;
using Nancy;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Common;
using System.IO;
using Base.Interface.Dto;
using Leadsoft.RestApi.NetCorePlatform;
using Base.Services.Interface;
using System.Collections;
using System.Collections.Generic;
using Leadsoft.Zatara.Interface.Dto.Responses;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class MasterDataModule : BaseModule
    {
        private readonly IMasterDataService _service;

        public MasterDataModule(LeadsoftUserContext userContext, IMasterDataService service)
            : base("masterdatas", false)
        {
            _service = service;


            Get("/", GetMasterDataAsync, null, "GetMasterData");
        }

        public async Task<object> GetMasterDataAsync(dynamic parameters)
        {
            var companycode = this.GetQueryParameter<string>("companycode");
            var departmentcode = this.GetQueryParameter<string>("departmentcode");

            var request = new GetMasterDataRequest() {
                CompanyCode = companycode,
                DepartmentCode = departmentcode
            };
            var data = await _service.GetMasterDataAsync(request);
            return new SuccessTaskResult<MasterDataDto>(data);
        }
    }
}