using System;
using System.Threading.Tasks;
using Base.Interface.Dto;
using Base.Services.Interface;
using Leadsoft.RestApi.NetCorePlatform.Auth;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using Leadsoft.Zatara.Services.Interface;
using Nancy.ModelBinding;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class UserModule : BaseModule
    {
        private readonly IPKUserRepository _pkUserRepo;
        public UserModule(LeadsoftUserContext userContext, IPKUserRepository pkUserRepo)
            : base("users", userContext, true)
        {
            _pkUserRepo = pkUserRepo;

            Get("/me", GetMeAsync, null, "me");
        }

        public async Task<object> GetMeAsync(dynamic parameters)
        {
            var userId = _userContext.UserName;

            var user = await _pkUserRepo.GetUserAsync(userId);
            if(user == null)
                return new FailTaskResult<string>($@"UserID invalid");

            return new SuccessTaskResult<UserDto>(new UserDto()
            {
                UserId = user.USERID,
                Name = user.NAME,
                RoleId = user.ROLEID
            });
        }
    }
}