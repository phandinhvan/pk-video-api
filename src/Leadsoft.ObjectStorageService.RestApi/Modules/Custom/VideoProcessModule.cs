using System.Threading.Tasks;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using Nancy.ModelBinding;
using System.Text.RegularExpressions;
using Nancy.Responses;
using Nancy;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Common;
using System.IO;
using Base.Interface.Dto;
using Leadsoft.RestApi.NetCorePlatform;
using Base.Services.Interface;
using System.Collections;
using System.Collections.Generic;
using Leadsoft.Zatara.Interface.Dto.Responses;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public class VideoProcessModule : BaseModule
    {
        private readonly IVideoProcessingService _service;

        public VideoProcessModule(LeadsoftUserContext userContext, IVideoProcessingService service)
            : base("videoprocess", false)
        {
            _service = service;


            Post("/convert", ConvertAsync, null, "Convert");
        }

        public async Task<object> ConvertAsync(dynamic parameters)
        {
            _service.ConvertVideo($@"D:\objectstorageroot\10162001\5a59fbc0-b802-4254-ae5f-670cf75a40c3.mp4", $@"D:\objectstorageroot\10162001\abc.mp4");

            return await Task.FromResult(new SuccessTaskResult<string>());
        }
    }
}