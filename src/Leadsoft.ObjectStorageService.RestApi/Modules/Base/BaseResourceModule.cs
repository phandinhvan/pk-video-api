using Base.Entities;
using Base.Interface.Dto;
using Base.Services.Interface;
using Nancy.ModelBinding;
using System.Threading.Tasks;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public abstract class BaseResourceModule<T> : BaseModule where T : BaseEntity
    {
        private readonly IBaseResourceService<T> _service;
        private readonly string _resourceName;

        public BaseResourceModule(string resourceName, IBaseResourceService<T> service, bool isAuthenRequire)
            : base($@"/{resourceName}", isAuthenRequire)
        {
            _resourceName = resourceName;
            _service = service;

            Get("/{id}", GetAsync, null, "Get");
            Post("/", AddAsync, null, "Add");
            Delete("/{id}", DeleteAsync, null, "Delete");
            Post("/{id}/update", UpdateAsync, null, "Put");
            CreateGetListRoute();
        }

        protected virtual void CreateGetListRoute()
        {
            Get("/", GetListAsync, null, "GetList");
        }

        public virtual async Task<object> AddAsync(dynamic parameters)
        {
            var request = this.Bind<T>();
            return await _service.AddAsync(request);
        }

        public async Task<object> UpdateAsync(dynamic parameters)
        {
            long id = parameters.id;

            var fullEntity = this.Bind<T>();
            fullEntity.ID = id;

            return await _service.UpdateAsync(fullEntity);
        }

        public async Task<object> DeleteAsync(dynamic parameters)
        {
            long id = parameters.id;
            return await _service.DeleteAsync(id);
        }

        public virtual async Task<object> GetAsync(dynamic parameters)
        {
            long id = parameters.id;
            return await _service.GetAsync(id);
        }

        public virtual async Task<object> GetListAsync(dynamic parameters)
        {
            var request = GetPagingRequest();
            return await _service.GetListAsync(request);
        }
    }
}