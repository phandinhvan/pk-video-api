using Base.Interface.Dto;
using Base.Services.Interface;
using Gandalf.Common;
using Leadsoft.Common;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Nancy.Security;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public abstract class BaseModule : NancyModule
    {
        protected readonly LeadsoftUserContext _userContext;

        public BaseModule(string modulePath, LeadsoftUserContext userContext, bool authen = true)
            : this(modulePath, authen)
        {
            _userContext = userContext;
        }


        protected virtual PagingRequest GetPagingRequest()
        {
            int pageSize = GetQueryParameter<int>("pagesize");
            if (pageSize == 0)
                pageSize = 30;
            int pageIndex = GetQueryParameter<int>("pageindex");
            if (pageIndex == 0)
                pageIndex = 1;

            return new PagingRequest()
            {
                PageIndex = pageIndex,
                PageSize = pageSize
            };
        }
        protected T GetQueryParameter<T>(string paramName)
        {
            var defaultValue = default(T);
            try
            {
                defaultValue = this.Request.Query[paramName];
            }
            catch (System.Exception ex)
            {
            }

            return defaultValue;
        }


        public BaseModule(string modulePath, bool authen = true)
            : base(modulePath)
        {
            if (authen)
            {
                this.RequiresAuthentication();
                this.RequiresClaims(claim => claim.Value == "admin");
            }
        }

        protected void PatchApplyTo<T>(T fullObject) where T : class
        {
            var operations = GetPatchOperations<T>();
            PatchApplyTo(fullObject, operations);
        }

        protected List<Microsoft.AspNetCore.JsonPatch.Operations.Operation<T1>> GetPatchOperations<T1>() where T1 : class
            => this.Bind<List<Microsoft.AspNetCore.JsonPatch.Operations.Operation<T1>>>();

        protected void PatchApplyTo<T1>(T1 fullObject, List<Microsoft.AspNetCore.JsonPatch.Operations.Operation<T1>> operations) where T1 : class
        {
            var patch = new Microsoft.AspNetCore.JsonPatch.JsonPatchDocument<T1>(operations, new Newtonsoft.Json.Serialization.DefaultContractResolver());
            patch.ApplyTo(fullObject);
        }

        #region Exception

        public Negotiator Exception(int errorCode, string errorMessage, object extendData = null)
          => Exception(HttpStatusCode.InternalServerError, errorCode, errorMessage, extendData);

        public Negotiator ExceptionBadRequest(int errorCode, string errorMessage, object extendData = null)
           => Exception(HttpStatusCode.BadRequest, errorCode, errorMessage, extendData);

        public Negotiator ExceptionNotFound(int errorCode, string errorMessage, object extendData = null)
            => Exception(HttpStatusCode.NotFound, errorCode, errorMessage, extendData);

        public Negotiator Exception(HttpStatusCode code, int errorCode, string errorMessage, object extendData = null)
        {
            return Negotiate
                .WithStatusCode(code)
                .WithModel(new { code = errorCode, msg = errorMessage, extendData = extendData })
                .WithContentType("application/json");
        }

        public async Task<object> DownloadFileAsync(string filePath)
        {
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                var dataInBase64 = StringHelper.ConvertToBase64(await StreamHelper.GetBytesAsync(stream));
                return new {
                    data = dataInBase64
                };
            }
        }

        #endregion Exception
    }
}