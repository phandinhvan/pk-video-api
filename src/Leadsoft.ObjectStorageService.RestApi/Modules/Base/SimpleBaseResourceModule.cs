using Base.Entities;
using Base.Interface;
using Base.Interface.Dto;
using Base.Services.Interface;
using Nancy.ModelBinding;
using System.Threading.Tasks;

namespace Leadsoft.ObjectStorageService.RestApi.Modules
{
    public abstract class SimpleBaseResourceModule<T> : BaseModule where T : BaseEntity
    {
        private readonly IGenericRepository<T> _repo;
        private readonly string _resourceName;

        public SimpleBaseResourceModule(string resourceName, IGenericRepository<T> repo, bool isAuthenRequire)
            : base($@"/{resourceName}", isAuthenRequire)
        {
            _resourceName = resourceName;
            _repo = repo;

            Get("/{id}", GetAsync, null, "Get");
            Post("/", AddAsync, null, "Add");
            Post("/{id}/update", UpdateAsync, null, "Put");
        }

         public virtual async Task<object> AddAsync(dynamic parameters)
        {
            var request = this.Bind<T>();
            return await _repo.AddAsync(request);
        }

        public async Task<object> UpdateAsync(dynamic parameters)
        {
            long id = parameters.id;

            var fullEntity = this.Bind<T>();
            fullEntity.ID = id;

            return await _repo.UpdateAsync(fullEntity);
        }

       
        public virtual async Task<object> GetAsync(dynamic parameters)
        {
            long id = parameters.id;
            return await _repo.FindByIdAsync(id);
        }

        // public virtual async Task<object> GetListAsync(dynamic parameters)
        // {
        //     var request = GetPagingRequest();
        //     return await _repo.Get(request);
        // }
    }
}