﻿using Leadsoft.RestApi.NetCorePlatform;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Nancy.Owin;
using Serilog;
using Serilog.Events;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;

namespace Leadsoft.ObjectStorageService.RestApi
{
    public class Startup
    {
        private readonly IConfiguration _applicationConfiguration;
        private readonly IHostingEnvironment _hostingEnvironment;

        public Startup(IHostingEnvironment env)
        {
            _hostingEnvironment = env;
            _applicationConfiguration = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", true, true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
            .AddEnvironmentVariables()
            .Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // Register the IConfiguration instance which options binds against.
            services.AddSingleton(_applicationConfiguration);
            services.AddAuthentication();

            //services.AddCors();
            //services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            //{
            //    builder.AllowAnyOrigin()
            //           .AllowAnyMethod()
            //           .AllowAnyHeader();
            //}));
            //services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime appLifetime)
        {
            //Set global logger
            Log.Logger = CreateLogger();
            loggerFactory.AddSerilog();
            appLifetime.ApplicationStopped.Register(Log.CloseAndFlush);

            //This registration for AuthPlatfrom, the IdTokenMiddleware will validate the request with server
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap = new Dictionary<string, string>();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            })
               .UseOwin()
               .UseMonitorAndLogging(Log.Logger, HealthCheckAsync)
               //.UseAuthPlatform("unified_service", "omnicasa_end_user")
               //.UseImageResizer(storageClient, Log.Logger)
               .UseNancy(opt => opt.Bootstrapper = new RestApiBootstrapper(_applicationConfiguration, Log.Logger, loggerFactory, _hostingEnvironment.ContentRootPath));
        }

        public async Task<bool> HealthCheckAsync()
        {
            return await Task.FromResult<bool>(true);
        }

        private Serilog.ILogger CreateLogger()
        {
            Serilog.ILogger logger = new LoggerConfiguration()
                   .Enrich.FromLogContext()
                   .WriteTo
                   .Async(a => a.RollingFile("logs/log-{Date}.log",
                                            outputTemplate: "{Timestamp:HH:mm:ss.fff} [{Level}] ({CorrelationToken}) [{SourceContext}] {Message}{NewLine}{Exception}",
                                            retainedFileCountLimit: 31,//31 days
                                            buffered: false))
                   .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                   .MinimumLevel.Override("System", LogEventLevel.Warning).CreateLogger();

            return logger;
        }
    }
}