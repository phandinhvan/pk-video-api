﻿/* fn_GetAppointmentCombineSubject */
CREATE FUNCTION [dbo].[fn_GetAppointmentCombineSubject] 
(
	@AppointmentId INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN	
		DECLARE @CombineSubject NVARCHAR(1000)
		DECLARE @Subject NVARCHAR(1000)
		DECLARE @ObjectNames NVARCHAR(1000)
		DECLARE @InternalComment NVARCHAR(1000)
		DECLARE @AppointmentPlace NVARCHAR(1000)
		DECLARE @PersonNames NVARCHAR(1000)
		DECLARE @NewLineChar AS CHAR(2) = CHAR(13) + CHAR(10)

		SET @CombineSubject = ''

		SET  @Subject = (SELECT Subject FROM Appointment WHERE AppointmentID = @AppointmentId)
		SET  @InternalComment = (SELECT InternalComments FROM Appointment WHERE AppointmentID = @AppointmentId)
		SET  @AppointmentPlace = (SELECT AppointmentPlace FROM Appointment WHERE AppointmentID = @AppointmentId)

		SELECT @ObjectNames = COALESCE(@ObjectNames + @NewLineChar, '') + Objects.Ident 
		FROM AgendaProperty
		INNER JOIN Objects ON AgendaProperty.ObjectId = Objects.Id
		WHERE AgendaProperty.AppointmentID = @AppointmentId

		SELECT @PersonNames = COALESCE(@PersonNames + @NewLineChar, '') + Persons.Ident 
		FROM AgendaPerson
		INNER JOIN Persons ON AgendaPerson.PersonID = Persons.PersonId
		WHERE AgendaPerson.AppointmentID = @AppointmentId

		IF ISNULL(@Subject, '') <> ''
		BEGIN
		SET @CombineSubject = @CombineSubject + @Subject
		END

		IF ISNULL(@ObjectNames, '') <> ''
		BEGIN
		SET @CombineSubject = @CombineSubject + @NewLineChar + @ObjectNames
		END

		IF ISNULL(@InternalComment, '') <> ''
		BEGIN
		SET @CombineSubject = @CombineSubject +  @NewLineChar + @InternalComment
		END

		IF ISNULL(@AppointmentPlace, '') <> ''
		BEGIN
		SET @CombineSubject = @CombineSubject +  @NewLineChar + @AppointmentPlace
		END

		IF ISNULL(@PersonNames, '') <> ''
		BEGIN
		SET @CombineSubject = @CombineSubject +  @NewLineChar + @PersonNames
		END
	
	RETURN @CombineSubject
END
GO

/* fn_EnumValues */
--DROP FUNCTION dbo.fn_EnumValues
CREATE FUNCTION [dbo].[fn_EnumValues] 
(
	@NameSpaces NVARCHAR(MAX)
)
RETURNS @values TABLE ([Type] NVARCHAR(1000), Id INT, DescNL NVARCHAR(MAX), DescFR NVARCHAR(MAX), DescEN NVARCHAR(MAX), [OrderIndex] INT)
AS
BEGIN
	INSERT INTO @values
	SELECT [Namespace] AS [Type], EnumValue AS Id, [1] AS DescNL, [2] AS DescFR, [3] AS DescEN, Ord AS [OrderIndex]
	FROM ( 
		SELECT [LangId], EnumText, EnumValue, [Namespace], Ord
		FROM dbo.EnumValues 
		JOIN dbo.fn_SplitStringToTable(@NameSpaces, ',') ns ON ns.ItemData = EnumValues.[Namespace]
	) AS SourceTable
	PIVOT (
		MAX(EnumText) 
		FOR [LangId] IN ([1], [2], [3])
	) AS PivotTable;

	RETURN
END
GO
