﻿using FluentValidation;
using Leadsoft.UnifiedService.Dto;

namespace Leadsoft.UnifiedService.RestApi.Validators
{
    public class AppointmentDtoValidator : AbstractValidator<AppointmentDto>
    {
        public AppointmentDtoValidator()
        {
            RuleFor(request => request.Id)
                .NotNull()
                .GreaterThan(0)
                .WithMessage("Id cannot be empty");

            RuleFor(request => request.AppointmentTypeId)
                .NotNull()
                .GreaterThan(0)
                .WithMessage("Appointment type cannot empty");

            RuleFor(request => request.StartTime)
                .NotNull()
                .WithMessage("Start time cannot be empty");

            RuleFor(request => request.StartTime)
                .NotNull()
                .WithMessage("End time cannot be empty");

            RuleFor(request => request.StartTime)
                .LessThanOrEqualTo(dto=>dto.EndTime)
                .WithMessage("Start time-end time not valid");
        }
    }
}
