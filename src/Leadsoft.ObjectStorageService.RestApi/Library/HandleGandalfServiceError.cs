﻿using Base.Interface.Dto;
using Leadsoft.RestApi.NetCorePlatform.Errors.Exceptions;
using Leadsoft.RestApi.NetCorePlatform.Errors.Model;
using Leadsoft.RestApi.NetCorePlatform.Errors.Specification.Errors;
using Nancy;
using Nancy.Responses.Negotiation;
using System;
using System.Linq;

namespace Leadsoft.UnifiedService.RestApi.Library
{
    public static class HandleGandalfServiceError
    {
        public static Response HandleException(NancyContext context, IResponseNegotiator responseNegotiator, Exception exception)
        {
            //if (exception is OmnicasaServiceException serviceException)
            //{
            //    object model = new { code = serviceException.Code, msg = serviceException.Message, extendData = serviceException.ExtendData };

            //    if (serviceException.Code == int.MinValue) ///multiple exceptions
            //    {
            //        var exceptions = (OmnicasaServiceException[])serviceException.ExtendData;
            //        model = exceptions.Select(a => new { code = a.Code, msg = a.Message, extendData = a.ExtendData });
            //    }

            //    var negotiator = new Negotiator(context)
            //        .WithStatusCode(HttpStatusCode.InternalServerError)
            //        .WithContentType("application/json")
            //        .WithModel(model);

            //    return responseNegotiator.NegotiateResponse(negotiator, context);
            //}

            return HandleGlobalResponse(context, responseNegotiator, exception);
        }

        private static Response HandleGlobalResponse(NancyContext context, IResponseNegotiator responseNegotiator, Exception exception)
        {
            HttpServiceError httpError = new GeneralServiceError();

            if (exception != null)
            {
                if (exception is HttpServiceErrorException exceptionWithServiceError)
                {
                    httpError = exceptionWithServiceError.HttpServiceError;
                }
                else
                {
                    httpError.ServiceError.Details = exception.Message;
                }
            }

            var negotiator = new Negotiator(context)
                //.WithStatusCode(httpError.HttpStatusCode)
                .WithStatusCode(HttpStatusCode.OK)
                .WithContentType("application/json")
                //.WithModel(new { code = httpError.ServiceError.Code, msg = httpError.ServiceError.Details });
                .WithModel(new TaskResult<string>() {
                    IsSuccess = false,
                    Code = (int)httpError.ServiceError.Code,
                    Log = httpError.ServiceError.Details
                });

            return responseNegotiator.NegotiateResponse(negotiator, context);
        }
    }
}