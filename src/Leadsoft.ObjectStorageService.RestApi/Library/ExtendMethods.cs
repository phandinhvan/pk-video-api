﻿using Nancy;

namespace Leadsoft.UnifiedService.RestApi
{
    public static class ExtendMethods
    {
        public static ByteArrayResponse FromBytes(this IResponseFormatter formatter, byte[] bytes, string contentType = "")
        {
            return new ByteArrayResponse(bytes, contentType);
        }
    }
}
