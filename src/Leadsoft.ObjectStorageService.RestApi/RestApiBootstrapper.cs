using Autofac;
using Base.Services.Interface;
using Leadsoft.Zatara.Infrastructure.Data.Dapper;
using Leadsoft.Zatara.Services.Implementation;
using Leadsoft.RestApi.NetCorePlatform;
using Leadsoft.RestApi.NetCorePlatform.Auth;
using Leadsoft.RestApi.NetCorePlatform.Errors.Handler;
using Leadsoft.RestApi.NetCorePlatform.Errors.Specification.Errors;
using Leadsoft.RestApi.NetCorePlatform.Serializer;
using Leadsoft.UnifiedService.RestApi.Library;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nancy;
using Nancy.Authentication.Stateless;
using Nancy.Bootstrapper;
using Nancy.Bootstrappers.Autofac;
using Nancy.Configuration;
using Nancy.Conventions;
using Nancy.Json;
using Nancy.Owin;
using Nancy.Responses.Negotiation;
using Nancy.Swagger.Services;
using Newtonsoft.Json;
using Swagger.ObjectModel;
using System;
using System.Linq;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto;

namespace Leadsoft.ObjectStorageService.RestApi
{
    public class RestApiBootstrapper : AutofacNancyBootstrapper
    {
        private IConfiguration _applicationConfiguration;
        private Serilog.ILogger _logger;
        private ILoggerFactory _loggerFactory;
        private static readonly IMemoryCache _memoryCacheInstance = new Microsoft.Extensions.Caching.Memory.MemoryCache(new MemoryCacheOptions());
        private readonly string _contentRootPath;

        public RestApiBootstrapper(IConfiguration applicationConfiguration, Serilog.ILogger logger, ILoggerFactory loggerFactory, string contentRootPath)
        {
            _applicationConfiguration = applicationConfiguration;
            _logger = logger;
            _loggerFactory = loggerFactory;
            _contentRootPath = contentRootPath;
            //AppDomainAssemblyTypeScanner.AddAssembliesToScan("Omnicasa.ServiceGateway.UniversalService.DTO.dll");

            //AppDomainAssemblyTypeScanner.AddAssembliesToScan("dto.dll");

            //just sample code for load dll to domain
        }

        protected override Func<ITypeCatalog, NancyInternalConfiguration> InternalConfiguration
        {
            get
            {
                return NancyInternalConfiguration
                    .WithOverrides(x =>
                        x.ResponseProcessors = new[] { typeof(CustomJsonProcessor) });
            }
        }

        public override void Configure(INancyEnvironment environment)
        {
            environment.Json(maxJsonLength: int.MaxValue, retainCasing: true);
            base.Configure(environment);
        }

        protected override void ApplicationStartup(ILifetimeScope container, IPipelines pipelines)
        {
            //var identityProvider = container.Resolve<IIdentityProvider>();
            //var statelessAuthConfig = new StatelessAuthenticationConfiguration(identityProvider.GetUserIdentity);
            //StatelessAuthentication.Enable(pipelines, statelessAuthConfig);

            //StaticConfiguration.DisableErrorTraces = false;
            //pipelines.EnableGzipCompression();
            base.ApplicationStartup(container, pipelines);

            //var bus = container.Resolve<IBusControl>();
            //bus.Start();
            //Nancy.Json.JsonSettings.RetainCasing = true;
            //Nancy.Json.JsonSettings.MaxJsonLength = int.MaxValue;
        }

        protected override void ConfigureApplicationContainer(ILifetimeScope existingContainer)
        {
            base.ConfigureApplicationContainer(existingContainer);
            SwaggerMetadataProvider.SetInfo("ObjectStorage api", "v0", "This is rest api using for external providers", new Contact()
            {
                Name = "Phuong Nguyen Dinh",
                EmailAddress = "ndphuong@outlook.com"
            });

            //#if !DEBUG
            //            string apiVersion = _applicationConfiguration["ApiVersion"];
            //            SwaggerMetadataProvider.SetSwaggerRoot(
            //                basePath: apiVersion
            //            );
            //#endif
            existingContainer.Update(builder =>
            {
                builder.RegisterType<DefaultLeadsoftContextFactory>().As<ILeadsoftUserContextFactory>().SingleInstance();
                builder.RegisterType<IdentityProvider>().As<IIdentityProvider>();
                
                builder.RegisterInstance(new AuthSettings()
                {
                    SecretKeyBase64 = _applicationConfiguration["AuthSettings:SecretKeyBase64"]
                });

                builder.RegisterInstance(new VideoServiceSetting()
                {
                    UsingFtpStorageOnly = bool.Parse(_applicationConfiguration["Settings:UsingFtpStorageOnly"]),
                }); 

                builder.RegisterInstance(new LocalStorageSetting()
                {
                    RootPath = _applicationConfiguration["LocalStorage:RootPath"],
                    TempUploadFolder = _applicationConfiguration["LocalStorage:TempUploadFolder"],
                    FFMPEGPath = _applicationConfiguration["VideoConfig:ffmpeg_path"]
                });

                builder.RegisterInstance(new FtpSetting()
                {
                    Host = _applicationConfiguration["FtpStorage:Host"],
                    Port = int.Parse(_applicationConfiguration["FtpStorage:Port"]),
                    UserName = _applicationConfiguration["FtpStorage:UserName"],
                    Password = _applicationConfiguration["FtpStorage:Password"],
                    WorkingDir = _applicationConfiguration["FtpStorage:WorkingDir"]
                });

                builder.RegisterInstance(new LeadsoftUserContext());

                builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>().SingleInstance();

                builder.RegisterInstance(_memoryCacheInstance).As<IMemoryCache>().SingleInstance();

                //Global
                builder.RegisterInstance<IConfiguration>(_applicationConfiguration).SingleInstance();
                builder.RegisterInstance<Serilog.ILogger>(_logger).SingleInstance();
                builder.RegisterInstance<ILoggerFactory>(_loggerFactory).SingleInstance();

                //Default settings
                builder.RegisterType<CustomJsonSerializer>().As<JsonSerializer>().SingleInstance();

                bool isUsingDistributeService = true;
                //#if DEBUG
                isUsingDistributeService = false;
                //#endif

                ZataraBootstrapper bootstraper = new Leadsoft.Zatara.Services.Implementation.ZataraBootstrapper(builder, isUsingDistributeService);
            });
        }

        protected override void ConfigureRequestContainer(ILifetimeScope container, NancyContext context)
        {
            base.ConfigureRequestContainer(container, context);

            //container.Register<Microsoft.EntityFrameworkCore.DbContext, OmnicasaDbContext>();
            container.Update(builder =>
            {
                //single instance for per request lifetime scope
                builder.RegisterType<LeadsoftUserContext>().SingleInstance();
            });
        }

        protected override void RequestStartup(ILifetimeScope container, IPipelines pipelines, NancyContext context)
        {
            base.RequestStartup(container, pipelines, context);

            //This
            //container.UseHttpClientFactory(context);

            var owinEnvironment = context.GetOwinEnvironment();
            //if (owinEnvironment != null && owinEnvironment.ContainsKey(OwinConstants.OMNICASA_END_USER_KEY))
            //    context.CurrentUser = context.GetOwinEnvironment()[OwinConstants.OMNICASA_END_USER_KEY] as ClaimsPrincipal;

            // Add the Common Error Handling Pipeline:
            CustomErrorHandler.Enable(pipelines, container.Resolve<IResponseNegotiator>(), new GeneralServiceError(), customHandleException: HandleGandalfServiceError.HandleException);

            pipelines.EnableCors();

            if (context.Request.Path.EndsWith($@"/api-docs") == false && context.Request.Path.EndsWith($@"/token") == false)
            {
                var identityProvider = container.Resolve<IIdentityProvider>();
                var statelessAuthConfig = new StatelessAuthenticationConfiguration(identityProvider.GetUserIdentity);
                StatelessAuthentication.Enable(pipelines, statelessAuthConfig);
            }

            //pipelines.AfterRequest += NancyGzipCompression.CheckAndCompressResponse;

            // Make custom registrations:
            //configuration.ConfigureRequestContainer(container, pipelines, context);
        }

        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            base.ConfigureConventions(nancyConventions);

            //nancyConventions.StaticContentsConventions.Add(
            //    StaticContentConventionBuilder.AddDirectory("/swagger-ui/dist")
            //);
        }

        private Type GetInterfaceType(Type t, params string[] baseInterfaceNames)
        {
            return t.GetInterfaces().Where(i => baseInterfaceNames.All(name => !i.Name.Contains(name))).FirstOrDefault() ?? t;
        }
    }
}