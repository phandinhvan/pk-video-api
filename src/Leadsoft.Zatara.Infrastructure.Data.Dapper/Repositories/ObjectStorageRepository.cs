﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Base.Infrastructure.Data.Dapper;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class ObjectStorageRepository : DapperGenericRepository<ObjectStorage>, IObjectStorageRepository
    {
        public ObjectStorageRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public async Task<int> DeleteObjectByKeyAsync(string key)
        {
            return await ExecuteAsync($@"DELETE FROM objectstorages WHERE OBJECTKEY like @key", new { key = key});
        }

        public async Task<ObjectStorage> GetObjectByKeyAsync(long bucketId, string key)
        {
            return await QueryFirstOrDefaultAsync<ObjectStorage>($@"SELECT * 
                                                                    FROM objectstorages
                                                                    WHERE
                                                                    BUCKETID = @bucketId
                                                                    AND OBJECTKEY like @key", new { bucketId = bucketId, key = key});
        }

        public async Task<ObjectStorage> GetObjectByIdAsync(long bucketId, long id)
        {
            return await QueryFirstOrDefaultAsync<ObjectStorage>($@"SELECT * 
                                                                    FROM objectstorages
                                                                    WHERE 
                                                                    BUCKETID = @bucketId
                                                                    AND ID = @id", new { bucketId = bucketId, id = id});
        }

        public async Task<IEnumerable<ObjectStorage>> SearchAsync(long bucketId,string keyword)
        {
            return await QueryAsync<ObjectStorage>($@"SELECT * 
                                                        FROM objectstorages
                                                        WHERE 
                                                        BUCKETID = @bucketId
                                                        AND OBJECTKEY like CONCAT('%', @keyword, '%')", new {bucketid = bucketId, keyword = keyword});
        }
    }
}