﻿using Base.Infrastructure.Data.Dapper;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class DealRepository : DapperGenericRepository<Deal>, IDealRepository
    {
        public DealRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }
    }
}