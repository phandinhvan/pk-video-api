﻿using Base.Infrastructure.Data.Dapper;
using Base.Interface.Dto;
using Leadsoft.Common;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Interface.Dto.Requests;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class PKFileCommentRepository : DapperGenericRepository<PKFileComment>, IPKFileCommentRepository
    {
        public PKFileCommentRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public async Task<PagingResult<PKFileComment>> GetListPagingAsync(GetCommentPagingRequest request)
        {
            int startIndex = NumberHelper.GetPagingStartIndex(request.PageIndex, request.PageSize);
            int endIndex = NumberHelper.GetPagingEndIndex(request.PageIndex, request.PageSize);

            string sqlCount = $@"SELECT COUNT(1)
                                FROM {_tableName}
                                WHERE
                                FILEID = '{request.FileId}' OR '{request.FileId}' = ''
                                ";

            string sql = $@"
                    SELECT *
                    FROM
                    (
                        SELECT {_tableName}.*,
                        rownum  rnum
                        FROM {_tableName}
                        WHERE
                        FILEID = '{request.FileId}' OR '{request.FileId}' = ''
                        ORDER BY COMMENTDATE DESC
                    )
                    WHERE rnum BETWEEN {startIndex} AND {endIndex}";

            //var parameter = new { UserId = request.UserId, ExchangeId = request.ExchangeId };

            int totalRecordCount = await ExecuteScalarAsync<int>(sqlCount);
            int totalPageCount = NumberHelper.CalcPageCount(totalRecordCount, request.PageSize);

            var data = await QueryAsync<PKFileComment>(sql);

            return new PagingResult<PKFileComment>()
            {
                IsSuccess = true,
                PageSize = request.PageSize,
                TotalPageCount = totalPageCount,
                TotalRecord = totalRecordCount,
                CurrentPageIndex = request.PageIndex,
                Result = data
            };
        }
    }
}