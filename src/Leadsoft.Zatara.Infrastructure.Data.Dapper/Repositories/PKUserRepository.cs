﻿using System.Threading.Tasks;
using Base.Infrastructure.Data.Dapper;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class PKUserRepository : DapperGenericRepository<PKUser>, IPKUserRepository
    {
        public PKUserRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public async Task<PKUser> GetUserAsync(string userid, string password)
        {
            var user = await QueryFirstOrDefaultAsync<PKUser>($@"
                            SELECT * FROM {_tableName}
                            WHERE USERID = '{userid}'");

            if(user == null) return null;

            if(user.PASSWD != password) return null;

            return user;
        }

        public async Task<PKUser> GetUserAsync(string userId)
        {
            var user_admin = await QueryFirstOrDefaultAsync<PKUser>($@"
                            SELECT T_CM_USMT.USERID, T_CM_USMT.NAME, temp.ROLEID
                            FROM T_CM_USMT
                            JOIN(SELECT T_CM_URMT.USERID, T_CM_URMT.ROLEID FROM T_CM_URMT  WHERE ROLEID = '9100' and USERID = '{userId}') temp
                            ON temp.USERID = T_CM_USMT.USERID
            ");

            if(user_admin != null)
            {
                return user_admin;
            }

            return await QueryFirstOrDefaultAsync<PKUser>($@"
                            SELECT * FROM {_tableName}
                            WHERE USERID = '{userId}'");
        }
    }
}