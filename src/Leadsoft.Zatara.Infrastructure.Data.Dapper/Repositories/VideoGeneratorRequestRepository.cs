﻿using Base.Infrastructure.Data.Dapper;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class VideoGeneratorRequestRepository : DapperGenericRepository<VideoGeneratorRequest>, IVideoGeneratorRequestRepository
    {
        public VideoGeneratorRequestRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }
    }
}