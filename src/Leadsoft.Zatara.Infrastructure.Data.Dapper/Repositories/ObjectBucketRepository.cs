﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Base.Infrastructure.Data.Dapper;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class ObjectBucketRepository : DapperGenericRepository<ObjectBucket>, IObjectBucketRepository
    {
        public ObjectBucketRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public async Task<int> DeleteObjectByKeyAsync(string key)
        {
            return await ExecuteAsync($@"DELETE FROM  {_tableName} WHERE BUCKETKEY like '{key}'");
        }

        public async Task<ObjectBucket> GetObjectByKeyAsync(string key)
        {
            return await QueryFirstOrDefaultAsync<ObjectBucket>($@"SELECT * 
                                                                    FROM {_tableName}
                                                                    WHERE BUCKETKEY like '{key}'");
        }
    }
}