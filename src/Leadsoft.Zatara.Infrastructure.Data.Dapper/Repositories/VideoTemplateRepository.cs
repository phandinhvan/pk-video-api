﻿using Base.Infrastructure.Data.Dapper;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class VideoTemplateRepository : DapperGenericRepository<VideoTemplate>, IVideoTemplateRepository
    {
        public VideoTemplateRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }
    }
}   