﻿using Base.Infrastructure.Data.Dapper;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class VideoGeneratorRequestItemRepository : DapperGenericRepository<VideoGeneratorRequestItem>, 
                        IVideoGeneratorRequestItemRepository
    {
        public VideoGeneratorRequestItemRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }
    }
}   