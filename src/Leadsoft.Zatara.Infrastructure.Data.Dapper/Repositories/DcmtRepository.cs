﻿using Base.Infrastructure.Data.Dapper;
using Base.Interface.Dto;
using Dommel;
using Leadsoft.Common;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class DcmtRepository : DapperGenericRepository<Dcmt>, IDcmtRepository
    {
        public DcmtRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }


        public async Task<IEnumerable<CompanyDto>> GetAllCompanyDtoAsync()
        {
            return await QueryAsync<CompanyDto>($@"
                            SELECT 
                            DEPTCODE AS CODE,
                            SHORTNAME AS NAME
                            FROM {_tableName}
                            WHERE DEPTLEVEL = '01'
                            AND USESTATUS = 'Y'
                            AND DEPTCODE != '1005'
                            UNION ALL
                            SELECT LABCODE AS CODE,
                            LABROOMNAME AS NAME
                            FROM T_PB_LRMT");
        }

        public async Task<IEnumerable<BuyerDto>> GetAllBuyerDtoAsync()
        {
            return await QueryAsync<BuyerDto>($@"
                            SELECT 
                            S_CODE AS CODE,
                            CODE_NAME AS NAME
                            FROM T_CM_MCMT
                            WHERE M_CODE = 'Buyer'
                            AND S_CODE <> '000'
                            AND CODE_STATUS = 'OK'
                            ORDER BY S_CODE ASC");
        }

        public async Task<IEnumerable<DepartmentDto>> GetAllDeptDtoAsync(string labcode)
        {
            return await QueryAsync<DepartmentDto>($@"SELECT DISTINCT
                            T_CM_DCMT.DEPTCODE AS CODE,
                            T_CM_DCMT.SHORTNAME AS NAME
                            FROM T_CM_DCMT
                            INNER JOIN T_CM_DCDT ON T_CM_DCMT.DEPTCODE = T_CM_DCDT.DEPARTMENT
                            WHERE T_CM_DCMT.DEPTLEVEL = '02'
                            AND T_CM_DCMT.USESTATUS = 'Y'
                            AND T_CM_DCDT.DEPTLEVEL = '02'
                            AND T_CM_DCMT.DEPTCODE != '1005'
                            AND (T_CM_DCDT.CORPORATION = '{labcode}' OR NVL('{labcode}', 'abc') = 'abc')
                            UNION ALL
                            SELECT
                            PROCODE AS CODE,
                            PRONAME AS NAME
                            FROM T_PB_LPMT
                            WHERE LABCODE = '{labcode}' ");
        }

        public async Task<IEnumerable<CategoryDto>> GetAllCateogryDtoAsync(string labcode, string department)
        {
            return await QueryAsync<CategoryDto>($@"SELECT DISTINCT
                            T_PB_VCAT.CATEGORY AS NAME
                            FROM T_PB_VCAT
                            WHERE T_PB_VCAT.CORPORATION = '{labcode}' AND T_PB_VCAT.DEPARTMENT = '{department}'");
        }
    }
}