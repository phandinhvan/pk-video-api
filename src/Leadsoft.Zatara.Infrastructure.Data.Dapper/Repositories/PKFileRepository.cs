﻿using Base.Infrastructure.Data.Dapper;
using Base.Interface.Dto;
using Dommel;
using Leadsoft.Common;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class PKFileRepository : DapperGenericRepository<PKFile>, IPKFileRepository
    {
        public PKFileRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }

        public override async Task<PKFile> AddAsync(PKFile obj)
        {
            try
            {
                using (var connection = GetDbConnection())
                {
                    await connection.InsertOracleAsync<PKFile>(obj);

                    return await FindByIdAsync(obj.FILEID);
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

            //using (var connection = GetDbConnection())
            //{
            //    await connection.InsertOracleAsync<PKFile>(obj);

            //    return await FindByIdAsync(obj.FILEID);
            //}
        }

        public async Task<PKFile> FindByIdAsync(string fileid)
        {
            return await QueryFirstOrDefaultAsync<PKFile>($@"SELECT * FROM {_tableName} WHERE FILEID = '{fileid}'");
        }

        public async Task<PagingResult<PKFileDto>> GetListPagingResultAsync(GetVideoPagingRequest request)
        {
            if(string.IsNullOrWhiteSpace(request.FileName))
                request.FileName = string.Empty;

            if(string.IsNullOrWhiteSpace(request.CompanyCode))
                request.CompanyCode = string.Empty;

            if(string.IsNullOrWhiteSpace(request.DepartmentCode))
                request.DepartmentCode = string.Empty;


            int startIndex = NumberHelper.GetPagingStartIndex(request.PageIndex, request.PageSize);
            int endIndex = NumberHelper.GetPagingEndIndex(request.PageIndex, request.PageSize);

            string sqlCount = $@"SELECT COUNT(1)
                                FROM {_tableName}
                                WHERE
                                (UPPER(t_pb_ufmt.FILENAME) LIKE '%{request.FileName.ToUpper()}%' OR NVL('{request.FileName}', 'abc') = 'abc')
                                AND (CORPORATION = '{request.CompanyCode}' OR NVL('{request.CompanyCode}', 'abc') = 'abc')
                                AND
                                (DEPARTMENT = '{request.DepartmentCode}' OR NVL('{request.DepartmentCode}', 'abc') = 'abc')
                                AND 
                                (UPLOADID = '{request.UserId}' OR NVL('{request.UserId}', 'abc') = 'abc')
                                AND
                                (UPLOADDATE > TO_DATE('{request.StartDate}', 'YYYY-MM-DD HH24:MI:SS') OR NVL('{request.StartDate}', 'abc') = 'abc')
										AND (UPLOADDATE < TO_DATE('{request.EndDate}', 'YYYY-MM-DD HH24:MI:SS') OR NVL('{request.EndDate}', 'abc') = 'abc')
                                AND
                                (CATALOGUE = '{request.Catalogue}' OR NVL('{request.Catalogue}', 'abc') = 'abc')
                                AND
                                (CATEGORY = '{request.Category}' OR NVL('{request.Category}', 'abc') = 'abc')
                                ";

            string sql = $@"
                    SELECT *
                    FROM
                    (
                        SELECT {_tableName}.*,
                        company.DEPTCODE || '-' || company.SHORTNAME AS COMPANY_NAME,
                        department.DEPTCODE || '-' || department.SHORTNAME AS DEPARTMENT_NAME,
                        users.NAME AS UPLOADER_NAME,
                        TO_CHAR(t_pb_ufmt.UPLOADDATE, 'YYYY-MON-DD HH24:MI:SS') AS  UPLOADDATE_TEXT,
                        row_number() OVER (ORDER BY t_pb_ufmt.UPLOADDATE DESC) rnum
                        FROM t_pb_ufmt
                        LEFT JOIN T_CM_DCMT company ON t_pb_ufmt.CORPORATION = company.DEPTCODE
                        LEFT JOIN T_CM_DCMT department ON t_pb_ufmt.DEPARTMENT = department.DEPTCODE
                        LEFT JOIN T_CM_USMT users ON t_pb_ufmt.UPLOADID = users.USERID
                        WHERE
                        (UPPER(t_pb_ufmt.FILENAME) LIKE '%{request.FileName.ToUpper()}%' OR NVL('{request.FileName}', 'abc') = 'abc')
                        AND (t_pb_ufmt.CORPORATION = '{request.CompanyCode}' OR NVL('{request.CompanyCode}', 'abc') = 'abc')
                        AND
                        (t_pb_ufmt.DEPARTMENT = '{request.DepartmentCode}' OR NVL('{request.DepartmentCode}', 'abc') = 'abc')
                        AND
                        (t_pb_ufmt.UPLOADID = '{request.UserId}' OR NVL('{request.UserId}', 'abc') = 'abc')
                        AND
                        (t_pb_ufmt.UPLOADDATE > TO_DATE('{request.StartDate}', 'YYYY-MM-DD HH24:MI:SS') OR NVL('{request.StartDate}', 'abc') = 'abc')
								AND (t_pb_ufmt.UPLOADDATE < TO_DATE('{request.EndDate}', 'YYYY-MM-DD HH24:MI:SS') OR NVL('{request.EndDate}', 'abc') = 'abc')
                        AND
                        (CATALOGUE = '{request.Catalogue}' OR NVL('{request.Catalogue}', 'abc') = 'abc')
                        AND
                        (CATEGORY = '{request.Category}' OR NVL('{request.Category}', 'abc') = 'abc')
                    )
                    WHERE rnum BETWEEN {startIndex} AND {endIndex}";

            int totalRecordCount = await ExecuteScalarAsync<int>(sqlCount);
            int totalPageCount = NumberHelper.CalcPageCount(totalRecordCount, request.PageSize);

            var data = await QueryAsync<PKFileDto>(sql);

            return new PagingResult<PKFileDto>()
            {
                IsSuccess = true,
                PageSize = request.PageSize,
                TotalPageCount = totalPageCount,
                TotalRecord = totalRecordCount,
                CurrentPageIndex = request.PageIndex,
                Result = data
            };
        }

        public async Task<PKFileDto> GetDtoByIdAsync(string fileId)
        {
            return await QueryFirstOrDefaultAsync<PKFileDto>($@"
                        SELECT {_tableName}.*,
                        company.DEPTCODE || '-' || company.SHORTNAME AS COMPANY_NAME,
                        department.DEPTCODE || '-' || department.SHORTNAME AS DEPARTMENT_NAME,
                        users.NAME AS UPLOADER_NAME,
                        TO_CHAR(t_pb_ufmt.UPLOADDATE, 'YYYY-MON-DD') AS  UPLOADDATE_TEXT,
                        rownum  rnum
                        FROM t_pb_ufmt
                        LEFT JOIN T_CM_DCMT company ON t_pb_ufmt.CORPORATION = company.DEPTCODE
                        LEFT JOIN T_CM_DCMT department ON t_pb_ufmt.DEPARTMENT = department.DEPTCODE
                        LEFT JOIN T_CM_USMT users ON t_pb_ufmt.UPLOADID = users.USERID
                        WHERE
                        t_pb_ufmt.FILEID = '{fileId}'");
        }

        public override async Task<bool> UpdateAsync(PKFile entity)
        {
            await ExecuteAsync($@"
                        UPDATE {_tableName}
                        SET 
                            FILENAME = '{entity.FILENAME}',
                            CORPORATION = '{entity.CORPORATION}',
                            DEPARTMENT = '{entity.DEPARTMENT}',
                            PUBLICHCHECK = '{entity.PUBLICHCHECK}',
                            DEPARTCHECK = '{entity.DEPARTCHECK}',
                            PRIVATECHECK = '{entity.PRIVATECHECK}'
                        WHERE
                            FILEID = '{entity.FILEID}'
                        ");

            return true;
        }

        /*----- Code by Dinh Van -----*/
        /*----- Begin get list manage style when select buyer -----*/
        public async Task<PagingResult<ManageStyleDto>> GetListManageStylePagingResultAsync(GetVideoManageStylePagingRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Buyer))
                request.Buyer = string.Empty;

            if (string.IsNullOrWhiteSpace(request.FileName))
                request.FileName = string.Empty;

            int startIndex = NumberHelper.GetPagingStartIndex(request.PageIndex, request.PageSize);
            int endIndex = NumberHelper.GetPagingEndIndex(request.PageIndex, request.PageSize);

            string sqlCount = $@"
                                SELECT COUNT(1)
                                FROM T_SD_DORM dorm
                                        JOIN T_00_STMT stmt ON dorm.STYLECODE = stmt.STYLECODE
									    JOIN T_00_SCMT scmt ON dorm.STYLECODE = scmt.STYLECODE AND dorm.STYLECOLORSERIAL = scmt.STYLECOLORSERIAL
									    LEFT JOIN T_CM_FCMT fcmt ON dorm.FACTORY = fcmt.FACTORY
									    JOIN T_CM_USMT usmt ON dorm.REGISTER = usmt.USERID
                                 WHERE  (UPPER(stmt.STYLECODE) LIKE '%{request.FileName.ToUpper()}%' OR NVL('{request.FileName}', 'abc') = 'abc')
                                        AND (stmt.BUYER = '{request.Buyer}' OR NVL('{request.Buyer}', 'abc') = 'abc')
                                ";

            string sql = $@"
                    SELECT *
                    FROM
                    (
                        SELECT row_number() OVER (ORDER BY dorm.STYLECODE DESC) rnum,
                                stmt.BUYER, 
                                stmt.STYLENAME,
                                stmt.BUYERSTYLECODE,
                                stmt.BUYERSTYLENAME, 
                                stmt.UNITPRICE,
                                stmt.FACTORY,
                                stmt.STATUS,
                                stmt.CURRCODE,
                                dorm.STYLECODE, 
                                dorm.STYLESIZE,
                                dorm.STYLECOLORSERIAL,
                                dorm.REVNO, 
                                dorm.REGISTER,
                                dorm.REGISTRYDATE,
                                dorm.REMARKS, 
                                TO_CHAR(dorm.AD_CONFIRM, 'DD-MM-YYYY HH24:MI:SS') AS  FINAL_CONFIRM,
                                scmt.STYLECOLORWAYS,
                                usmt.NAME AS UESERNAME
                        FROM T_SD_DORM  dorm
                             JOIN T_00_STMT stmt ON dorm.STYLECODE = stmt.STYLECODE
						     JOIN T_00_SCMT scmt ON dorm.STYLECODE = scmt.STYLECODE AND dorm.STYLECOLORSERIAL = scmt.STYLECOLORSERIAL
						     LEFT JOIN T_CM_FCMT fcmt ON dorm.FACTORY = fcmt.FACTORY
						     JOIN T_CM_USMT usmt ON dorm.REGISTER = usmt.USERID
                        WHERE (UPPER(stmt.STYLECODE) LIKE '%{request.FileName.ToUpper()}%' OR NVL('{request.FileName}', 'abc') = 'abc')
                             AND (stmt.BUYER = '{request.Buyer}' OR NVL('{request.Buyer}', 'abc') = 'abc')
                        ORDER BY dorm.STYLECODE DESC, stmt.BUYERSTYLECODE, dorm.REVNO, scmt.STYLECOLORWAYS DESC
                    )
                    WHERE rnum BETWEEN {startIndex} AND {endIndex}";

            ////var parameter = new { UserId = request.UserId, ExchangeId = request.ExchangeId };

            int totalRecordCount = await ExecuteScalarAsync<int>(sqlCount);
            int totalPageCount = NumberHelper.CalcPageCount(totalRecordCount, request.PageSize);


            var data = await QueryAsync<ManageStyleDto>(sql);

            return new PagingResult<ManageStyleDto>()
            {
                IsSuccess = true,
                PageSize = request.PageSize,
                TotalPageCount = totalPageCount,
                TotalRecord = totalRecordCount,
                CurrentPageIndex = request.PageIndex,
                Result = data
            };
        }
        /*----- End get list manage style when select buyer -----*/


        /*----- Start get list video when click row manage style -----*/
        public async Task<PagingResult<ManageStyleVideoDto>> GetListVideoManageStylePagingResultAsync(GetListVideoManageStylePagingRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.CompanyCode))
                request.CompanyCode = string.Empty;

            if (string.IsNullOrWhiteSpace(request.DepartmentCode))
                request.DepartmentCode = string.Empty;


            int startIndex = NumberHelper.GetPagingStartIndex(request.PageIndex, request.PageSize);
            int endIndex = NumberHelper.GetPagingEndIndex(request.PageIndex, request.PageSize);
            var CompanyCode_Color = request.StyleCode + request.StyleSize + "000";

            string sqlCount = $@"SELECT COUNT(1)
                                FROM {_tableName}
                                WHERE
                                ( (t_pb_ufmt.CORPORATION = '{request.StyleCode}' )
                                    OR
                                    (t_pb_ufmt.CORPORATION = '{request.CompanyCode}' AND t_pb_ufmt.DEPARTMENT = '000' )
                                    OR
                                    (t_pb_ufmt.CORPORATION = '{CompanyCode_Color}' AND t_pb_ufmt.DEPARTMENT = '{request.DepartmentCode}' ) 
                                    OR
                                    (t_pb_ufmt.CORPORATION = '{CompanyCode_Color}' AND t_pb_ufmt.DEPARTMENT = '000' ) 
                                    OR
                                    (t_pb_ufmt.CORPORATION = '{request.CompanyCode}' AND t_pb_ufmt.DEPARTMENT = '{request.DepartmentCode}' ) 
                                )";
            string sql = $@"
                    SELECT *
                    FROM
                    (
                        SELECT {_tableName}.*,
                        users.NAME AS UPLOADER_NAME,
                        TO_CHAR(t_pb_ufmt.UPLOADDATE, 'YYYY-MON-DD HH24:MI:SS') AS  UPLOADDATE_TEXT,
                        row_number() OVER (ORDER BY t_pb_ufmt.UPLOADDATE DESC) rnum
                        FROM t_pb_ufmt
                        LEFT JOIN T_CM_DCMT company ON t_pb_ufmt.CORPORATION = company.DEPTCODE
                        LEFT JOIN T_CM_DCMT department ON t_pb_ufmt.DEPARTMENT = department.DEPTCODE
                        LEFT JOIN T_CM_USMT users ON t_pb_ufmt.UPLOADID = users.USERID
                        WHERE
                        ( (t_pb_ufmt.CORPORATION = '{request.StyleCode}' )
                        OR
                        (t_pb_ufmt.CORPORATION = '{request.CompanyCode}' AND t_pb_ufmt.DEPARTMENT = '000' ) 
                        OR
                        (t_pb_ufmt.CORPORATION = '{CompanyCode_Color}' AND t_pb_ufmt.DEPARTMENT = '{request.DepartmentCode}' ) 
                        OR
                        (t_pb_ufmt.CORPORATION = '{CompanyCode_Color}' AND t_pb_ufmt.DEPARTMENT = '000' ) 
                        OR
                        (t_pb_ufmt.CORPORATION = '{request.CompanyCode}' AND t_pb_ufmt.DEPARTMENT = '{request.DepartmentCode}' ) 
                        )
                    )
                    WHERE rnum BETWEEN {startIndex} AND {endIndex}";

            //var parameter = new { UserId = request.UserId, ExchangeId = request.ExchangeId };

            int totalRecordCount = await ExecuteScalarAsync<int>(sqlCount);
            int totalPageCount = NumberHelper.CalcPageCount(totalRecordCount, request.PageSize);

            var data = await QueryAsync<ManageStyleVideoDto>(sql);

            return new PagingResult<ManageStyleVideoDto>()
            {
                IsSuccess = true,
                PageSize = request.PageSize,
                TotalPageCount = totalPageCount,
                TotalRecord = totalRecordCount,
                CurrentPageIndex = request.PageIndex,
                Result = data
            };
        }
        /*----- Start get list video when click row manage style -----*/

        
        /*----- Start get list project name menu setups -----*/
        public async Task<PagingResult<SetupDto>> GetListProjectNamePagingResultAsync(GetListProjectSetupPagingRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.LabCode))
                request.LabCode = string.Empty;

            if (string.IsNullOrWhiteSpace(request.LabRoomName))
                request.LabRoomName = string.Empty;

            string sqlCount = $@"SELECT COUNT(1)
                                 FROM T_PB_LRMT";

            string sql = $@"
                    SELECT T_PB_LRMT.*, users.NAME AS UPLOADER_NAME, 
                    TO_CHAR(T_PB_LRMT.REGISTRYDATE, 'YYYY-MON-DD HH24:MI:SS') AS  UPLOADDATE_TEXT
                    FROM T_PB_LRMT 
                    LEFT JOIN T_CM_USMT users ON T_PB_LRMT.REGISTERID = users.USERID";

            int totalRecordCount = await ExecuteScalarAsync<int>(sqlCount);
            int totalPageCount = NumberHelper.CalcPageCount(totalRecordCount, request.PageSize);

            var data = await QueryAsync<SetupDto>(sql);

            return new PagingResult<SetupDto>()
            {
                IsSuccess = true,
                PageSize = request.PageSize,
                TotalPageCount = totalPageCount,
                TotalRecord = totalRecordCount,
                CurrentPageIndex = request.PageIndex,
                Result = data
            };

        }

        /*----- End get list project name menu setups -----*/

        public async Task<PagingResult<SetupDto>> GetListItemProjectNamePagingResultAsync(GetListItemProjectNamePagingResult request)
        {
            if (string.IsNullOrWhiteSpace(request.LabCode))
                request.LabCode = string.Empty;


            int startIndex = NumberHelper.GetPagingStartIndex(request.PageIndex, request.PageSize);
            int endIndex = NumberHelper.GetPagingEndIndex(request.PageIndex, request.PageSize);

            //string sqlCount = $@"SELECT COUNT(1)
            //                    FROM {_tableName}
            //                    WHERE
            //                    ( (t_pb_ufmt.CORPORATION = '{request.StyleCode}' )
            //                        OR
            //                        (t_pb_ufmt.CORPORATION = '{request.CompanyCode}' AND t_pb_ufmt.DEPARTMENT = '000' ) 
            //                        OR
            //                        (t_pb_ufmt.CORPORATION = '{CompanyCode_Color}' AND t_pb_ufmt.DEPARTMENT = '{request.DepartmentCode}' ) 
            //                        OR
            //                        (t_pb_ufmt.CORPORATION = '{CompanyCode_Color}' AND t_pb_ufmt.DEPARTMENT = '000' ) 
            //                        OR
            //                        (t_pb_ufmt.CORPORATION = '{request.CompanyCode}' AND t_pb_ufmt.DEPARTMENT = '{request.DepartmentCode}' ) 
            //                    )";
            //string sql = $@"
            //        SELECT *
            //        FROM
            //        (
            //            SELECT {_tableName}.*,
            //            users.NAME AS UPLOADER_NAME,
            //            TO_CHAR(t_pb_ufmt.UPLOADDATE, 'YYYY-MON-DD HH24:MI:SS') AS  UPLOADDATE_TEXT,
            //            row_number() OVER (ORDER BY t_pb_ufmt.UPLOADDATE DESC) rnum
            //            FROM t_pb_ufmt
            //            LEFT JOIN T_CM_DCMT company ON t_pb_ufmt.CORPORATION = company.DEPTCODE
            //            LEFT JOIN T_CM_DCMT department ON t_pb_ufmt.DEPARTMENT = department.DEPTCODE
            //            LEFT JOIN T_CM_USMT users ON t_pb_ufmt.UPLOADID = users.USERID
            //            WHERE
            //            ( (t_pb_ufmt.CORPORATION = '{request.StyleCode}' )
            //            OR
            //            (t_pb_ufmt.CORPORATION = '{request.CompanyCode}' AND t_pb_ufmt.DEPARTMENT = '000' ) 
            //            OR
            //            (t_pb_ufmt.CORPORATION = '{CompanyCode_Color}' AND t_pb_ufmt.DEPARTMENT = '{request.DepartmentCode}' ) 
            //            OR
            //            (t_pb_ufmt.CORPORATION = '{CompanyCode_Color}' AND t_pb_ufmt.DEPARTMENT = '000' ) 
            //            OR
            //            (t_pb_ufmt.CORPORATION = '{request.CompanyCode}' AND t_pb_ufmt.DEPARTMENT = '{request.DepartmentCode}' ) 
            //            )
            //        )
            //        WHERE rnum BETWEEN {startIndex} AND {endIndex}";

            //var parameter = new { UserId = request.UserId, ExchangeId = request.ExchangeId };

            //int totalRecordCount = await ExecuteScalarAsync<int>(sqlCount);
            //int totalPageCount = NumberHelper.CalcPageCount(totalRecordCount, request.PageSize);
            string sql = $@"
                    SELECT T_PB_LPMT.*, users.NAME AS UPLOADER_NAME,
                    TO_CHAR(T_PB_LPMT.REGISTRYDATE, 'YYYY-MON-DD HH24:MI:SS') AS  UPLOADDATE_TEXT
                    FROM T_PB_LPMT 
                    LEFT JOIN T_CM_USMT users ON T_PB_LPMT.REGISTERID = users.USERID
                    WHERE T_PB_LPMT.LABCODE = '{request.LabCode}'";

            var data = await QueryAsync<SetupDto>(sql);

            return new PagingResult<SetupDto>()
            {
                IsSuccess = true,
                Result = data
            };

        }

        
        public async Task<PagingResult<PKFileDto>> GetNewVideoPagingResultAsync()
        {
            string sql = $@"select * from 
                              (SELECT t_pb_ufmt.*, company.DEPTCODE || '-' || company.SHORTNAME AS COMPANY_NAME,
                                        department.DEPTCODE || '-' || department.SHORTNAME AS DEPARTMENT_NAME,
                                        users.NAME AS UPLOADER_NAME,
                                        TO_CHAR(t_pb_ufmt.UPLOADDATE, 'YYYY-MON-DD HH24:MI:SS') AS  UPLOADDATE_TEXT
                                        FROM t_pb_ufmt
                                        LEFT JOIN T_CM_DCMT company ON t_pb_ufmt.CORPORATION = company.DEPTCODE
                                        LEFT JOIN T_CM_DCMT department ON t_pb_ufmt.DEPARTMENT = department.DEPTCODE
                                        LEFT JOIN T_CM_USMT users ON t_pb_ufmt.UPLOADID = users.USERID
                               WHERE  t_pb_ufmt.catalogue = 'video' AND t_pb_ufmt.FILETYPE = '.mp4'
                               order by UPLOADDATE desc) 
                            where rownum=1";

            var data = await QueryAsync<PKFileDto>(sql);

            return new PagingResult<PKFileDto>()
            {
                IsSuccess = true,
                Result = data
            };
        }

    }
}