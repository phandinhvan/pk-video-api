﻿using Base.Infrastructure.Data.Dapper;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class VideoTemplateTypeRepository : DapperGenericRepository<VideoTemplateType>, IVideoTemplateTypeRepository
    {
        public VideoTemplateTypeRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }
    }
}