﻿using Base.Infrastructure.Data.Dapper;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories
{
    public class VideoTemplateItemRepository : DapperGenericRepository<VideoTemplateItem>, IVideoTemplateItemRepository
    {
        public VideoTemplateItemRepository(IConnectionFactory connectionFactory)
            : base(connectionFactory)
        {
        }
    }
}