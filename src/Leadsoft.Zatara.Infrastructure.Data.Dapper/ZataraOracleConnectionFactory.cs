﻿using Base.Infrastructure.Data.Dapper;
using System.Data;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper
{
    public class ZataraOracleConnectionFactory : OracleConnectionFactory, IConnectionFactory
    {
        public ZataraOracleConnectionFactory(string connectionString)
            : base(connectionString)
        {
        }
    }
}