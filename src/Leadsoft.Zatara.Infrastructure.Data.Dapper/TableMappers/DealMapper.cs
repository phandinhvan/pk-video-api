﻿using Dapper.FluentMap.Dommel.Mapping;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers
{
    public class DealMapper : DommelEntityMap<Deal>
    {
        public DealMapper()
        {
            ToTable("deals");
            Map(p => p.ID).IsKey().IsIdentity();
        }
    }
}