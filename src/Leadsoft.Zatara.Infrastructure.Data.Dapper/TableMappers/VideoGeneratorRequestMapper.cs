﻿using Dapper.FluentMap.Dommel.Mapping;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers
{
    public class VideoGeneratorRequestMapper : DommelEntityMap<VideoGeneratorRequest>
    {
        public VideoGeneratorRequestMapper()
        {
            ToTable("videogeneratorrequests");
            Map(p => p.ID).IsKey().IsIdentity();
        }
    }
}