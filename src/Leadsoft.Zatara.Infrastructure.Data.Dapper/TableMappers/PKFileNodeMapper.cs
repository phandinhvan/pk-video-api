﻿using Dapper.FluentMap.Dommel.Mapping;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers
{
    public class PKFileNodeMapper : DommelEntityMap<PKFileNode>
    {
        public PKFileNodeMapper()
        {
            ToTable("T_PB_UFOT");
            Map(p => p.NODEDID).IsKey().IsIdentity();
        }
    }
}