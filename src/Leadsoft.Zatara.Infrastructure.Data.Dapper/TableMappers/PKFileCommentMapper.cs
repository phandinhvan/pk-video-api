﻿using Dapper.FluentMap.Dommel.Mapping;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers
{
    public class PKFileCommentMapper : DommelEntityMap<PKFileComment>
    {
        public PKFileCommentMapper()
        {
            ToTable("t_pb_fcmt");
            Map(p => p.COMMENTID).IsKey().IsIdentity();
        }
    }
}