﻿using Dapper.FluentMap.Dommel.Mapping;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers
{
    public class ObjectBucketMapper : DommelEntityMap<ObjectBucket>
    {
        public ObjectBucketMapper()
        {
            //ToTable("objectbuckets");
            ToTable("T_PB_OJBK");
            Map(p => p.ID).IsKey().IsIdentity();
        }
    }
}