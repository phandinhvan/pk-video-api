﻿using Dapper.FluentMap.Dommel.Mapping;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers
{
    public class VideoTemplateMapper : DommelEntityMap<VideoTemplate>
    {
        public VideoTemplateMapper()
        {
            ToTable("videotemplates");
            Map(p => p.ID).IsKey().IsIdentity();
        }
    }
}