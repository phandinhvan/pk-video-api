﻿using Dapper.FluentMap.Dommel.Mapping;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers
{
    public class PKFileMapper : DommelEntityMap<PKFile>
    {
        public PKFileMapper()
        {
            ToTable("t_pb_ufmt");
            Map(p => p.FILEID).IsKey().IsIdentity();
        }
    }
}