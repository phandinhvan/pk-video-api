﻿using Dapper.FluentMap.Dommel.Mapping;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers
{
    public class PKUserMapper : DommelEntityMap<PKUser>
    {
        public PKUserMapper()
        {
            ToTable("T_CM_USMT");
            Map(p => p.USERID).IsKey().IsIdentity();
        }
    }
}