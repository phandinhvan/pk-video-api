﻿using Dapper.FluentMap.Dommel.Mapping;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers
{
    public class VideoTemplateItemMapper : DommelEntityMap<VideoTemplateItem>
    {
        public VideoTemplateItemMapper()
        {
            ToTable("videotemplateitems");
            Map(p => p.ID).IsKey().IsIdentity();
        }
    }
}