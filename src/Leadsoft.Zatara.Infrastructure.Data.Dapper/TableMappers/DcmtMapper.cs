﻿using Dapper.FluentMap.Dommel.Mapping;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers
{
    public class DcmtMapper : DommelEntityMap<Dcmt>
    {
        public DcmtMapper()
        {
            ToTable("T_CM_DCMT");
            Map(p => p.DEPTCODE).IsKey().IsIdentity();
        }
    }
}