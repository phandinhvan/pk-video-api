﻿using Dapper.FluentMap.Dommel.Mapping;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers
{
    public class ObjectStorageMapper : DommelEntityMap<ObjectStorage>
    {
        public ObjectStorageMapper()
        {
            //ToTable("objectstorages");
            ToTable("T_PB_OJST");
            Map(p => p.ID).IsKey().IsIdentity();
        }
    }
}