﻿using Base.Infrastructure.Data.Dapper;
using System.Data;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper
{
    public class ZataraMySqlConnectionFactory : MySqlConnectionFactory, IConnectionFactory
    {
        public ZataraMySqlConnectionFactory(string connectionString)
            : base(connectionString)
        {
        }
    }
}