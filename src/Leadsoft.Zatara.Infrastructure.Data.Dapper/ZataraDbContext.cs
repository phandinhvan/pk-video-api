﻿using Dapper.FluentMap;
using Dapper.FluentMap.Dommel;
using Dapper.FluentMap.Mapping;
using Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers;
using System;
using System.Linq;
using System.Reflection;

namespace Leadsoft.Zatara.Infrastructure.Data.Dapper
{
    public class ZataraDbContext
    {
        public ZataraDbContext()
        {
            FluentMapper.Initialize(config =>
            {
                //var lstMapperTypes = typeof(TinhMapper).Assembly.GetTypes().Where(t => t.Name.EndsWith("Mapper"));
                //foreach (var mapperType in lstMapperTypes)
                //{
                //    var mapper = Activator.CreateInstance(mapperType);

                //    MethodInfo method = config.GetType().GetMethod("AddMap");
                //    method = method.MakeGene  ricMethod(mapperType);
                //    method.Invoke(config, new object[] { mapper });
                //    //'Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers.AreaMapper' cannot be converted to type 'Dapper.FluentMap.Mapping.IEntityMap`1[Leadsoft.Zatara.Infrastructure.Data.Dapper.TableMappers.AreaMapper]'.'

                //}
                
                config.AddMap(new DealMapper());
                config.AddMap(new ObjectStorageMapper());
                config.AddMap(new VideoGeneratorRequestMapper());
                config.AddMap(new VideoTemplateMapper());
                config.AddMap(new VideoTemplateItemMapper());
                config.AddMap(new ObjectBucketMapper());
                config.AddMap(new PKFileMapper());
                config.AddMap(new PKFileCommentMapper());
                config.AddMap(new PKUserMapper());
                config.AddMap(new DcmtMapper());
                config.AddMap(new PKFileNodeMapper());

                config.ForDommel();
            });
        }
    }
}