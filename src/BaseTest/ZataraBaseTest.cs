﻿using Leadsoft.Zatara.Services.Implementation;

namespace BaseTest
{
    public class ZataraBaseTest
    {
        protected ZataraBootstrapper bootstrapper;

        public ZataraBaseTest()
        {
            bootstrapper = new ZataraBootstrapper();
        }
    }
}