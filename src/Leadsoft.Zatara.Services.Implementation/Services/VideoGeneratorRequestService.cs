﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class VideoGeneratorRequestService : BaseResourceService<VideoGeneratorRequest>, IVideoGeneratorRequestService
    {
        private readonly IVideoGeneratorRequestRepository _repo;

        public VideoGeneratorRequestService(LeadsoftUserContext userContext,
                                    IVideoGeneratorRequestRepository repo)
            : base(userContext, repo)
        {
            _repo = repo;
        }
        protected override async Task<TaskResult<long>> ValidateAsync(VideoGeneratorRequest entity)
        {
            // if (string.IsNullOrWhiteSpace(entity.NO))
            //     return await Task.FromResult(new FailTaskResult<long>("Vui lòng chọn Mã số"));

            // if (string.IsNullOrWhiteSpace(entity.NAME))
            //     return await Task.FromResult(new FailTaskResult<long>("Vui lòng chọn Tên"));

            return await Task.FromResult(new SuccessTaskResult<long>());
        }
    }
}