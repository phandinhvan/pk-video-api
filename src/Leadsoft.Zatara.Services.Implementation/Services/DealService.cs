﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class DealService : BaseResourceService<Deal>, IDealService
    {
        private readonly IDealRepository _repo;

        public DealService(LeadsoftUserContext userContext,
                                    IDealRepository repo)
            : base(userContext, repo)
        {
            _repo = repo;
        }

        protected override async Task<TaskResult<long>> ValidateAsync(Deal entity)
        {
            // if (string.IsNullOrWhiteSpace(entity.NO))
            //     return await Task.FromResult(new FailTaskResult<long>("Vui lòng chọn Mã số"));

            // if (string.IsNullOrWhiteSpace(entity.NAME))
            //     return await Task.FromResult(new FailTaskResult<long>("Vui lòng chọn Tên"));

            return await Task.FromResult(new SuccessTaskResult<long>());
        }
    }
}