﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using Leadsoft.Common;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class ObjectBucketService : BaseResourceService<ObjectBucket>, IObjectBucketService
    {
        private readonly IObjectBucketRepository _repo;

        public ObjectBucketService(LeadsoftUserContext userContext, IObjectBucketRepository repo)
            : base(userContext, repo)
        {
            _repo = repo;
        }

        public async Task<ObjectBucket> CheckExistAndCreateBucketAsync(string bucketKey, bool isPublic = true)
        {
            var bucketInfo = await _repo.GetObjectByKeyAsync(bucketKey);
            if (bucketInfo != null)
                return bucketInfo;

            bucketInfo = new ObjectBucket() {
                BUCKETKEY = bucketKey,
                NAME = bucketKey,
                ISPUBLIC = isPublic == true ? 1 : 0
            };

            bucketInfo =  await _repo.AddAsync(bucketInfo);

            return bucketInfo;
        }

        public async Task<TaskResult<ObjectBucket>> GetObjectBucketByKeyAsync(string bucketKey)
        {
            var bucketInfo = await _repo.GetObjectByKeyAsync(bucketKey);
            if (bucketInfo == null)
                return new FailTaskResult<ObjectBucket>($@"Bucket Key {bucketKey} not existed");

            return new SuccessTaskResult<ObjectBucket>(bucketInfo);
        }

        protected override async Task<TaskResult<long>> ValidateAsync(ObjectBucket entity)
        {
            if (string.IsNullOrWhiteSpace(entity.BUCKETKEY))
                return await Task.FromResult(new FailTaskResult<long>("Invalid KEY"));

            // if (string.IsNullOrWhiteSpace(entity.NAME))
            //     return await Task.FromResult(new FailTaskResult<long>("Vui lòng chọn Tên"));


            entity.BUCKETKEY = StringHelper.RemoveNonPrinableCharacters(entity.BUCKETKEY);
            entity.NAME = StringHelper.RemoveNonPrinableCharacters(entity.NAME);

            return await Task.FromResult(new SuccessTaskResult<long>());
        }
    }
}