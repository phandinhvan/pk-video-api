﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using FluentFTP;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Interface.Dto;
using Leadsoft.Zatara.Services.Interface;
using Serilog;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class FtpFileService : IFtpFileService
    {
        private readonly IConfigService _configService;
        private readonly FtpSetting _ftpSetting;
        public FtpFileService(IConfigService configService)
        {
            _configService = configService;
            _ftpSetting = _configService.GetFtpSetting();
        }

        FtpClient CreateFtpClient()
        {
            var ftpSetting = _configService.GetFtpSetting();
            
            var client = new FtpClient(ftpSetting.Host, ftpSetting.Port, ftpSetting.UserName, ftpSetting.Password);
            // client.Credentials = new NetworkCredential(ftpSetting.UserName, ftpSetting.Password);

            return client;
        }

        public void TryDelete(string relativeFilePath)
        {
            try
            {
                var client = CreateFtpClient();
                client.Connect();
                client.SetWorkingDirectory(_ftpSetting.WorkingDir);
                client.DeleteFile(relativeFilePath);
                client.Disconnect();
            }
            catch (System.Exception ex)
            {
            }
        }

        public async Task<Stream> ReadAsync(string relativeFilePath)
        {
            var client = CreateFtpClient();
            await client.ConnectAsync();
            client.SetWorkingDirectory(_ftpSetting.WorkingDir);

            return await client.OpenReadAsync(relativeFilePath, FtpDataType.Binary, 0, true);
        }

        public async Task WriteAsync(Stream stream, string relativeFilePath)
        {
            var client = CreateFtpClient();
            await client.ConnectAsync();
            client.SetWorkingDirectory(_ftpSetting.WorkingDir);

            await client.UploadAsync(stream, relativeFilePath, FtpExists.Overwrite, true);
        }

        public async Task UploadFileAsync(string localFilePath, string remoteFilePath)
        {
            try
            {
                var client = CreateFtpClient();
                await client.ConnectAsync();
                client.SetWorkingDirectory(_ftpSetting.WorkingDir);
                var abc = await client.UploadFileAsync(localFilePath, remoteFilePath, FtpExists.Overwrite, createRemoteDir: true);
                await client.DisconnectAsync();
            } catch(Exception ex)
            {
                var err = ex.Message;
            }
        }
    }
}