﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Interface.Dto;
using Leadsoft.Zatara.Services.Interface;
using System.IO;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class ConfigService : IConfigService
    {
        private readonly LocalStorageSetting _storageSetting;
        private readonly FtpSetting _ftpSetting;
        private readonly VideoServiceSetting _videoServiceSetting;

        public bool UsingFtpStorageOnly => _videoServiceSetting.UsingFtpStorageOnly;

        public ConfigService(VideoServiceSetting videoServiceSetting, LocalStorageSetting storageSetting, FtpSetting ftpSetting)
        {
            _videoServiceSetting = videoServiceSetting;
            _storageSetting = storageSetting;
            _ftpSetting = ftpSetting;
        }

        public string GetFFMPEGPath()
        {
            return _storageSetting.FFMPEGPath;
        }

        public string GetObjectStorageRootPath()
        {
            return _storageSetting.RootPath;
        }

        public FtpSetting GetFtpSetting()
        {
            return _ftpSetting;
        }


        public string GetTempUploadFolder()
        {
            return _storageSetting.TempUploadFolder;
        }
    }
}