﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using Leadsoft.Common;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using Leadsoft.Zatara.Services.Interface;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class PKFileService : IPKFileService
    {
        private readonly IPKFileRepository _repo;
        private readonly IPKFileCommentRepository _fileCommentRepo;
        private readonly IConfigService _configService;
        private readonly IPersistentFileService _persistentFileService;
        private readonly IFtpFileService _ftpFileService;


        private readonly IVideoProcessingService _videoProcessingService;
        private readonly LeadsoftUserContext _userContext;

        public PKFileService(LeadsoftUserContext userContext,
                            IPKFileRepository repo,
                            IPKFileCommentRepository fileCommentRepo,
                            IConfigService configService,
                            IPersistentFileService persistentFileService,
                            IVideoProcessingService videoProcessingService,
                            IFtpFileService ftpFileService)
        {
            _userContext = userContext;
            _repo = repo;
            _fileCommentRepo = fileCommentRepo;
            _configService = configService;
            _persistentFileService = persistentFileService;
            _videoProcessingService = videoProcessingService;
            _ftpFileService = ftpFileService;
        }

        public async Task<PagingResult<PKFileDto>> GetListPagingAsync(GetVideoPagingRequest request)
        {
            var result = await _repo.GetListPagingResultAsync(request);
            var lst = new List<PKFileDto>();

            foreach (var dto in result.Result)
            {
                CalcPKFileDto(dto);
                lst.Add(dto);
            }

            result.Result = lst;
            return result;
        }

        public async Task<TaskResult<PKFileDto>> GetDtoAsync(string fileId)
        {
            var dto = await _repo.GetDtoByIdAsync(fileId);
            if (dto == null)
                return new FailTaskResult<PKFileDto>($@"File Id {fileId} not existed");

            CalcPKFileDto(dto);

            return new SuccessTaskResult<PKFileDto>(dto);
        }

        public async Task<TaskResult<string>> DeleteAsync(string fileId)
        {
            var dto = await _repo.GetDtoByIdAsync(fileId);
            if (dto == null)
                return new FailTaskResult<string>($@"File Id {fileId} not existed");
            try
            {
                // delete file share
                if(dto.FILETYPE == "link")
                {
                    await _repo.DeleteByIdAsync(fileId);
                } else
                {
                    // delete local file first
                    var bucketName = $@"{dto.COMPANY_NAME}{dto.DEPARTMENT}";
                    _persistentFileService.TryDelete(bucketName, dto.FILENAMESYS);

                    // delete on db
                    await _repo.DeleteByIdAsync(fileId);
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }


            return new SuccessTaskResult<string>(dto.FILEID);
        }

        protected void CalcPKFileDto(PKFileDto dto)
        {
            // if (dto.FILEID.Length > 20)//new video
            //     dto.URL = $@"http://203.113.146.146:8884/pkvideos/{dto.FILEID}";
            // else

            dto.URL = $@"http://video.pungkookvn.com:9000/{dto.CORPORATION}{dto.DEPARTMENT}/{dto.FILENAMESYS}";

            if (dto.PUBLICHCHECK == "Y")
            {
                dto.PRIVACY_CODE = "Public";
                dto.PRIVACY_NAME = "Public";
            }
            else if (dto.DEPARTCHECK == "Y")
            {
                dto.PRIVACY_CODE = "Department";
                dto.PRIVACY_NAME = "Department only";
            }
            else if (dto.PRIVATECHECK == "Y")
            {
                dto.PRIVACY_CODE = "Private";
                dto.PRIVACY_NAME = "Private";
            }

            /*----- Code by Dinh Van -----*/
            /*----- Begin add URLImage -----*/
            if (dto.FILENAMESYS != null)
            {
                string imageName = "";
                if (dto.FILENAMESYS.Contains(".mp4"))
                {
                    imageName = dto.FILENAMESYS.Replace(".mp4", ".jpeg");
                }
                else if (dto.FILENAMESYS.Contains(".MOV"))
                {
                    imageName = dto.FILENAMESYS.Replace(".MOV", ".jpeg");
                }
                else if (dto.FILENAMESYS.Contains(".wmv"))
                {
                    imageName = dto.FILENAMESYS.Replace(".wmv", ".jpeg");
                }
                else if (dto.FILENAMESYS.Contains(".avi"))
                {
                    imageName = dto.FILENAMESYS.Replace(".avi", ".jpeg");
                }
                else if (dto.FILENAMESYS.Contains(".MP4"))
                {
                    imageName = dto.FILENAMESYS.Replace(".", ".jpeg");
                }
                else
                {
                    imageName = "abc.jpeg";
                }

                string url_image = $@"http://video.pungkookvn.com:9000/{dto.CORPORATION}{dto.DEPARTMENT}/{imageName}";

                HttpWebResponse res = null;
                var request = (HttpWebRequest)WebRequest.Create(url_image);
                request.Method = "HEAD";

                //Check url have exists
                try
                {
                    res = (HttpWebResponse)request.GetResponse();
                }
                catch (WebException ex)
                {
                    /* A WebException will be thrown if the status of the response is not `200 OK` */
                    var err = ex.Message;
                }
                finally
                {
                    // Check response.
                    if (res != null)
                    {
                        dto.URLImage = url_image;
                    }
                    else
                    {
                        dto.URLImage = "";
                    }
                }
            }
            else
            {
                dto.URLImage = "";
            }
            /*----- End add URLImage -----*/

            /*----- Begin call function calculate time upload -----*/
            DateTime current = DateTime.Now;
            TimeSpan diff1 = current.Subtract(dto.UPLOADDATE);
            dto.TIME_UPLOADDATE = CalculateTimeUpload(diff1);
            /*----- End call function calculate time upload -----*/
        }

        public async Task<PagingResult<PKFileComment>> GetListCommentPagingAsync(GetCommentPagingRequest request)
        {
            return await _fileCommentRepo.GetListPagingAsync(request);
        }


        public async Task<TaskResult<PKFileDto>> AddChunkObjectAsync(AddChunkObjectStorageRequest request)
        {
            try
            {
                //test
                // await _ftpFileService.UploadFileAsync("/Users/macbook/Documents/bcons1.jpg", "test.jpg");

                var validateResult = await ValidateAsync(request);
                if (validateResult.IsSuccess == false)
                    return new FailTaskResult<PKFileDto>(validateResult.Log);

                //abc.mp4.part1-3
                //abc.mp4.part2-3
                //abc.mp4.part3-3
                //GET BASE FILE FIRST
                var baseFileName = GetBaseFileNameFromTheChunkFileName(request.FileName);
                var fileExtension = Path.GetExtension(request.FileName);

                //we store file in random name
                var actualStorageFileName = request.Key + fileExtension;//just random file name
                var imageName = request.Key + ".jpeg";
                var chunkFileName = $@"{actualStorageFileName}.part{request.PartIndex}-{request.TotalPart}";

                //save chunk file to temp folder
                var folderName = $@"{request.CompanyCode}{request.TeamCode}";
                //var tempFolderPath = _configService.GetTempUploadFolder();
                var tempFolderPath = _configService.GetObjectStorageRootPath();

                var chunkFilePath = Path.Combine(tempFolderPath, folderName, chunkFileName);

                //delete first
                if (File.Exists(chunkFilePath))
                    File.Delete(chunkFilePath);

                //save to temp
                //make sure folder existed
                var chunkFileInfo = new FileInfo(chunkFilePath);
                if (chunkFileInfo.Directory.Exists == false)
                    chunkFileInfo.Directory.Create();
                using (var chunkFileStream = System.IO.File.Create(chunkFilePath))
                {
                    request.DataStream.Seek(0, SeekOrigin.Begin);
                    request.DataStream.CopyTo(chunkFileStream);
                    await chunkFileStream.FlushAsync();
                }

                var bucketName = $@"{request.CompanyCode}{request.TeamCode}";
                var actualStorageFilePath = _persistentFileService.GetAbsoluteFilePath(bucketName, actualStorageFileName);
                var actualRelativeFilePath = _persistentFileService.GetRelativeFilePath(bucketName, actualStorageFileName);

                var actualStorageFilePathImage = _persistentFileService.GetAbsoluteFilePath(bucketName, imageName);
                var actualRelativeFilePathImage = _persistentFileService.GetRelativeFilePath(bucketName, imageName);

                //check to merge, save to destination path
                var isMergeSuccess = await CheckAndMergeChunkFilesAsync(chunkFilePath, actualStorageFilePath, request.Catalogue);

                PKFile pkFileInfo = null;
                //add file info
                if (isMergeSuccess == true)
                {
                    if (_configService.UsingFtpStorageOnly == true)
                    {
                        // upload file video to ftp
                        await _ftpFileService.UploadFileAsync(actualStorageFilePath, actualRelativeFilePath);
                        // upload image to ftp
                        if (request.Catalogue == "video")
                        {
                            string image = @"D:\objectstorageroot\" + folderName + @"\" + imageName;
                            string video = @"D:\objectstorageroot\" + actualRelativeFilePath;
                            GetThumbnail(video, image);
                            await _ftpFileService.UploadFileAsync(actualStorageFilePathImage, actualRelativeFilePathImage);

                        }
                        // delete local file
                        File.Delete(actualStorageFilePathImage);
                        File.Delete(actualStorageFilePath);
                    }


                    pkFileInfo = new PKFile()
                    {
                        CORPORATION = request.CompanyCode,
                        DEPARTMENT = request.TeamCode,
                        FILEID = Guid.NewGuid().ToString(),
                        PRIVATECHECK = request.IsPrivated == true ? "Y" : "N",
                        PUBLICHCHECK = request.IsPublic == true ? "Y" : "N",
                        DEPARTCHECK = request.IsOnlyForDepartment == true ? "Y" : "N",
                        FILENAMESYS = actualStorageFileName,
                        FILENAME = request.Title,
                        CONTENTTYPE = MimeTypes.GetMimeType(request.FileName), //???
                        FILETYPE = fileExtension,
                        FILEPATH = _persistentFileService.GetRelativeFilePath(bucketName, actualStorageFileName),
                        // FILEPATH = _persistentFileService.GetAbsoluteFilePath(bucketName, actualStorageFileName),
                        UPLOADDATE = DateTime.Now,
                        UPLOADID = _userContext.UserName.ToString(),
                        PRODUCT = "N",//???
                        CATALOGUE = request.Catalogue,
                        CATEGORY = request.Category
                    };

                    //add
                    pkFileInfo = await _repo.AddAsync(pkFileInfo);
                }

                return await GetDtoAsync(pkFileInfo.FILEID);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }

        }


        //abc.mp4.part1-3
        //abc.mp4.part2-3
        //abc.mp4.part3-3
        protected async Task<bool> CheckAndMergeChunkFilesAsync(string oneChunkFilePath, string destinationFilePath, string catalogue)
        {
            FileInfo chunkFileInfo = new FileInfo(oneChunkFilePath);
            var directoryInfo = chunkFileInfo.Directory;
            var baseFolderPath = directoryInfo.FullName;
            var totalPart = GetTotalFilePartOfChunk(oneChunkFilePath);

            var fileNameWithoutChunkExtension = GetBaseFileNameFromTheChunkFileName(chunkFileInfo.Name);

            var lstFile = directoryInfo.GetFiles();
            if (lstFile == null || lstFile.Length < totalPart) return false;

            List<string> lstChunkFilePaths = new List<string>();
            //check all part completed
            for (int i = 1; i <= totalPart; i++)
            {
                var chunkFileName = $@"{fileNameWithoutChunkExtension}.part{i}-{totalPart}";
                var chunkFilePath = Path.Combine(baseFolderPath, chunkFileName);
                if (File.Exists(chunkFilePath) == false)//lack file
                    return false;

                lstChunkFilePaths.Add(chunkFilePath);
            }

            var tempDestinationPath = $@"{destinationFilePath}.temp";

            //merge
            using (var outputStream = File.Create(tempDestinationPath))
            {
                foreach (var chunkFilePath in lstChunkFilePaths)
                {
                    using (var chunkStream = File.OpenRead(chunkFilePath))
                    {
                        // Buffer size can be passed as the second argument.
                        await chunkStream.CopyToAsync(outputStream);
                    }
                    // Console.WriteLine("The file {0} has been processed.", inputFilePath);
                }
            }

            //delete all chunk files
            foreach (var chunkFilePath in lstChunkFilePaths)
            {
                try
                {
                    File.Delete(chunkFilePath);
                }
                catch (System.Exception)
                {
                }
            }

            if (catalogue == "video")
            {
                //convert to viewable format
                _videoProcessingService.ConvertVideo(tempDestinationPath, destinationFilePath);
            }
            else
            {
                //convert to viewable format
                File.Copy(tempDestinationPath, destinationFilePath, true);
            }

            //delete temp file
            File.Delete(tempDestinationPath);

            return true;
        }

        public async Task<TaskResult<Stream>> GetFileDataByIdAsync(string fileId)
        {
            var fileInfo = await _repo.FindByIdAsync(fileId);
            if (fileInfo == null)
                return new FailTaskResult<Stream>($@"File Id {fileId} not found");

            var absoluteFilePath = GetAbsoluteFilePath(fileInfo);

            var stream = await _persistentFileService.ReadAsync(absoluteFilePath);

            return new SuccessTaskResult<Stream>(stream);
        }

        public async Task<TaskResult<PKFile>> GetFileInfoByIdAsync(string fileId)
        {
            var fileInfo = await _repo.FindByIdAsync(fileId);
            if (fileInfo == null)
                return new FailTaskResult<PKFile>($@"File Id {fileId} not found");

            return new SuccessTaskResult<PKFile>(fileInfo);
        }

        protected string GetFileRelativePath(PKFile fileInfo)
        {
            var folderName = $@"{fileInfo.CORPORATION}{fileInfo.DEPARTMENT}";
            return Path.Combine(folderName, fileInfo.FILENAMESYS);
        }

        protected string GetAbsoluteFilePath(PKFile fileInfo)
        {
            // should combine using relative path
            return _persistentFileService.GetRelativeFilePath($@"{fileInfo.CORPORATION}{fileInfo.DEPARTMENT}", fileInfo.FILENAMESYS);

            // return fileInfo.FILEPATH;
        }

        //abc.mp4.part1-3
        protected string GetBaseFileNameFromTheChunkFileName(string chunkFileName)
        {
            var lastDotIndex = chunkFileName.LastIndexOf('.');
            return chunkFileName.Substring(0, lastDotIndex);
        }

        protected int GetTotalFilePartOfChunk(string chunkFileName)
        {
            var lst = chunkFileName.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            var lastPart = lst.LastOrDefault();
            int totalPart = 1;
            int.TryParse(lastPart, out totalPart);

            return totalPart;
        }

        protected async Task<TaskResult<long>> ValidateAsync(AddChunkObjectStorageRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Key))
                return await Task.FromResult(new FailTaskResult<long>("Key cannot empty"));

            if (string.IsNullOrWhiteSpace(request.FileName))
                return await Task.FromResult(new FailTaskResult<long>("FileName cannot empty"));

            if (string.IsNullOrWhiteSpace(request.Title))
                return await Task.FromResult(new FailTaskResult<long>("Title cannot empty"));

            if (string.IsNullOrWhiteSpace(request.CompanyCode) || string.IsNullOrWhiteSpace(request.TeamCode))
                return await Task.FromResult(new FailTaskResult<long>("CompanyCode and TeamCode cannot empty"));

            request.FileName = StringHelper.RemoveNonPrinableCharacters(request.FileName);
            request.Title = StringHelper.RemoveNonPrinableCharacters(request.Title);

            return await Task.FromResult(new SuccessTaskResult<long>());
        }

        public async Task<TaskResult<string>> UpdateAsync(UpdateFileRequest request)
        {
            var validateResult = await ValidateAsync(request);
            if (validateResult.IsSuccess == false)
                return validateResult;

            var entity = await _repo.FindByIdAsync(request.fileid);
            if (entity == null)
                return new FailTaskResult<string>($@"File id {request.fileid} not found");

            entity.FILENAME = request.title;
            entity.CORPORATION = request.companycode;
            entity.DEPARTMENT = request.departmentcode;

            if (string.IsNullOrWhiteSpace(request.privacy) == false)
            {
                if (request.privacy.ToUpper() == "PUBLIC")
                {
                    entity.PUBLICHCHECK = "Y";
                    entity.PRIVATECHECK = "N";
                    entity.DEPARTCHECK = "N";
                }
                else if (request.privacy.ToUpper() == "PRIVATE")
                {
                    entity.PUBLICHCHECK = "N";
                    entity.PRIVATECHECK = "Y";
                    entity.DEPARTCHECK = "N";
                }
                else if (request.privacy.ToUpper() == "DEPARTMENT")
                {
                    entity.PUBLICHCHECK = "N";
                    entity.PRIVATECHECK = "N";
                    entity.DEPARTCHECK = "Y";
                }
            }

            await _repo.UpdateAsync(entity);

            return new SuccessTaskResult<string>();
        }

        async Task<TaskResult<string>> ValidateAsync(UpdateFileRequest request)
        {
            if (string.IsNullOrEmpty(request.title))
                return new FailTaskResult<string>($@"Please input title");
            if (string.IsNullOrEmpty(request.companycode))
                return new FailTaskResult<string>($@"Please input company");
            if (string.IsNullOrEmpty(request.departmentcode))
                return new FailTaskResult<string>($@"Please input department");
            if (string.IsNullOrEmpty(request.privacy))
                return new FailTaskResult<string>($@"Please input privacy");

            return await Task.FromResult(new SuccessTaskResult<string>());
        }

        async Task<TaskResult<string>> ValidateAsync(ShareLinkRequest request)
        {
            if (string.IsNullOrEmpty(request.Link))
                return new FailTaskResult<string>($@"Please input link");
            if (string.IsNullOrEmpty(request.Title))
                return new FailTaskResult<string>($@"Please input title");
            if (string.IsNullOrEmpty(request.CompanyCode))
                return new FailTaskResult<string>($@"Please input company");
            if (string.IsNullOrEmpty(request.DepartmentCode))
                return new FailTaskResult<string>($@"Please input department");
            if (string.IsNullOrEmpty(request.Privacy))
                return new FailTaskResult<string>($@"Please input privacy");

            return await Task.FromResult(new SuccessTaskResult<string>());
        }

        public async Task<TaskResult<PKFileDto>> ShareLinkAsync(ShareLinkRequest request)
        {
            var validateResult = await ValidateAsync(request);
            if (validateResult.IsSuccess == false)
                return new FailTaskResult<PKFileDto>(validateResult.Log);

            var isPublic = request.Privacy.ToUpper().Contains("PUBLIC");
            var isPrivate = request.Privacy.ToUpper().Contains("PRIVATE");
            var isDepartment = request.Privacy.ToUpper().Contains("DEPARTMENT");

            var pkFileInfo = new PKFile()
            {
                CORPORATION = request.CompanyCode,
                DEPARTMENT = request.DepartmentCode,
                FILEID = Guid.NewGuid().ToString(),
                PRIVATECHECK = isPrivate == true ? "Y" : "N",
                PUBLICHCHECK = isPublic == true ? "Y" : "N",
                DEPARTCHECK = isDepartment == true ? "Y" : "N",
                // FILENAMESYS = actualStorageFileName,
                FILENAME = request.Title,
                // CONTENTTYPE = MimeTypes.GetMimeType(request.FileName), //???
                FILETYPE = "link",
                //FILEPATH =  _localFileService.GetRelativeFilePath(bucketName, actualStorageFileName),
                FILEPATH = request.Link,
                UPLOADDATE = DateTime.Now,
                UPLOADID = _userContext.UserName.ToString(),
                PRODUCT = "N",//???
                CATALOGUE = request.Catalogue,
            };

            //add
            pkFileInfo = await _repo.AddAsync(pkFileInfo);

            return await GetDtoAsync(pkFileInfo.FILEID);
        }

        /*----- Code by Dinh Van -----*/
        /*----- Begin get image from video -----*/
        public static Bitmap GetThumbnail(string video, string thumbnail)
        {
            var cmd = "ffmpeg  -itsoffset -1  -i " + '"' + video + '"' + " -vcodec mjpeg -vframes 1 -an -f rawvideo -s 250x150 " + '"' + thumbnail + '"';
            try
            {
                Process p;

                p = new Process();
                ProcessStartInfo info = new ProcessStartInfo();

                info.FileName = "cmd.exe";
                info.UseShellExecute = false;
                info.RedirectStandardOutput = true;
                info.RedirectStandardInput = true;
                info.RedirectStandardError = true;

                p.StartInfo = info;
                p.Start();

                using (StreamWriter sw = p.StandardInput)
                {
                    if (sw.BaseStream.CanWrite)
                    {
                        sw.WriteLine(@"cd /d D:\ffmpeg\");
                        //run cmd
                        sw.WriteLine(cmd);
                    }
                }

                string output = p.StandardOutput.ReadToEnd();

                p.WaitForExit();


            }
            catch (Exception ex)
            {
                var mes = ex.Message;
            }

            return LoadImage(thumbnail);
        }

        static Bitmap LoadImage(string path)
        {
            var ms = new MemoryStream(File.ReadAllBytes(path));
            return (Bitmap)Image.FromStream(ms);
        }

        /*----- End get image from video -----*/

        /*----- Begin get get list of Manage style files -----*/
        public async Task<PagingResult<ManageStyleDto>> GetListManageStylePagingAsync(GetVideoManageStylePagingRequest request)
        {
            var result = await _repo.GetListManageStylePagingResultAsync(request);
            var lst = new List<ManageStyleDto>();

            foreach (var dto in result.Result)
            {
                lst.Add(dto);
            }

            result.Result = lst;
            return result;
        }
        /*----- End get get list of Manage style files -----*/

        /*----- Begin get get list video of Manage style files -----*/
        public async Task<PagingResult<ManageStyleVideoDto>> GetListVideoManageStyleAsync(GetListVideoManageStylePagingRequest request)
        {
            var result = await _repo.GetListVideoManageStylePagingResultAsync(request);
            var lst = new List<ManageStyleVideoDto>();


            foreach (var dto in result.Result)
            {
                dto.URL = $@"http://video.pungkookvn.com:9000/{dto.CORPORATION}{dto.DEPARTMENT}/{dto.FILENAMESYS}";
                lst.Add(dto);
            }

            result.Result = lst;
            return result;
        }
        /*----- End get get list video of Manage style files -----*/

        /*----- Begin calculate time upload page Home -----*/
        protected string CalculateTimeUpload(TimeSpan item)
        {
            int days = item.Days;
            int hours = item.Hours;
            int minutes = item.Minutes;
            int seconds = item.Seconds;
            if (days > 0)
            {
                if (days >= 365)
                {
                    int year = days / 365;
                    if (year == 1)
                    {
                        return year + " year ago";
                    }
                    else
                    {
                        return year + " years ago";
                    }
                }
                else if (days >= 30)
                {
                    int month = days / 30;
                    if (month == 1)
                    {
                        return month + " month ago";
                    }
                    else
                    {
                        return month + " months ago";
                    }
                }
                else if (days >= 7)
                {
                    int week = days / 7;
                    if (week == 1)
                    {
                        return week + " week ago";
                    }
                    else
                    {
                        return week + " weeks ago";
                    }
                }
                else
                {
                    if (days == 1)
                    {
                        return days + " day ago";
                    }
                    else
                    {
                        return days + " days ago";
                    }
                }
            }
            else
            {
                if (hours > 0)
                {
                    if (hours == 1)
                    {
                        return hours + " hour ago";
                    }
                    else
                    {
                        return hours + " hours ago";
                    }
                }
                else if (minutes > 0)
                {
                    if (minutes == 1)
                    {
                        return minutes + " minute ago";
                    }
                    else
                    {
                        return minutes + " minutes ago";
                    }
                }
                else
                {
                    return seconds + " seconds ago";
                }
            }
        }

        /*----- End calculate time upload page Home -----*/

        /*----- Begin get list project name menu setups -----*/
        public async Task<PagingResult<SetupDto>> GetListProjectSetupAsync(GetListProjectSetupPagingRequest request)
        {
            var result = await _repo.GetListProjectNamePagingResultAsync(request);
            var lst = new List<SetupDto>();


            foreach (var dto in result.Result)
            {
                lst.Add(dto);
            }

            result.Result = lst;
            return result;
        }

        /*----- Begin get list project name menu setups -----*/

        /*----- Begin get list item project name menu setups -----*/
        public async Task<PagingResult<SetupDto>> GetListItemProjectAsync(GetListItemProjectNamePagingResult request)
        {
            var result = await _repo.GetListItemProjectNamePagingResultAsync(request);
            var lst = new List<SetupDto>();


            foreach (var dto in result.Result)
            {
                lst.Add(dto);
            }

            result.Result = lst;
            return result;
        }

        /*----- Begin get list project name menu setups -----*/

        public async Task<TaskResult<PKFileDto>> AddMoreVideoAsync(AddChunkObjectStorageRequest request)
        {
            try
            {
                //test
                // await _ftpFileService.UploadFileAsync("/Users/macbook/Documents/bcons1.jpg", "test.jpg");

                var validateResult = await ValidateAsync(request);
                if (validateResult.IsSuccess == false)
                    return new FailTaskResult<PKFileDto>(validateResult.Log);

                //abc.mp4.part1-3
                //abc.mp4.part2-3
                //abc.mp4.part3-3
                //GET BASE FILE FIRST
                var baseFileName = GetBaseFileNameFromTheChunkFileName(request.FileName);
                var fileExtension = Path.GetExtension(request.FileName);

                //we store file in random name
                var actualStorageFileName = request.Key + fileExtension;//just random file name
                var actualStorageFileNameCrop = request.Key + "-1" + fileExtension;
                var imageName = request.Key + "-1" + ".jpeg";
                var chunkFileName = $@"{actualStorageFileName}.part{request.PartIndex}-{request.TotalPart}";

                //save chunk file to temp folder
                var folderName = $@"{request.CompanyCode}{request.TeamCode}";
                var timeStart = request.TimeStart;
                var timeEnd = request.TimeEnd;
                //var tempFolderPath = _configService.GetTempUploadFolder();
                var tempFolderPath = _configService.GetObjectStorageRootPath();

                var chunkFilePath = Path.Combine(tempFolderPath, folderName, chunkFileName);

                //delete first
                if (File.Exists(chunkFilePath))
                    File.Delete(chunkFilePath);

                //save to temp
                //make sure folder existed
                var chunkFileInfo = new FileInfo(chunkFilePath);
                if (chunkFileInfo.Directory.Exists == false)
                    chunkFileInfo.Directory.Create();
                using (var chunkFileStream = System.IO.File.Create(chunkFilePath))
                {
                    request.DataStream.Seek(0, SeekOrigin.Begin);
                    request.DataStream.CopyTo(chunkFileStream);
                    await chunkFileStream.FlushAsync();
                }

                var bucketName = $@"{request.CompanyCode}{request.TeamCode}";
                var actualStorageFilePath = _persistentFileService.GetAbsoluteFilePath(bucketName, actualStorageFileName);
                var actualRelativeFilePath = _persistentFileService.GetRelativeFilePath(bucketName, actualStorageFileName);

                var actualStorageFilePathImage = _persistentFileService.GetAbsoluteFilePath(bucketName, imageName);
                var actualRelativeFilePathImage = _persistentFileService.GetRelativeFilePath(bucketName, imageName);

                //check to merge, save to destination path
                var isMergeSuccess = await CheckAndMergeChunkFilesAsync(chunkFilePath, actualStorageFilePath, request.Catalogue);

                PKFile pkFileInfo = null;
                //add file info
                if (isMergeSuccess == true)
                {
                    if (_configService.UsingFtpStorageOnly == true)
                    {
                        string inputFilePath = @"D:\objectstorageroot\" + folderName + @"\" + actualStorageFileName;
                        string outputFilePath = @"D:\objectstorageroot\" + folderName + @"\" + actualStorageFileNameCrop;

                        var actualStorageFilePath_Crop = _persistentFileService.GetAbsoluteFilePath(bucketName, actualStorageFileNameCrop);
                        var actualRelativeFilePath_Crop = _persistentFileService.GetRelativeFilePath(bucketName, actualStorageFileNameCrop);

                        CropVideo(inputFilePath, outputFilePath, timeStart, timeEnd);
                        // upload file video to ftp
                        await _ftpFileService.UploadFileAsync(actualStorageFilePath_Crop, actualRelativeFilePath_Crop);
                        // upload image to ftp
                        if (request.Catalogue == "video")
                        {

                            string image = @"D:\objectstorageroot\" + folderName + @"\" + imageName;
                            string video = @"D:\objectstorageroot\" + actualRelativeFilePath_Crop;
                            GetThumbnail(video, image);
                            await _ftpFileService.UploadFileAsync(actualStorageFilePathImage, actualRelativeFilePathImage);

                        }
                        // delete local file
                        File.Delete(actualStorageFilePathImage);
                        File.Delete(actualStorageFilePath);
                        File.Delete(actualStorageFilePath_Crop);
                    }


                    pkFileInfo = new PKFile()
                    {
                        CORPORATION = request.CompanyCode,
                        DEPARTMENT = request.TeamCode,
                        FILEID = Guid.NewGuid().ToString(),
                        PRIVATECHECK = request.IsPrivated == true ? "Y" : "N",
                        PUBLICHCHECK = request.IsPublic == true ? "Y" : "N",
                        DEPARTCHECK = request.IsOnlyForDepartment == true ? "Y" : "N",
                        FILENAMESYS = actualStorageFileNameCrop,
                        FILENAME = request.Title,
                        CONTENTTYPE = MimeTypes.GetMimeType(request.FileName), //???
                        FILETYPE = fileExtension,
                        FILEPATH = _persistentFileService.GetRelativeFilePath(bucketName, actualStorageFileNameCrop),
                        UPLOADDATE = DateTime.Now,
                        UPLOADID = _userContext.UserName.ToString(),
                        PRODUCT = "N",//???
                        CATALOGUE = request.Catalogue,
                        CATEGORY = request.Category
                    };

                    //add
                    pkFileInfo = await _repo.AddAsync(pkFileInfo);
                }

                return await GetDtoAsync(pkFileInfo.FILEID);
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }

        }

        public void CropVideo(string inputFilePath, string outputFilePath, string timeStart, string timeEnd)
        {
            try
            {
                var ffmpegFilePath = _configService.GetFFMPEGPath();
                Log.Logger.Information($@"Input file crop: {inputFilePath}");
                Log.Logger.Information($@"Output file crop: {outputFilePath}");
                string strCmdText;
                strCmdText = $@"-i {inputFilePath} -ss {timeStart} -to {timeEnd} -async 1 {outputFilePath}";
                var process = System.Diagnostics.Process.Start(ffmpegFilePath, strCmdText);
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                var mes = ex.Message;
            }

        }

        
        public async Task<PagingResult<PKFileDto>> GetNewVideoPagingAsync()
        {
            var result = await _repo.GetNewVideoPagingResultAsync();
            DateTime current = DateTime.Now;
            foreach (var dto in result.Result)
            {
                dto.URL = $@"http://video.pungkookvn.com:9000/{dto.CORPORATION}{dto.DEPARTMENT}/{dto.FILENAMESYS}";
                
                TimeSpan diff1 = current.Subtract(dto.UPLOADDATE);
                dto.TIME_UPLOADDATE = CalculateTimeUpload(diff1);
            }
            return result;
        }
    }
}