﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Interface.Dto;
using Leadsoft.Zatara.Services.Interface;
using Serilog;
using System.IO;
using System.Threading.Tasks;
using YoutubeExplode;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class YoutubeService : IYoutubeService
    {

        private readonly YoutubeClient _youtubeClient = new YoutubeClient();
        public async Task<TaskResult<YoutubeVideoInfo>> GetVideoInfoAsync(string youtubeLink)
        {
            try
            {
                var id = YoutubeClient.ParseVideoId(youtubeLink);

                var video = await _youtubeClient.GetVideoAsync(id);

                return new SuccessTaskResult<YoutubeVideoInfo>(new YoutubeVideoInfo()
                {
                    Title = video.Title,
                    Author = video.Author,
                    Duration = video.Duration
                });
            }
            catch (System.Exception ex)
            {
                return new FailTaskResult<YoutubeVideoInfo>(ex.Message);
            }

        }
    }
}