﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Interface.Dto;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using Leadsoft.Zatara.Services.Interface;
using System.IO;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class MasterDataService : IMasterDataService
    {
        private readonly IDcmtRepository _dcmtRepo;

        public MasterDataService(IDcmtRepository dcmtRepo)
        {
            _dcmtRepo = dcmtRepo;
        }
        public async Task<MasterDataDto> GetMasterDataAsync(GetMasterDataRequest request)
        {
            return new MasterDataDto(){
                ListCompany = await _dcmtRepo.GetAllCompanyDtoAsync(),
                ListBuyer = await _dcmtRepo.GetAllBuyerDtoAsync(),
                ListDepartment = await _dcmtRepo.GetAllDeptDtoAsync(request.CompanyCode),
                ListCategory = await _dcmtRepo.GetAllCateogryDtoAsync(request.CompanyCode, request.DepartmentCode)
            };
        }
    }
}