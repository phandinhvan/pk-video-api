﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class VideoTemplateService : BaseResourceService<VideoTemplate>, IVideoTemplateService
    {
        private readonly IVideoTemplateRepository _repo;
        private readonly IVideoTemplateItemRepository _videoTemplateItemRepo;

        public VideoTemplateService(IVideoTemplateRepository repo,
                                        IVideoTemplateItemRepository videoTemplateItemRepo)
            : base(null, repo)
        {
            _repo = repo;
            _videoTemplateItemRepo = videoTemplateItemRepo;
        }

        protected override async Task<TaskResult<long>> ValidateAsync(VideoTemplate entity)
        {
            // if (string.IsNullOrWhiteSpace(entity.NO))
            //     return await Task.FromResult(new FailTaskResult<long>("Vui lòng chọn Mã số"));

            // if (string.IsNullOrWhiteSpace(entity.NAME))
            //     return await Task.FromResult(new FailTaskResult<long>("Vui lòng chọn Tên"));

            return await Task.FromResult(new SuccessTaskResult<long>());
        }

        public string[] ExtractAllFilePath(string text)
        {
            List<string> lstFilePaths = new List<string>();
            if (string.IsNullOrWhiteSpace(text)) return lstFilePaths.ToArray();

            string[] arrStringValue = text.Split('"');
            if(arrStringValue == null || arrStringValue.Length == 0) return lstFilePaths.ToArray();

            foreach (string stringValue in arrStringValue)
            {
                if (IsFilePath(stringValue) && lstFilePaths.Contains(stringValue) == false)
                    lstFilePaths.Add(stringValue);
            }
            return lstFilePaths.ToArray();
        }

        bool IsFilePath(string text)
        {
            if (text.Contains(@":\") || text.Contains(@"//") || text.Contains(@"\\") || text.Contains(@":/"))
                return true;

            return false;
        }
        string GetStringValueContainIndex(string text, int iIndex)
        {
            string value = string.Empty;
            int iStartIndex = 0;
            int iEndIndex = 0;
            for (int i = 1; i < 1000; i++)
            {
                int preIndex = iIndex - i;
                if(text[preIndex] == '"')//Double quote
                {
                    iStartIndex = preIndex;
                    break;
                }
            }

            for (int i = 1; i < 1000; i++)
            {
                int nextIndex = iIndex + i;
                if (text[nextIndex] == '"')//Double quote
                {
                    iEndIndex = nextIndex;
                    break;
                }
            }

            if (iStartIndex < iEndIndex)
                value = text.Substring(iStartIndex, iEndIndex - iStartIndex + 1);

            return value;
        }

        public string GetMediaType(string filePath)
        {
            string mediaType = string.Empty;
            if (string.IsNullOrWhiteSpace(filePath)) return mediaType;

            filePath = filePath.ToUpper();
            if (filePath.EndsWith("PNG") || filePath.EndsWith("JPG"))
                mediaType = VideoTemplateMediaType.Image.ToString();
            else if (filePath.EndsWith("SVG"))
                mediaType = VideoTemplateMediaType.Svg.ToString();
            else if (filePath.EndsWith("MP3") || filePath.EndsWith("M4A"))
                mediaType = VideoTemplateMediaType.Audio.ToString();
            else if (filePath.EndsWith("MP4") || filePath.EndsWith("AVI"))
                mediaType = VideoTemplateMediaType.Video.ToString();

            return mediaType;
        }

        public string ImageToBase64(Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        public Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }

        public List<VideoTemplateItem> LoadDesignerTemplateFromFile(string templateFilePath)
        {
            var lstItems = new List<VideoTemplateItem>();

            if (File.Exists(templateFilePath) == false) return lstItems;

            string fileContent = File.ReadAllText(templateFilePath);

            string[] arrFilePaths = ExtractAllFilePath(fileContent);
            if (arrFilePaths == null || arrFilePaths.Length == 0) return lstItems;

            int i = 0;
            foreach (string filePath in arrFilePaths)
            {
                VideoTemplateItem itemInfo = CreateVideoTemplateItemsInfo(filePath);
                if (itemInfo == null) continue;

                if (itemInfo.ItemGenerateType == "dynamic")
                    itemInfo.Ord = i++;

                lstItems.Add(itemInfo);
            }
            // txtNumpic.Text = i.ToString();

            return lstItems;
        }
        VideoTemplateItem CreateVideoTemplateItemsInfo(string filePath)
        {
            // if (File.Exists(filePath) == false) return null;
            // byte[] bytes = File.ReadAllBytes(filePath);
            VideoTemplateItem itemInfo = new VideoTemplateItem()
            {
                AbsolutePath = filePath,
                FileName = Path.GetFileName(filePath),
                // FileContent = bytes,
                MediaType = GetMediaType(filePath),
                ItemGenerateType = "dynamic",
            };

            if (filePath.ToUpper().Contains("THUMBNAIL"))
                itemInfo.ItemGenerateType = "static";
            else if (itemInfo.MediaType == VideoTemplateMediaType.Svg.ToString())
                itemInfo.ItemGenerateType = "svg";
            else if (itemInfo.MediaType == VideoTemplateMediaType.Audio.ToString())
                itemInfo.ItemGenerateType = "static";

            return itemInfo;
        }

        public async Task<TaskResult<string>> UploadFileAsync(long typeId, string templateFilePath)
        {
            if(File.Exists(templateFilePath) == false)
                return new FailTaskResult<string>($@"Template file {templateFilePath} not existed");
            
            var lstItems = LoadDesignerTemplateFromFile(templateFilePath);
            
            int iNumDynamicPic = 0;
            foreach (VideoTemplateItem itemInfo in lstItems)
            {
                if (itemInfo.ItemGenerateType == "dynamic")
                    iNumDynamicPic++;
            }

            VideoTemplate templateInfo = new VideoTemplate() {
                VideoTemplateTypeId = typeId,
                DesignerContent = File.ReadAllText(templateFilePath),
                NumberOfImage = iNumDynamicPic
            };

            RestructureTemplate(templateInfo, lstItems);
           
            templateInfo = await _repo.AddAsync(templateInfo);
            foreach (VideoTemplateItem itemInfo in lstItems)
            {
                itemInfo.VideoTemplateId = templateInfo.ID;
                await _videoTemplateItemRepo.AddAsync(itemInfo);
            }

            return new SuccessTaskResult<string>();
        }

        void RestructureTemplate(VideoTemplate templateInfo, List<VideoTemplateItem> arrTemplateItems)
        {
            string designerContent = templateInfo.DesignerContent;

            foreach (VideoTemplateItem itemInfo in arrTemplateItems)
            {
                itemInfo.RelativePath = CreateRelativePath(itemInfo);
                designerContent = designerContent.Replace(itemInfo.AbsolutePath, itemInfo.RelativePath);
            }

            templateInfo.DesignerContent = designerContent;
        }

        string CreateRelativePath(VideoTemplateItem itemInfo)
        {
            if (string.IsNullOrEmpty(itemInfo.AbsolutePath)) return string.Empty;
            string relativePath = string.Empty;

            string fileName = Path.GetFileName(itemInfo.AbsolutePath);
            string fileExtension = System.IO.Path.GetExtension(fileName);
            if(itemInfo.ItemGenerateType == "dynamic")
                relativePath = $"{itemInfo.ItemGenerateType}/{itemInfo.Ord}{fileExtension}";
            else
                relativePath = $"{itemInfo.ItemGenerateType}/{fileName}";
                
            return relativePath;
        }
    }
}