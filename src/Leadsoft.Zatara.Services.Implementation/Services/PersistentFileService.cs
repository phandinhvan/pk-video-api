﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using Serilog;
using System.IO;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class PersistentFileService : IPersistentFileService
    {
        private readonly IConfigService _configService;
        private readonly ILocalFileService _localFileService;
        private readonly IFtpFileService _ftpFileService;

        public PersistentFileService(IConfigService configService, ILocalFileService localFileService, IFtpFileService ftpFileService)
        {
            _configService = configService;
            _localFileService = localFileService;
            _ftpFileService = ftpFileService;
        }

        public void TryDelete(string bucketKey, string relativeFilePath)
        {
            var absoluteFilePath = GetAbsoluteFilePath(bucketKey, relativeFilePath);
            _localFileService.TryDelete(absoluteFilePath);
            _ftpFileService.TryDelete(relativeFilePath);
        }

        public string GetAbsoluteFilePath(string bucketKey, string relativeFilePath)
        {
            var _root_path = _configService.GetObjectStorageRootPath();
            return Path.Combine(_root_path, bucketKey, relativeFilePath);
        }
        public string GetRelativeFilePath(string bucketKey, string relativeFilePath)
        {
            return Path.Combine(bucketKey, relativeFilePath);
        }

        public async Task<Stream> ReadAsync(string bucketKey, string relativeFilePath)
        {
            if (_configService.UsingFtpStorageOnly == false)
            {
                var absoluteFilePath = GetAbsoluteFilePath(bucketKey, relativeFilePath);
                return await _localFileService.ReadAsync(absoluteFilePath);
            }
            else
            {
                var relativePath = GetRelativeFilePath(bucketKey, relativeFilePath);
                return await _ftpFileService.ReadAsync(relativePath);
            }
        }

        public async Task<Stream> ReadAsync(string absoluteFilePath)
        {
            if (File.Exists(absoluteFilePath) == false)
                throw new FileNotFoundException($@"File {absoluteFilePath} not found");

            var memoryStream = new MemoryStream();
            var fs = File.OpenRead(absoluteFilePath);
            fs.Seek(0, SeekOrigin.Begin);

            await fs.CopyToAsync(memoryStream);

            memoryStream.Seek(0, SeekOrigin.Begin);

            return memoryStream;
        }

        public async Task WriteAsync(Stream stream, string bucketKey, string relativeFilePath)
        {
            var absoluteFilePath = GetAbsoluteFilePath(bucketKey, relativeFilePath);
            var relativePath = GetRelativeFilePath(bucketKey, relativeFilePath);

            if (_configService.UsingFtpStorageOnly == false)
            {
                await _localFileService.WriteAsync(stream, absoluteFilePath);
            }else{
                await _ftpFileService.WriteAsync(stream, relativePath);
            }
        }
    }
}