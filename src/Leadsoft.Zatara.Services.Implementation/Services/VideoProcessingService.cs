﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using Serilog;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class VideoProcessingService : IVideoProcessingService
    {
        private readonly IConfigService _configService;
        public VideoProcessingService(IConfigService configService)
        {
            _configService = configService;
        }

        public void ConvertVideo(string inputFilePath, string outputFilePath)
        {
            try
            {
                var ffmpegFilePath = _configService.GetFFMPEGPath();
                Log.Logger.Information($@"Input file: {inputFilePath}");
                Log.Logger.Information($@"Output file: {outputFilePath}");
                string strCmdText;
                strCmdText = $@"-i {inputFilePath} {outputFilePath}";
                var process = System.Diagnostics.Process.Start(ffmpegFilePath, strCmdText);
                process.WaitForExit();
            }
            catch (Exception ex)
            {
                var mes = ex.Message;
            }

        }
    }
}