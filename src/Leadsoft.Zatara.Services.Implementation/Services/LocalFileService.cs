﻿using Base.Interface.Dto;
using Base.Services.Implementation;
using Base.Services.Interface;
using FluentFTP;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using Serilog;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Implementation.Services
{
    public class LocalFileService : ILocalFileService
    {
        private readonly IConfigService _configService;
        public LocalFileService(IConfigService configService)
        {
            _configService = configService;
        }

        public void TryDelete(string absoluteFilePath)
        {
            try
            {
                File.Delete(absoluteFilePath);
            }
            catch (System.Exception ex)
            {
            }

        }

        public async Task<Stream> ReadAsync(string absoluteFilePath)
        {
            if (File.Exists(absoluteFilePath) == false)
                throw new FileNotFoundException($@"File {absoluteFilePath} not found");

            var memoryStream = new MemoryStream();
            var fs = File.OpenRead(absoluteFilePath);
            fs.Seek(0, SeekOrigin.Begin);

            await fs.CopyToAsync(memoryStream);

            memoryStream.Seek(0, SeekOrigin.Begin);

            return memoryStream;
        }

        public async Task WriteAsync(Stream stream, string absoluteFilePath)
        {
            FileInfo fileInfo = new FileInfo(absoluteFilePath);
            if(fileInfo.Directory.Exists == false)
                fileInfo.Directory.Create();

            using (var fileStream = new FileStream(absoluteFilePath, FileMode.Create, FileAccess.Write))
            {
                stream.Seek(0, SeekOrigin.Begin);
                await stream.CopyToAsync(fileStream);
            }
        }
    }
}