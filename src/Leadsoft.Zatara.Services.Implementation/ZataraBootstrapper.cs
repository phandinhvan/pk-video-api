﻿using Autofac;
using Base.Infrastructure.Data.Dapper;
using Base.Services.Implementation;
using Leadsoft.Common.Interceptors;
using Leadsoft.Zatara.Infrastructure.Data.Dapper;
using Leadsoft.Zatara.Infrastructure.Data.Dapper.Repositories;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Implementation.Services;
using MassTransit;
using Serilog;
using System;
using System.Linq;
using System.Reflection;

namespace Leadsoft.Zatara.Services.Implementation
{
    public class ZataraBootstrapper
    {
        private ContainerBuilder builder = new ContainerBuilder();

        public ContainerBuilder Builder => builder;
        private IContainer _container = null;

        public IContainer Container
        {
            get
            {
                if (_container == null)
                    _container = builder.Build();

                return _container;
            }
        }

        private bool _isDistributeService = false;

        public ZataraBootstrapper(bool isDistributeService = false)
        {
            _isDistributeService = isDistributeService;
            RegisterServices(builder);
        }

        public ZataraBootstrapper(ContainerBuilder _builder, bool isDistributeService = false)
        {
            builder = _builder;
            _isDistributeService = isDistributeService;
            RegisterServices(builder);
        }

        private void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterInstance<Serilog.ILogger>(Log.Logger).SingleInstance();
            builder.RegisterType<MethodCallInterceptor>();

            string connectionString = ZataraApplicationSettings.DB_CONNECTIONSTRING;
            //var connectionFactory = new GandalfMsSqlConnectionFactory(connectionString);
            //var connectionFactory = new ZataraMySqlConnectionFactory(connectionString);
            var connectionFactory = new ZataraOracleConnectionFactory(connectionString);

            builder.RegisterInstance<IConnectionFactory>(connectionFactory).SingleInstance();
            builder.RegisterInstance(new ZataraDbContext()).SingleInstance();

            //Repositories
            var repositoryAssembly = typeof(DealRepository).GetTypeInfo().Assembly;
            builder.RegisterAssemblyTypes(repositoryAssembly).As(t =>
            {
                var arrInterface = t.GetInterfaces().Where(i => i.Name.Contains("IGenericRepository") == false);
                Type interfaceType = arrInterface.FirstOrDefault();
                if (interfaceType == null)
                    interfaceType = t;

                return interfaceType;
            });
            //.EnableInterfaceInterceptors().InterceptedBy(typeof(MethodCallInterceptor));

            //Base Services
            var baseServiceAssembly = typeof(TelegramService).GetTypeInfo().Assembly;
            builder.RegisterAssemblyTypes(baseServiceAssembly)
                .Where(t => t.Name.EndsWith("Service") && (_isDistributeService == false || IsBaseClass(t) == false))
                .As(t =>
                {
                    var test = t.GetInterfaces().Where(i => i.Name.EndsWith("Service")).FirstOrDefault();
                    return test;
                });

            // //Mappers
            // var mapperAssembly = typeof(EmployeeMapper).GetTypeInfo().Assembly;
            // builder.RegisterAssemblyTypes(mapperAssembly)
            //     .Where(t => t.Name.EndsWith("Mapper") && IsBaseClass(t) == false);

            //Services
            var serviceAssembly = typeof(DealService).GetTypeInfo().Assembly;
            builder.RegisterAssemblyTypes(serviceAssembly)
                .Where(t => t.Name.EndsWith("Service") && (_isDistributeService == false || IsBaseClass(t) == false))
                .As(t =>
                {
                    var test = t.GetInterfaces().Where(i => i.Name.EndsWith("Service")).FirstOrDefault();
                    return test;
                });
            //.EnableInterfaceInterceptors().InterceptedBy(typeof(MethodCallInterceptor));

            ////mapper
            //var mapperAssembly = typeof(ClosedOrderMapper).GetTypeInfo().Assembly;
            //builder.RegisterAssemblyTypes(mapperAssembly)
            //.Where(t => t.Name.EndsWith("Mapper"))
            //.AsImplementedInterfaces()
            //.SingleInstance();
        }

        private bool IsBaseClass(Type type)
        {
            var subclassTypes = Assembly.GetAssembly(type)
                           .GetTypes()
                           .Where(t => t.IsSubclassOf(type));

            if (subclassTypes != null && subclassTypes.Count() > 0)
                return true;

            return false;
        }
    }
}