﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Dommel.SqlBuilder
{
    /// <summary>
    ///
    /// </summary>
    public class OracleSqlBuilder:BaseSqlBuilder, ISqlBuilder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnNames"></param>
        /// <param name="paramNames"></param>
        /// <param name="keyName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public string BuildInsertOld(string tableName, string[] columnNames, string[] paramNames, string keyName, string keyParamName)
        {
            return $"INSERT INTO {tableName} ({string.Join(", ", columnNames.Select(columnName => $@"""{columnName}"""))}) values ({string.Join(", ", paramNames.Select(p => p.Replace("@", ":")))}) returning Id into :Id";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnNames"></param>
        /// <param name="paramNames"></param>
        /// <param name="keyName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public string BuildInsert(string tableName, string[] columnNames, string[] paramNames, string keyName, string keyParamName)
        {
            return $"INSERT INTO {tableName} ({string.Join(", ", columnNames.Select(columnName => $@"""{columnName}"""))}) values ({string.Join(", ", paramNames.Select(p => p.Replace("@", ":")))})";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="keyColumnName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public override string BuildGetByIdSql(string tableName, string keyColumnName, string keyParamName)
        {
            return $"select * from {tableName} where {keyColumnName} = :{keyParamName}";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="keyColumnName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public override string BuildDeleteByISql(string tableName, string keyColumnName, string keyParamName)
        {
            return $"DELETE FROM {tableName} WHERE {keyColumnName} = :{keyParamName}";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dicColumnAndParamNames"></param>
        /// <param name="keyColumnName"></param>
        /// <returns></returns>
        public override string BuildUpdateQuerySql(string tableName, Dictionary<string, string> dicColumnAndParamNames, string keyColumnName)
        {
            var columnOperations = dicColumnAndParamNames.Select(p => $"{p.Key} = :{p.Value}").ToArray();
            return $"update {tableName} set {string.Join(", ", columnOperations)} where {keyColumnName} = :{keyColumnName}";
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="tableName"></param>
       /// <param name="columnNames"></param>
       /// <param name="paramNames"></param>
       /// <param name="keyName"></param>
       /// <param name="keyParamName"></param>
       /// <returns></returns>
        public string BuildInsertWithKey(string tableName, string[] columnNames, string[] paramNames, string keyName, string keyParamName)
        {
            var lstColumnName = columnNames.ToList();
            var lstParamNames = paramNames.ToList();

            lstColumnName.Insert(0, keyName);
            lstParamNames.Insert(0, keyParamName);

            return $"INSERT INTO {tableName} ({string.Join(", ", lstColumnName.Select(columnName => $@"""{columnName}"""))}) values ({string.Join(", ", lstParamNames.Select(p => p.Replace("@", ":")))})";
        }
    }
}