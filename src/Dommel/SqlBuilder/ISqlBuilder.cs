﻿using System.Collections.Generic;
using System.Reflection;

namespace Dommel.SqlBuilder
{
    /// <summary>
    /// Defines methods for building specialized SQL queries.
    /// </summary>
    public interface ISqlBuilder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnNames"></param>
        /// <param name="paramNames"></param>
        /// <param name="keyName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        string BuildInsert(string tableName, string[] columnNames, string[] paramNames, string keyName, string keyParamName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnNames"></param>
        /// <param name="paramNames"></param>
        /// <param name="keyName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        string BuildInsertWithKey(string tableName, string[] columnNames, string[] paramNames, string keyName, string keyParamName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="keyColumnName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        string BuildGetByIdSql(string tableName, string keyColumnName, string keyParamName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="keyColumnName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        string BuildDeleteByISql(string tableName, string keyColumnName, string keyParamName);

        /// <summary>
        ///
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="dicColumnAndParamNames"></param>
        /// <param name="keyColumnName"></param>
        /// <returns></returns>
        string BuildUpdateQuerySql(string tableName, Dictionary<string, string> dicColumnAndParamNames, string keyColumnName);
    }
}