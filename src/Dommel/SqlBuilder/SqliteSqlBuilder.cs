﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dommel.SqlBuilder
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class SqliteSqlBuilder : BaseSqlBuilder, ISqlBuilder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnNames"></param>
        /// <param name="paramNames"></param>
        /// <param name="keyName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public string BuildInsert(string tableName, string[] columnNames, string[] paramNames, string keyName, string keyParamName)
        {
            return $"insert into {tableName} ({string.Join(", ", columnNames)}) values ({string.Join(", ", paramNames)}); select last_insert_rowid() id";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnNames"></param>
        /// <param name="paramNames"></param>
        /// <param name="keyName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public string BuildInsertWithKey(string tableName, string[] columnNames, string[] paramNames, string keyName, string keyParamName)
        {
            return BuildInsert(tableName, columnNames, paramNames, keyName, keyParamName);
        }
    }
}
