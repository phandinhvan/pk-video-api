﻿using System.Collections.Generic;
using System.Linq;

namespace Dommel.SqlBuilder
{
    /// <summary>
    ///
    /// </summary>
    public abstract class BaseSqlBuilder
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="keyColumnName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public virtual string BuildGetByIdSql(string tableName, string keyColumnName, string keyParamName)
        {
            return $"select * from {tableName} where {keyColumnName} = @{keyParamName}";
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="keyColumnName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public virtual string BuildDeleteByISql(string tableName, string keyColumnName, string keyParamName)
        {
            return $"DELETE FROM {tableName} WHERE {keyColumnName} = @{keyParamName}";
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="tableName"></param>
       /// <param name="dicColumnAndParamNames"></param>
       /// <param name="keyColumnName"></param>
       /// <returns></returns>
        public virtual string BuildUpdateQuerySql(string tableName, Dictionary<string, string> dicColumnAndParamNames, string keyColumnName)
        {
            var columnOperations = dicColumnAndParamNames.Select(p => $"{p.Key} = @{p.Value}").ToArray();
            return $"update {tableName} set {string.Join(", ", columnOperations)} where {keyColumnName} = @{keyColumnName}";
        }
    }
}