﻿using System.Reflection;

namespace Dommel.SqlBuilder
{
    /// <summary>
    ///
    /// </summary>
    public sealed class PostgresSqlBuilder : BaseSqlBuilder, ISqlBuilder
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnNames"></param>
        /// <param name="paramNames"></param>
        /// <param name="keyName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public string BuildInsert(string tableName, string[] columnNames, string[] paramNames, string keyName, string keyParamName)
        {
            var sql = $"insert into {tableName} ({string.Join(", ", columnNames)}) values ({string.Join(", ", paramNames)})";

            var keyColumnName = keyName;

            sql += " RETURNING " + keyColumnName;

            return sql;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnNames"></param>
        /// <param name="paramNames"></param>
        /// <param name="keyName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public string BuildInsertWithKey(string tableName, string[] columnNames, string[] paramNames, string keyName, string keyParamName)
        {
            return BuildInsert(tableName, columnNames, paramNames, keyName, keyParamName);
        }
    }
}