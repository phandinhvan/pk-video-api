﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dommel.SqlBuilder
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class MySqlSqlBuilder : BaseSqlBuilder, ISqlBuilder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnNames"></param>
        /// <param name="paramNames"></param>
        /// <param name="keyName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public string BuildInsert(string tableName, string[] columnNames, string[] paramNames, string keyName, string keyParamName)
        {
            if (DommelMapper.EscapeCharacterStart == char.MinValue && DommelMapper.EscapeCharacterEnd == char.MinValue)
            {
                // Fall back to the default behavior.
                return $"insert into `{tableName}` (`{string.Join("`, `", columnNames)}`) values ({string.Join(", ", paramNames)}); select LAST_INSERT_ID() id";
            }

            // Table and column names are already escaped.
            return $"insert into {tableName} ({string.Join(", ", columnNames)}) values ({string.Join(", ", paramNames)}); select LAST_INSERT_ID() id";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnNames"></param>
        /// <param name="paramNames"></param>
        /// <param name="keyName"></param>
        /// <param name="keyParamName"></param>
        /// <returns></returns>
        public string BuildInsertWithKey(string tableName, string[] columnNames, string[] paramNames, string keyName, string keyParamName)
        {
            return BuildInsert(tableName, columnNames, paramNames, keyName, keyParamName);
        }
    }
}
