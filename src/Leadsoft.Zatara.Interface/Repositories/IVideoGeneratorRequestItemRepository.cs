﻿using Base.Interface;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Interface
{
    public interface IVideoGeneratorRequestItemRepository : IGenericRepository<VideoGeneratorRequestItem>
    {
    }
}