﻿using Base.Interface;
using Base.Interface.Dto;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Interface
{
    public interface IPKFileRepository : IGenericRepository<PKFile>
    {
        Task<PagingResult<PKFileDto>> GetListPagingResultAsync(GetVideoPagingRequest request);
        Task<PKFileDto> GetDtoByIdAsync(string fileId);
        Task<PagingResult<ManageStyleDto>> GetListManageStylePagingResultAsync(GetVideoManageStylePagingRequest request);
        Task<PagingResult<ManageStyleVideoDto>> GetListVideoManageStylePagingResultAsync(GetListVideoManageStylePagingRequest request);
        Task<PagingResult<SetupDto>> GetListProjectNamePagingResultAsync(GetListProjectSetupPagingRequest request);
        Task<PagingResult<SetupDto>> GetListItemProjectNamePagingResultAsync(GetListItemProjectNamePagingResult request);
        Task<PagingResult<PKFileDto>> GetNewVideoPagingResultAsync();
        
    }
}