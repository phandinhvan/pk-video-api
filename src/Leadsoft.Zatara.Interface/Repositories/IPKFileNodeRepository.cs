﻿using Base.Interface;
using Base.Interface.Dto;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Interface
{
    public interface IPKFileNodeRepository : IGenericRepository<PKFileNode>
    {

    }
}