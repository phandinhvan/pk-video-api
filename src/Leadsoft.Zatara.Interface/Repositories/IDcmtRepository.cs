﻿using Base.Interface;
using Base.Interface.Dto;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Interface
{
    public interface IDcmtRepository : IGenericRepository<Dcmt>
    {
        Task<IEnumerable<CompanyDto>> GetAllCompanyDtoAsync();
        Task<IEnumerable<BuyerDto>> GetAllBuyerDtoAsync();
        Task<IEnumerable<DepartmentDto>> GetAllDeptDtoAsync(string labcode);
        Task<IEnumerable<CategoryDto>> GetAllCateogryDtoAsync(string labcode, string department);
        
    }
}