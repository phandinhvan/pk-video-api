﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Base.Interface;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Interface
{
    public interface IObjectStorageRepository : IGenericRepository<ObjectStorage>
    {
        Task<ObjectStorage> GetObjectByKeyAsync(long bucketId, string key);
        Task<int> DeleteObjectByKeyAsync(string key);
        Task<IEnumerable<ObjectStorage>> SearchAsync(long bucketId, string keyword);
        Task<ObjectStorage> GetObjectByIdAsync(long bucketId, long id);
    }
}