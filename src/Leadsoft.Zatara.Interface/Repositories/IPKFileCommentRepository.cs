﻿using Base.Interface;
using Base.Interface.Dto;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface.Dto.Requests;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Interface
{
    public interface IPKFileCommentRepository : IGenericRepository<PKFileComment>
    {
        Task<PagingResult<PKFileComment>> GetListPagingAsync(GetCommentPagingRequest request);
    }
}