﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Base.Interface;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Interface
{
    public interface IObjectBucketRepository : IGenericRepository<ObjectBucket>
    {
        Task<ObjectBucket> GetObjectByKeyAsync(string key);
        Task<int> DeleteObjectByKeyAsync(string key);
    }
}