﻿using System.Threading.Tasks;
using Base.Interface;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Interface
{
    public interface IPKUserRepository : IGenericRepository<PKUser>
    {
        Task<PKUser> GetUserAsync(string userid, string password);
        Task<PKUser> GetUserAsync(string userId);
    }
}