﻿namespace Leadsoft.Zatara.Interface.Dto
{
    public class VideoServiceSetting
    {
        public bool UsingFtpStorageOnly { get; set; }
    }
}