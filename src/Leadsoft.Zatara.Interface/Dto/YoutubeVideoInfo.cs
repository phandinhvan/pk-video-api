﻿using System;

namespace Leadsoft.Zatara.Interface.Dto
{
    public class YoutubeVideoInfo
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public TimeSpan Duration { get; set; }
    }
}