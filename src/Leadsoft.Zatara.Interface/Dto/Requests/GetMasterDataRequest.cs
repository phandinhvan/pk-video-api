﻿using System.IO;

namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class GetMasterDataRequest
    {
        public string CompanyCode { get; set; }
        public string DepartmentCode { get; set; }
    }
}