﻿using Base.Interface.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class GetListItemProjectNamePagingResult : PagingRequest
    {
        public string LabCode { get; set; }
    }
}
