﻿namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class UpdateFileRequest
    {
        public string fileid { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string companycode { get; set; }
        public string departmentcode { get; set; }
        public string privacy { get; set; }

    }
}