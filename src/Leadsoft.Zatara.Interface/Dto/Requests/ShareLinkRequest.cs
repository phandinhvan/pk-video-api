﻿using System.IO;

namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class ShareLinkRequest
    {
        public string Link { get; set; }
        public string Title { get; set; }
        public string CompanyCode { get; set; }
        public string DepartmentCode { get; set; }
        public string Privacy { get; set; }
        public string Catalogue { get; set; }
    }
}