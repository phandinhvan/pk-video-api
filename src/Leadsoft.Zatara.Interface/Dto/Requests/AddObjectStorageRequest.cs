﻿using System.IO;

namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class AddObjectStorageRequest
    {
        public string Key { get; set; }
        public string BucketKey { get; set; }
        public bool IsPublic { get; set; }
        public Stream DataStream { get; set; }
    }
}