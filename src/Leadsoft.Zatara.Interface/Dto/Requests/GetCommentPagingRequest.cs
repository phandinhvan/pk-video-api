﻿using Base.Interface.Dto;
using System.IO;

namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class GetCommentPagingRequest : PagingRequest
    {
        public string FileId { get; set; }
    }
}