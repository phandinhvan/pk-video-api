﻿using System.IO;

namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class AddChunkObjectStorageRequest
    {
        //abc.mp4.part1-3
        //abc.mp4.part2-3
        //abc.mp4.part3-3
        public string Key { get; set; }
        public string FileName { get; set; }
        public string Title { get; set; }
        public int PartIndex { get; set; }
        public int TotalPart { get; set; }

        public string CompanyCode { get; set; }
        public string TeamCode { get; set; }
        public string Description { get; set; }
        public string Catalogue { get; set; }
        public string Category { get; set; }

        public bool IsPublic { get; set; }
        public bool IsOnlyForDepartment { get; set; }
        public bool IsPrivated { get; set; }

        public Stream DataStream { get; set; }
        public string TimeStart { get; set; }
        public string TimeEnd { get; set; }
    }
}