﻿using Base.Interface.Dto;

namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class GetVideoManageStylePagingRequest : PagingRequest
    {
        public string Buyer { get; set; }
        public string UserId { get; set; }
        public int AfterDay { get; set; }
        public string FileName { get; set; }
    }
}
