﻿using Base.Interface.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class GetListVideoManageStylePagingRequest : PagingRequest
    {
        public string CompanyCode { get; set; }
        public string DepartmentCode { get; set; }
        public int AfterDay { get; set; }
        public string StyleCode { get; set; }
        public string StyleSize { get; set; }
        public string StyleColorSerial { get; set; }

    }

}
