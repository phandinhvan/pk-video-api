﻿using Base.Interface.Dto;

namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class GetVideoPagingRequest: PagingRequest
    {
        public string FileName { get; set; }
        public string CompanyCode { get; set; }
        public string DepartmentCode { get; set; }
        public string UserId { get; set; }
        //public int AfterDay { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Catalogue { get; set; }
        public string Category { get; set; }
    }
}