﻿using Base.Interface.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class GetListProjectSetupPagingRequest : PagingRequest
    {
        public string LabCode { get; set; }
        public string LabRoomName { get; set; }
        public string UserId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string UseStatus { get; set; }
    }
}
