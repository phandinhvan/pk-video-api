﻿using System.IO;

namespace Leadsoft.Zatara.Interface.Dto.Requests
{
    public class GetYoutubeInfoRequest
    {
        public string Link { get; set; }
    }
}