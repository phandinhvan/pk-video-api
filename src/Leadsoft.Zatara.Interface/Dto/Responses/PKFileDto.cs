﻿using System;
using System.Collections.Generic;
using System.Text;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Interface.Dto.Responses
{
    public class PKFileDto: PKFile
    {
        public string COMPANY_NAME { get; set; }
        public string DEPARTMENT_NAME { get; set; }
        public string UPLOADDATE_TEXT { get; set; }
        public string UPLOADER_NAME { get; set; }
        public string URL { get; set; }
        public string PRIVACY_CODE { get; set; }
        public string PRIVACY_NAME { get; set; }
        public string URLImage { get; set; }
        public string TIME_UPLOADDATE { get; set; }
    }
}
