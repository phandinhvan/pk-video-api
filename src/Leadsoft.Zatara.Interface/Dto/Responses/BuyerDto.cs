﻿using System;
using System.Collections.Generic;
using System.Text;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Interface.Dto.Responses
{
    public class BuyerDto
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string DSC { get; set; }
    }
}
