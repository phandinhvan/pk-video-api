﻿using System;
using System.Collections.Generic;
using System.Text;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Interface.Dto.Responses
{
    public class MasterDataDto
    {
        public IEnumerable<CompanyDto> ListCompany { get; set; }
        public IEnumerable<BuyerDto> ListBuyer { get; set; }
        public IEnumerable<DepartmentDto> ListDepartment { get; set; }
        public IEnumerable<CategoryDto> ListCategory { get; set; }
    }
}
