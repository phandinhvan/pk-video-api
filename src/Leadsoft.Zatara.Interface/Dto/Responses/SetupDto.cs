﻿using Leadsoft.Zatara.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Leadsoft.Zatara.Interface.Dto.Responses
{
    public class SetupDto : PKSetup
    {
        public string UPLOADER_NAME { get; set; }
        public string UPLOADDATE_TEXT { get; set; }
        public string PROCODE { get; set; }
        public string PRONAME { get; set; }
        public string PRODESCRIPTION { get; set; }
    }
}
