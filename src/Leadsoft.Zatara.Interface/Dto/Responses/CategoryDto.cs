﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leadsoft.Zatara.Interface.Dto.Responses
{
    public class CategoryDto
    {
        public string NAME { get; set; }
        public string STATUS { get; set; }
    }
}
