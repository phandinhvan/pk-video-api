﻿using Leadsoft.Zatara.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Leadsoft.Zatara.Interface.Dto.Responses
{
    public class ManageStyleVideoDto : PKManageStyleVideo
    {
        public string UPLOADER_NAME { get; set; }
        public string UPLOADDATE_TEXT { get; set; }
        public string URL { get; set; }
    }
}
