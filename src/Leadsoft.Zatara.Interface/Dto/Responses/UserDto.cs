﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leadsoft.Zatara.Interface.Dto.Responses
{
    public class UserDto
    {
        public string  UserId { get; set; }
        public string  Name { get; set; }
        public string RoleId { get; set; }
    }
}
