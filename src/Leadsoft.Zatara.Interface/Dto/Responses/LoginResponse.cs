﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leadsoft.Zatara.Interface.Dto.Responses
{
    public class LoginResponse
    {
        public string[] roles { get; set; }
        public string token { get; set; }
        public string introduction { get; set; }
        public string avatar { get; set; }
        public string name { get; set; }
    }
}
