﻿using Leadsoft.Zatara.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Leadsoft.Zatara.Interface.Dto.Responses
{
    public class ManageStyleDto : PKManageStyle
    {
        public string UESERNAME { get; set; }
        public string FINAL_CONFIRM { get; set; }
    }
}
