﻿namespace Leadsoft.Zatara.Interface.Dto
{
    public class FtpSetting
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string WorkingDir { get; set; }
    }
}