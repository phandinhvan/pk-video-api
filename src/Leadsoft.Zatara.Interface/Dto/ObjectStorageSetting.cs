﻿namespace Leadsoft.Zatara.Interface.Dto
{
    public class LocalStorageSetting
    {
        public string RootPath { get; set; }
        public string TempUploadFolder { get; set; }
        public string FFMPEGPath { get; set; }
    }
}