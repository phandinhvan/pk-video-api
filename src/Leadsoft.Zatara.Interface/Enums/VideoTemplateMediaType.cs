﻿using Base.Interface;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Interface
{
    public enum VideoTemplateMediaType
    {
        Image = 0,
        Video = 1,
        Svg = 2,
        Audio = 3
    }
}