﻿using System.Collections.Generic;

namespace Gandalf.Interface.Dto.QueryParameter
{
    public class FindByIdParameter : BaseParameter
    {
        public object Id { get; set; }
        public IEnumerable<ColumnMap> Fields { get; set; }
        public IEnumerable<ConditionParameter> Conditions { set; get; }
    }
}
