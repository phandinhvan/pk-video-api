﻿namespace Gandalf.Interface.Dto.QueryParameter
{
    public class PagingParameter
    {
        public int PageIndex { set; get; }
        public int PageSize { set; get; }
    }
}
