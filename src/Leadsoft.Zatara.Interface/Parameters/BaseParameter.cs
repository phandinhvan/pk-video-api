﻿namespace Gandalf.Interface.Dto.QueryParameter
{
    public class BaseParameter
    {
        public Omnicasa.Common.Enums.Language Language { get; set; }
    }
}
