﻿using System;

namespace Base.Entities
{
    public abstract class BaseMasterEntity: BaseEntity
    {
        public DateTime CREATED_DATE { get; set; }
        public string CREATED_USER { get; set; }
    }
}
