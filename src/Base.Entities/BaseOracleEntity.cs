﻿using System;

namespace Base.Entities
{
    public abstract class BaseEntity
    {
        public long ID { get; set; }
    }
}
