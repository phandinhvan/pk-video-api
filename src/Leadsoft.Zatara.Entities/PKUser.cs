﻿using System;
using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class PKUser
    {
        public string USERID { get; set; }
        public string NAME { get; set; }
        public string PASSWD { get; set; }
        public string TEL { get; set; }
        public string EMAIL { get; set; }
        public string COMPCODE { get; set; }
        public string DEPTCODE { get; set; }
        public string SEX { get; set; }
        public string CONFIRM { get; set; }
        public string STATUS { get; set; }
        public string SOS { get; set; }
        public string KEYID { get; set; }
        public string TYPE { get; set; }
        public string REGISTRYDATE { get; set; }
        public string LASTMODIFYDATE { get; set; }
        public string EXPIRYDATE { get; set; }
        public string ROLEID { get; set; }
    }
}   