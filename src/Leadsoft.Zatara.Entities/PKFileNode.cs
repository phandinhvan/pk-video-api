﻿using System;
using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class PKFileNode
    {
        public string OWNERID { get; set; }
        public string NODEDID { get; set; }
        public string PARENTID { get; set; }
        public string FILEID { get; set; }
        public string CORPORATION { get; set; }
        public string DEPARTMENT { get; set; }
        public string SECRETCHECK { get; set; }
        public string PRIVATECHECK { get; set; }
        public string DEPARTCHECK { get; set; }
        public string PUBLICHCHECK { get; set; }
        public string FILENAMESYS { get; set; }
        public string FILENAME { get; set; }
        public string CONTENTTYPE { get; set; }
        public string ISFOLDER { get; set; }
        public string FILEDATA { get; set; }
        public string LASTWRITETIME { get; set; }
        public string FILETYPE { get; set; }

        public decimal FILESIZE { get; set; }
        public string FILEPATH { get; set; }
        public DateTime? UPLOADDATE { get; set; }
        public string UPLOADID { get; set; }
    }
}   