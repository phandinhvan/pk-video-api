﻿using System;
using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class ObjectStorage: BaseEntity
    {
        public string OBJECTKEY { get; set; }
        public decimal SIZE { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public int ISDELETED { get; set; }
        public long BUCKETID { get; set; }
        public string MIME_TYPE { get; set; }
        public int ISPUBLIC { get; set; }
    }
}