﻿using System;
using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class PKFileComment
    {
        public string FILEID { get; set; }
        public string COMMENTID { get; set; }
        public string COMMENTNOTE { get; set; }
        public DateTime COMMENTDATE { get; set; }
        public string REGISTERID { get; set; }
    }
}   