﻿using System;
using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class VideoGeneratorRequest: BaseEntity
    {
        public long VideoTemplateId { get; set; }
        public string ItemGenerateType { get; set; }
        public int NumberOfPictureUpload { get; set; }
        public bool IsUploadFileCompleted { get; set; }
        public bool IsGeneratedVideoCompleted { get; set; }
        public string LocalResultUrl { get; set; }
        public string PublicResultUrl { get; set; }
        public string RequestStatus { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public string ProcessLog { get; set; }
    }
}   