﻿using System;
using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class ObjectBucket: BaseEntity
    {
        public string BUCKETKEY { get; set; }
        public string NAME { get; set; }
        public string DSC { get; set; }
        public int ISPUBLIC { get; set; }
    }
}