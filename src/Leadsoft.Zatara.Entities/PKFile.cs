﻿using System;
using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class PKFile
    {
        public string FILEID { get; set; }
        public string CORPORATION { get; set; }
        public string DEPARTMENT { get; set; }
        public string PRIVATECHECK { get; set; }
        public string DEPARTCHECK { get; set; }
        public string PUBLICHCHECK { get; set; }
        public string FILENAMESYS { get; set; }
        public string FILENAME { get; set; }
        public string CONTENTTYPE { get; set; }
        public string FILETYPE { get; set; }
        public string FILEPATH { get; set; }
        public DateTime UPLOADDATE { get; set; }
        public string UPLOADID { get; set; }
        public string PRODUCT { get; set; }
        public string FILEKIND { get; set; }
        public string CATALOGUE { get; set; }
        public string CATEGORY { get; set; }
    }
}   