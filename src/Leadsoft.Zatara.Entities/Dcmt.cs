﻿using System;
using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class Dcmt
    {
        public string DEPTCODE { get; set; }
        public string DEPTLEVEL { get; set; }
        public string SHORTNAME { get; set; }
        public string FULLNAME { get; set; }
        public string UPDATEBY { get; set; }
        public string UPDATEDATE { get; set; }
        public string USESTATUS { get; set; }
    }
}   