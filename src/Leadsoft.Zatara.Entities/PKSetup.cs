﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leadsoft.Zatara.Entities
{
    public class PKSetup
    {
        public string LABCODE { get; set; }
        public string LABROOMNAME { get; set; }
        public string REGISTERID { get; set; }
        public DateTime REGISTRYDATE { get; set; }
        public string USESTATUS { get; set; }
    }
}
