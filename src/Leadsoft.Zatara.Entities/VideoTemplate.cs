﻿using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class VideoTemplate: BaseEntity
    {
        public long VideoTemplateTypeId { get; set; }
        public string DesignerContent { get; set; }
        public int NumberOfImage { get; set; }
        public bool IsDeleted { get; set; }
    }
}   