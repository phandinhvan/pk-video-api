﻿using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class VideoTemplateType: BaseEntity
    {
        public string Name { get; set; }
        public string Dsc { get; set; }
        public string PreviewUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public bool IsDeleted { get; set; }
    }
}   