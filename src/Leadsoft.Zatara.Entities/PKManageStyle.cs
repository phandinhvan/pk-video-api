﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leadsoft.Zatara.Entities
{
    public class PKManageStyle
    {
        public string STYLECODE { get; set; }
        public string STYLESIZE { get; set; }
        public string STYLECOLORSERIAL { get; set; }
        public string REVNO { get; set; }
        public string REGISTER { get; set; }
        public string REGISTRYDATE { get; set; }
        public string BUYER { get; set; }
        public string REMARKS { get; set; }
        public string FACTORY { get; set; }
        public DateTime AD_CONFIRM { get; set; }
        public string STYLENAME { get; set; }
        public string BUYERSTYLECODE { get; set; }
        public string BUYERSTYLENAME { get; set; }
        public string STATUS { get; set; }
        public string CURRCODE { get; set; }
        public decimal UNITPRICE { get; set; }
        public string STYLECOLORWAYS { get; set; }
    }
}
