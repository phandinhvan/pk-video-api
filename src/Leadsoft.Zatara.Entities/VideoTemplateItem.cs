﻿using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class VideoTemplateItem: BaseEntity
    {
        public long VideoTemplateId { get; set; }
        public string ItemGenerateType { get; set; }
        public string MediaType { get; set; }
        public string AbsolutePath { get; set; }
        public string RelativePath { get; set; }
        public string FileName { get; set; }
        public string FileContentUrl { get; set; }
        public byte[] FileContent { get; set; }//temp
        public int Ord { get; set; }
    }
}   