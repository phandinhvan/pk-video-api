﻿using Base.Entities;

namespace Leadsoft.Zatara.Entities
{
    public class VideoGeneratorRequestItem: BaseEntity
    {
        public long VideoGeneratorRequestId { get; set; }
        public string ItemType { get; set; }
        public string FileName { get; set; }
        public string FileUrl { get; set; }
        public int Ord { get; set; }
    }
}   