﻿using Autofac;
using Base.Infrastructure.Data.Dapper;
using Dapper;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Implementation;
using Leadsoft.Zatara.Services.Interface;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.MigrationService
{
    internal class Program
    {
        private static ZataraBootstrapper bootstrapper = null;

        private static void Main(string[] args)
        {
            bootstrapper = new ZataraBootstrapper();

            // var gdvSalaryService = bootstrapper.Container.Resolve<IGdvSalaryService>();

            // gdvSalaryService.CalcGdvSalaryAsync(1557,2019, 4).Wait();
            // gdvSalaryService.CalcGdvShopSalaryAsync(2, 2019, 2).Wait();
            // gdvSalaryService.CalcGdvSalaryAsync(1560, 2019, 4);
            //MigrateLocationAsync().Wait();
            // MigrateGdvAsync().Wait();
            //GenerateDefaultWorkingDayAsync(0).Wait();
            //GenerateDefaultWorkingDayAsync(1).Wait();
        }
    }
}