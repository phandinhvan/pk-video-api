﻿using Base.Services.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace Base.Services.Implementation
{
    public class TelegramService : ITelegramService
    {
        public async Task SendChatTypingAsync(string botToken, long chatId)
        {
            var client = CreateBotClient(botToken);
            await client.SendChatActionAsync(chatId, ChatAction.Typing);
        }
        public async Task<Message> SendTextMessageAsync(string botToken, long chatId, string text, ParseMode parseMode = ParseMode.Default, bool disableWebPagePreview = false, bool disableNotification = false, int replyToMessageId = 0, IReplyMarkup replyMarkup = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            var client = CreateBotClient(botToken);
            return await client.SendTextMessageAsync(chatId, text, parseMode, disableWebPagePreview, disableNotification, replyToMessageId, replyMarkup, cancellationToken);
        }

        public async Task<bool> IsChatFromAdminOfGroupAsync(string botToken, Telegram.Bot.Types.Message message)
        {
            if (message.From == null) return false;
            int fromUserId = message.From.Id;

            var botClient = CreateBotClient(botToken);

            var lstAdmins = await botClient.GetChatAdministratorsAsync(message.Chat.Id);

            if (lstAdmins != null)
            {
                foreach (var adminMember in lstAdmins)
                {
                    if (adminMember.User.Id == fromUserId)
                        return true;
                }
            }

            return false;
        }

        private TelegramBotClient CreateBotClient(string botToken)
        {
            return new TelegramBotClient(botToken);
        }

        public async Task LogTelegramAsync(string text)
        {
            try
            {
                await SendTextMessageAsync($@"618275695:AAG-XYo-WgQeEgCufi8hX37r7vfLw0RBMPE", 313788580, text);
            }
            catch (Exception ex)
            {
            }
        }
    }
}
