﻿using Base.Entities;
using Base.Interface;
using Base.Interface.Dto;
using Base.Services.Interface;
using System.Linq;
using System.Threading.Tasks;

namespace Base.Services.Implementation
{
    public class BaseResourceService<T> : IBaseResourceService<T> where T : BaseEntity
    {
        private readonly IGenericRepository<T> _repo;
        protected LeadsoftUserContext _userContext;

        public BaseResourceService(LeadsoftUserContext userContext,
                                    IGenericRepository<T> repo)
        {
            _userContext = userContext;
            _repo = repo;
        }

        public virtual async Task<TaskResult<T>> GetAsync(object id)
        {
            var entity = await _repo.FindByIdAsync(id);
            if (entity == null)
                return new FailTaskResult<T>($@"ID {id} not existed");

            return new SuccessTaskResult<T>(entity);
        }

        public virtual async Task<PagingResult<T>> GetListAsync(PagingRequest request)
        {
            var data = await _repo.GetAllAsync();

            return new PagingResult<T>()
            {
                TotalPageCount = 1,
                TotalRecord = data.Count(),
                IsSuccess = true,
                PageSize = data.Count(),
                CurrentPageIndex = 1,
                Result = data
            };
        }
        public virtual async Task<TaskResult<long>> AddAsync(T entity)
        {
            var validateResult = await ValidateAsync(entity);
            if (validateResult.IsSuccess == false)
                return validateResult;

            entity = await _repo.AddAsync(entity);
            return new SuccessTaskResult<long>(entity.ID);
        }

        public virtual async Task<TaskResult<bool>> UpdateAsync(T entity)
        {
            var validateResult = await ValidateAsync(entity);
            if (validateResult.IsSuccess == false)
                return new FailTaskResult<bool>(validateResult.Log);

            var isSuccess = await _repo.UpdateAsync(entity);

            return new SuccessTaskResult<bool>(isSuccess);
        }

        public virtual async Task<TaskResult<bool>> DeleteAsync(long id)
        {
            var entity = await _repo.FindByIdAsync(id);
            if (entity == null)
                return new FailTaskResult<bool>($@"ID {id} not existed");

            var isSuccess = await _repo.DeleteByIdAsync(id);

            return new SuccessTaskResult<bool>(isSuccess);
        }

        protected virtual async Task<TaskResult<long>> ValidateAsync(T entity)
        {
            return await Task.FromResult(new SuccessTaskResult<long>());
        }
    }
}