﻿using Base.Interface.Dto;
using Base.Services.Interface.Services;
using Novell.Directory.Ldap;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Base.Services.Implementation.Services
{
    public class LDAPService: ILDAPService
    {
        public async Task<TaskResult<bool>> AuthenticateLdapAsync(string directorySite, string userName, string password)
        {
            //return await Task.FromResult(new FailTaskResult<bool>($@"Web temporary disable now"));
            return await Task.FromResult(new SuccessTaskResult<bool>());
            //using (var cn = new LdapConnection())
            //{
            //    try
            //    {
            //        cn.Connect(directorySite, 389);
            //        cn.Bind(userName, password);
            //        return await Task.FromResult(new SuccessTaskResult<bool>());
            //    }
            //    catch (Exception ex)
            //    {
            //        return new FailTaskResult<bool>(ex.ToString());
            //    }
            //}
        }
    }
}
