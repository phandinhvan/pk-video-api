﻿using Base.Services.Interface;
using MassTransit;
using System;
using System.Threading.Tasks;

namespace Base.Services.Implementation
{
    public class MessageProducer : IMessageProducer
    {
        private IBusControl _bus;

        public MessageProducer(IBusControl bus)
        {
            _bus = bus;
        }

        public IRequestClient<TRequest, TResponse> CreateRequestClient<TRequest, TResponse>(string endpoint, TimeSpan tp)
            where TRequest : class
            where TResponse : class
        {
            return _bus.CreateRequestClient<TRequest, TResponse>(new Uri(endpoint), tp);
        }

        public async Task PublishAsync(object message)
        {
            await _bus.Publish(message);
        }

        public async Task PublishAsync(object message, TimeSpan ttl)
        {
            await _bus.Publish(message, ctx =>
            {
                ctx.TimeToLive = ttl;
            });
        }

        public async Task SendAsync(object message, string sendEndpoint)
        {
            var endpoint = await _bus.GetSendEndpoint(new Uri(sendEndpoint));
            await endpoint.Send(message);
        }

        public async Task SendAsync(object message, string sendEndpoint, TimeSpan ttl)
        {
            var endpoint = await _bus.GetSendEndpoint(new Uri(sendEndpoint));
            await endpoint.Send(message, ctx =>
            {
                ctx.TimeToLive = ttl;
            });
        }
    }
}