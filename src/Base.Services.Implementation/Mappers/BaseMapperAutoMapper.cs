﻿using AutoMapper;
using Base.Services.Interface.Mappers;
using System;

namespace Base.Services.Implementation.Mappers
{
    public abstract class BaseMapperAutoMapper: IBaseMapper
    {
        private IMapper _mapper;

        public Action<object, object> BeforeMap { set; get; }
        public Action<object, object> AfterMap { set; get; }

        //private ConcurrentDictionary<string, ColumnMap> dicColumnMapperByColumnNames = new ConcurrentDictionary<string, ColumnMap>(StringComparer.OrdinalIgnoreCase);
        //private ConcurrentDictionary<string, ColumnMap> dicColumnMapperByColumnAlias = new ConcurrentDictionary<string, ColumnMap>(StringComparer.OrdinalIgnoreCase);

        //private ConcurrentDictionary<string, PropertyInfo> dicDestProperties = new ConcurrentDictionary<string, PropertyInfo>(StringComparer.OrdinalIgnoreCase);
        //private ConcurrentDictionary<string, PropertyInfo> dicSourceProperties = new ConcurrentDictionary<string, PropertyInfo>(StringComparer.OrdinalIgnoreCase);

        public BaseMapperAutoMapper()
        {
            SetMapper(new Mapper(CreateMapperConfiguration()));
        }

        public void SetMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public TDestination Map<TDestination>(object source)
        {
            return Map<TDestination>(source, BeforeMap, AfterMap);
        }

        public TDestination Map<TDestination>(object source, Action<TDestination> after = null)
        {
            var dest = Map<TDestination>(source);

            if (dest != null && after != null)
            {
                after(dest);
            }

            return dest;
        }

        public TDestination Map<TDestination>(object source, Action<object, object> beforeMap = null, Action<object, object> afterMap = null)
        {
            if (source == null)
            {
                return default(TDestination);
            }

            return _mapper.Map<TDestination>(source, opts =>
            {
                if (beforeMap != null)
                    opts.BeforeMap(beforeMap);
                if (afterMap != null)
                    opts.AfterMap(afterMap);
            });
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            return _mapper.Map(source, destination);
        }

        protected IConfigurationProvider CreateMapperConfiguration()
        {
            return new MapperConfiguration(ConfigMapperProvider);
        }

        protected virtual void ConfigMapperProvider(IMapperConfigurationExpression cfg)
        {
            //cfg.CreateMap(TSource, TDest>();
            //cfg.CreateMap<TDest, TSource>();
        }

        public object GetMapEngine()
        {
            return _mapper;
        }

        public string GetDestinationPropertyFor<TSrc, TDst>(MapperConfiguration mapper, string sourceProperty)
        {
            //var map = mapper.FindTypeMapFor<TSrc, TDst>();
            //var propertyMap = map.GetPropertyMaps().First(pm => pm.SourceMember == typeof(TSrc).(sourceProperty));

            //return propertyMap.DestinationProperty.Name;
            return string.Empty;
        }

        //public IBaseMapper AddColumnMapper(string columnName, string columnAlias)
        //{
        //    AddColumnMapper(new ColumnMap(columnName, columnAlias));
        //    return this;
        //}

        //public IBaseMapper AddColumnMapper(ColumnMap columnMapper)
        //{
        //    dicColumnMapperByColumnNames.TryAdd(columnMapper.ColumnName, columnMapper);
        //    dicColumnMapperByColumnAlias.TryAdd(columnMapper.ColumnAlias, columnMapper);
        //    return this;
        //}

        //public ColumnMap GetColumnMapperByColumnName(string columnName)
        //{
        //    ColumnMap mapper = new ColumnMap(columnName, columnName);//default
        //    dicColumnMapperByColumnNames.TryGetValue(columnName, out mapper);
        //    return mapper;
        //}

        //public ColumnMap GetColumnMapperByColumnAlias(string columnAlias)
        //{
        //    ColumnMap mapper = null;
        //    dicColumnMapperByColumnAlias.TryGetValue(columnAlias, out mapper);
        //    if (mapper == null)
        //    {
        //        if (dicSourceProperties.ContainsKey(columnAlias))//columnalias = columnName
        //            mapper = new ColumnMap(columnAlias, columnAlias);//default
        //    }

        //    return mapper;
        //}

        //public string GetAliasByColumnName(string columnName)
        //{
        //    var mapper = GetColumnMapperByColumnName(columnName);
        //    return mapper.ColumnAlias;
        //}

        //public string GetColumnNameByAlias(string columnAlias)
        //{
        //    var mapper = GetColumnMapperByColumnAlias(columnAlias);
        //    return mapper.ColumnName;
        //}

        //public virtual QueryParameter MapToQueryParameter(QueryRequest queryRequest, QueryParameter parameter)
        //{
        //    if (queryRequest == null) return parameter;

        //    var fields = ParseFields(queryRequest.Fields);
        //    List<ColumnMap> lstColumnMapper = new List<ColumnMap>();
        //    if (fields == null || fields.Count() == 0)//we need select all here
        //    {
        //        foreach (var pair in dicDestProperties)
        //        {
        //            var columnMapper = GetColumnMapperByColumnAlias(pair.Key);
        //            if (columnMapper != null)
        //                lstColumnMapper.Add(columnMapper);
        //        }
        //        fields = lstColumnMapper;
        //    }

        //    parameter.Fields = fields;

        //    parameter.Paging = ParsePaging(queryRequest.PageIndex, queryRequest.PageSize);
        //    parameter.OrderBy = ParseOrderBy(queryRequest.OrderBy);

        //    return parameter;
        //}

        //public virtual QueryParameter CreateQueryParameter(QueryRequest queryRequest)
        //{
        //    var parameter = new QueryParameter();
        //    if (queryRequest == null) return parameter;

        //    return MapToQueryParameter(queryRequest, parameter);
        //}
        //public FindByIdParameter CreateFindByIdParameter(FindByIdRequest findByIdRequest)
        //{
        //    var parameter = new FindByIdParameter();
        //    if (findByIdRequest == null) return parameter;

        //    parameter.Id = findByIdRequest.Id;

        //    var fields = ParseFields(findByIdRequest.Fields);
        //    List<ColumnMap> lstColumnMapper = new List<ColumnMap>();
        //    if (fields == null || fields.Count() == 0)//we need select all here
        //    {
        //        foreach (var pair in dicDestProperties)
        //        {
        //            var columnMapper = GetColumnMapperByColumnAlias(pair.Key);
        //            if (columnMapper != null)
        //                lstColumnMapper.Add(columnMapper);
        //        }
        //        fields = lstColumnMapper;
        //    }

        //    parameter.Fields = fields;
        //    return parameter;
        //}

        //protected virtual List<ColumnMap> ParseFields(string fields)
        //{
        //    if (string.IsNullOrWhiteSpace(fields)) return null;

        //    var arrFields = fields.Split(Constants.SEPARATOR_CHARACTERS, StringSplitOptions.RemoveEmptyEntries);
        //    List<ColumnMap> lstColumnMappers = new List<ColumnMap>();
        //    foreach (var fieldName in arrFields)
        //    {
        //        var columnMapper = GetColumnMapperByColumnAlias(fieldName.Trim());
        //        if (columnMapper != null)
        //            lstColumnMappers.Add(columnMapper);
        //    }

        //    return lstColumnMappers;
        //}

        //protected virtual PagingParameter ParsePaging(int pageIndex, int pageSize)
        //{
        //    if (pageIndex <= 0 && pageSize <= 0)
        //    {
        //        pageIndex = 1;
        //        pageSize = int.MaxValue;//no paging
        //    }
        //    else
        //    {
        //        if (pageIndex <= 0)
        //            pageIndex = 1;

        //        if (pageSize <= 0)
        //            pageSize = Constants.DEFAULT_PAGE_SIZE;
        //    }
        //    return new PagingParameter
        //    {
        //        PageIndex = pageIndex,
        //        PageSize = pageSize
        //    };
        //}

        //protected virtual List<OrderByParameter> ParseOrderBy(string orderbyFields)
        //{
        //    if (string.IsNullOrWhiteSpace(orderbyFields)) return null;

        //    var arrFields = orderbyFields.Split(Constants.SEPARATOR_CHARACTERS, StringSplitOptions.RemoveEmptyEntries);
        //    List<OrderByParameter> lstOrderParams = new List<OrderByParameter>();
        //    foreach (var fieldName in arrFields)
        //    {
        //        OrderByType type = OrderByType.ASC;
        //        if (fieldName.StartsWith("-"))
        //            type = OrderByType.DESC;

        //        string fieldAlias = fieldName.Trim('-');
        //        var field = GetColumnMapperByColumnAlias(fieldAlias);
        //        if (field != null)
        //            lstOrderParams.Add(new OrderByParameter()
        //            {
        //                Type = type,
        //                Field = field
        //            });
        //    }
        //    return lstOrderParams;
        //}

        //public virtual QueryParameter CreateQueryParameterFromSearchBase(SearchBaseRequest request, string aliasFieldId = "Id")
        //{
        //    return new QueryParameter();
        //}

        //protected QueryParameter CreateSearchQueryParameter(ColumnMapHelper colMapHelper, ModuleType moduleType, SearchBaseRequest request, string aliasFieldId = "Id")
        //{
        //    ColumnMap defaultField = null;

        //    var param = new QueryParameter
        //    {
        //        //select
        //        Fields = colMapHelper.GetSearchFields(moduleType, request.Fields),

        //        //paging
        //        Paging = ParsePaging(request.PageIndex, request.PageSize)
        //    };

        //    if (param.Fields.Count == 0)
        //    {
        //        defaultField = defaultField ?? colMapHelper.GetSearchField(moduleType, aliasFieldId);

        //        if (defaultField != null)
        //        {
        //            param.Fields.Add(defaultField);
        //        }
        //    }

        //    //force always paging
        //    if (param.Paging.PageSize == int.MaxValue)
        //        param.Paging.PageSize = Constants.DEFAULT_PAGE_SIZE;

        //    //orderby
        //    var orderBy = new List<OrderByParameter>();

        //    if (!string.IsNullOrWhiteSpace(request.OrderBy))
        //    {
        //        orderBy = request.OrderBy.Split(Constants.SEPARATOR_CHARACTERS, StringSplitOptions.RemoveEmptyEntries)
        //            .Select(name => new OrderByParameter(name.StartsWith("-") ? OrderByType.DESC : OrderByType.ASC)
        //            {
        //                Field = colMapHelper.GetSearchField(moduleType, name.Trim('-'))
        //            })
        //            .Where(a => a.Field != null && a.Field.CanSort).ToList();
        //    }

        //    if (orderBy.Count == 0)
        //    {
        //        defaultField = defaultField ?? colMapHelper.GetSearchField(moduleType, aliasFieldId);

        //        if (defaultField != null)
        //        {
        //            orderBy.Add(new OrderByParameter(OrderByType.DESC) { Field = defaultField });
        //        }
        //    }

        //    param.OrderBy = orderBy;

        //    return param;
        //}
    }
}