﻿using Autofac;
using Leadsoft.Zatara.Services.Implementation;
using Leadsoft.Zatara.Services.Interface;
using PuppeteerSharp;
using System.Threading;

namespace Leadsoft.Zatara.AgentService
{
    internal class Program
    {
        private static ZataraBootstrapper bootstrapper = null;

        private static void Main(string[] args)
        {
            bootstrapper = new ZataraBootstrapper();
            // var agentService = bootstrapper.Container.Resolve<IAgentService>();

            // agentService.StartAgentServiceAsync().Wait();

            new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision).Wait();
            var browser = Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true
            }).Result;
            var page = browser.NewPageAsync().Result;
            page.GoToAsync("http://video.pungkookvn.com:9000/10112001/0H5AQh9SycceZxt.mp4").Wait();

            Thread.Sleep(10000);
            page.ScreenshotAsync("abc.png").Wait();

            Thread.Sleep(-1);
        }
    }
}