﻿using Autofac;
using Base.Services.Interface;
using Leadsoft.Zatara.Services.Interface;
using Serilog;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Zatara.Services.ImplementationTest
{
    public class VideoServiceTest : BaseTest.ZataraBaseTest
    {
        [Fact]
        public void LoadDesignerTemplateFromFile_Test()
        {
            var service = bootstrapper.Container.Resolve<IVideoTemplateService>();

            var templateFile = $@"/Users/macbook/Documents/projects/video-service/template/templates-new-auto/simple15.osp";
            var lst = service.LoadDesignerTemplateFromFile(templateFile);

            Assert.False(lst.Count == 0);
        }

        [Fact]
        public async Task UploadFile_Test()
        {
            var service = bootstrapper.Container.Resolve<IVideoTemplateService>();

            var templateFile = $@"/Users/macbook/Documents/projects/video-service/template/templates-new-auto/simple15.osp";
            await service.UploadFileAsync(1, templateFile);
        }
    }
}
