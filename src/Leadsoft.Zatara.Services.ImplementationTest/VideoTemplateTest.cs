﻿using Autofac;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface;
using Leadsoft.Zatara.Services.Interface;
using Serilog;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Zatara.Services.ImplementationTest
{
    public class VideoTemplateTest : BaseTest.ZataraBaseTest
    {
        [Fact]
        public async Task CreateRequestAsync_Test()
        {
            var service = bootstrapper.Container.Resolve<IVideoGeneratorRequestService>();
            var requestRepo = bootstrapper.Container.Resolve<IVideoGeneratorRequestRepository>();
            var requestItemRepo = bootstrapper.Container.Resolve<IVideoGeneratorRequestRepository>();

            var requestInfo = await requestRepo.AddAsync(new VideoGeneratorRequest(){
                VideoTemplateId = 1,
                NumberOfPictureUpload = 15,
                IsUploadFileCompleted = true,
                CreatedDate = DateTime.UtcNow
            });

            
        }
    }
}
