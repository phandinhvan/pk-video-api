﻿using Base.Services.Interface;
using Leadsoft.RestApi.NetCorePlatform.Auth;
using Nancy;

namespace Leadsoft.RestApi.NetCorePlatform
{
    public interface ILeadsoftUserContextFactory
    {
        void PopulateDataForContext(LeadsoftUserContext userContext, NancyContext nancyContext, AuthToken authToken);
    }
}