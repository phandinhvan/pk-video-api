﻿using Base.Services.Interface;
using Leadsoft.RestApi.NetCorePlatform.Auth;
using Nancy;

namespace Leadsoft.RestApi.NetCorePlatform
{
    public class DefaultLeadsoftContextFactory : ILeadsoftUserContextFactory
    {
        public DefaultLeadsoftContextFactory()
        {
        }

        public void PopulateDataForContext(LeadsoftUserContext userContext, NancyContext nancyContext, AuthToken authToken)
        {
            userContext.UserId = authToken.UserId;
            userContext.UserName = authToken.UserName;
        }
    }
}