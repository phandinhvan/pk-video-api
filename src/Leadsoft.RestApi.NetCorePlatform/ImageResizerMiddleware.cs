﻿namespace Leadsoft.RestApi.NetCorePlatform
{
    /*
        public class ImageResizerMiddleware
        {
            private static readonly IMemoryCache _memoryCache = new MemoryCache(new MemoryCacheOptions() {
            });
            static ProgramRootStorageClient _storageClient;
            static ILogger _logger;

            private static readonly string[] suffixes = new string[] {
                ".png",
                ".jpg",
                ".jpeg"
            };

            public static AppFunc Middleware(AppFunc next, ProgramRootStorageClient storageClient, ILogger logger)
            {
                _logger = logger;
                _storageClient = storageClient;
                return async env =>
                {
                    var owinContext = new OwinContext(env);
                    var path = owinContext.Request.Path;

                    // hand to next middleware if we are not dealing with an image
                    if (!IsImagePath(path))
                    {
                        await next(env);
                        return;
                    }

                    string relativePath = path.Value.Trim('/');

                    // hand to next middleware if we are dealing with an image but it doesn't have any usable resize querystring params
                    var resizeParams = GetResizeParams(path, owinContext.Request.Query);
                    if (!resizeParams.hasParams || (resizeParams.w == 0 && resizeParams.h == 0))
                    {
                        var rawImageData = GetRawImageData(relativePath);
                        if(rawImageData == null)
                        {
                            await next(env);
                            return;
                        }

                        owinContext.Response.ContentType = resizeParams.format == "png" ? "image/png" : "image/jpeg";
                        owinContext.Response.ContentLength = rawImageData.Length;
                        await owinContext.Response.Body.WriteAsync(rawImageData, 0, (int)rawImageData.Length);
                    }else//resize it
                    {
                        // if we got this far, resize it
                        _logger.Information($"Resizing {path.Value} with params {resizeParams}");

                        // write to stream
                        var imageData = GetResizeImageData(relativePath, resizeParams);
                        owinContext.Response.ContentType = resizeParams.format == "png" ? "image/png" : "image/jpeg";
                        owinContext.Response.ContentLength = imageData.Size;

                        await owinContext.Response.Body.WriteAsync(imageData.ToArray(), 0, (int)imageData.Size);
                        // cleanup
                        imageData.Dispose();
                    }
                    await next(env);
                    return;
                };
            }
            private static ResizeParams GetResizeParams(PathString path, IReadableStringCollection query)
            {
                ResizeParams resizeParams = new ResizeParams();

                // before we extract, do a quick check for resize params
                resizeParams.hasParams =
                    resizeParams.GetType().GetTypeInfo()
                    .GetFields().Where(f => f.Name != "hasParams")
                    .Any(f => query[f.Name] != null);
                //.Any(f => query.ContainsKey(f.Name));

                // if no params present, bug out
                if (!resizeParams.hasParams)
                    return resizeParams;

                // extract resize params

                if (query["format"] != null)
                    resizeParams.format = query["format"];
                else
                    resizeParams.format = path.Value.Substring(path.Value.LastIndexOf('.') + 1);

                if (query["autorotate"] != null)
                    bool.TryParse(query["autorotate"], out resizeParams.autorotate);

                int quality = 100;
                if (query["quality"] != null)
                    int.TryParse(query["quality"], out quality);
                resizeParams.quality = quality;

                int w = 0;
                if (query["w"] != null)
                    int.TryParse(query["w"], out w);
                resizeParams.w = w;

                int h = 0;
                if (query["h"] != null)
                    int.TryParse(query["h"], out h);
                resizeParams.h = h;

                resizeParams.mode = "max";
                // only apply mode if it's a valid mode and both w and h are specified
                if (h != 0 && w != 0 && query["mode"] != null && ResizeParams.modes.Any(m => query["mode"] == m))
                    resizeParams.mode = query["mode"];

                return resizeParams;
            }
            private static bool IsImagePath(PathString path)
            {
                if (path == null || !path.HasValue)
                    return false;

                return suffixes.Any(x => path.Value.EndsWith(x, StringComparison.OrdinalIgnoreCase));
            }

            private static byte[] GetRawImageData(string imageRelativeFilePath)
            {
                var fileStorageInfo = _storageClient.GetFileInfoByRelative(imageRelativeFilePath);
                if (fileStorageInfo == null) return null;

                // check cache and return if cached
                long cacheKey;
                unchecked
                {
                    cacheKey = imageRelativeFilePath.GetHashCode() + fileStorageInfo.LastModifiedFileTime.GetHashCode();
                }

                byte[] imageBytes;
                bool isCached = _memoryCache.TryGetValue<byte[]>(cacheKey, out imageBytes);
                if (isCached)
                {
                    _logger.Information("Serving from cache");
                    return imageBytes;
                }

                string tempFilePath = Path.GetTempFileName();
                _storageClient.DownloadFileByRelative(imageRelativeFilePath, tempFilePath);
                if (File.Exists(tempFilePath) == false) return null;

                imageBytes = File.ReadAllBytes(tempFilePath);
                File.Delete(tempFilePath);

                // cache the result
                _memoryCache.Set<byte[]>(cacheKey, imageBytes);

                return imageBytes;
            }

            private static SKData GetResizeImageData(string imageRelativePath, ResizeParams resizeParams)
            {
                // check cache and return if cached
                long cacheKey;
                unchecked
                {
                    cacheKey = imageRelativePath.GetHashCode() + resizeParams.ToString().GetHashCode();
                }

                SKData imageData;
                byte[] imageBytes;
                bool isCached = _memoryCache.TryGetValue<byte[]>(cacheKey, out imageBytes);
                if (isCached)
                {
                    _logger.Information("Serving from cache");
                    return SKData.CreateCopy(imageBytes);
                }

                string tempFilePath = Path.GetTempFileName();
                _storageClient.DownloadFileByRelative(imageRelativePath, tempFilePath);
                if (File.Exists(tempFilePath) == false) return null;

                SKCodecOrigin origin; // this represents the EXIF orientation

                using (var stream = File.OpenRead(tempFilePath))
                {
                    var bitmap = LoadBitmap(stream, out origin); // always load as 32bit (to overcome issues with indexed color)

                    // if autorotate = true, and origin isn't correct for the rotation, rotate it
                    if (resizeParams.autorotate && origin != SKCodecOrigin.TopLeft)
                        bitmap = RotateAndFlip(bitmap, origin);

                    // if either w or h is 0, set it based on ratio of original image
                    if (resizeParams.h == 0)
                        resizeParams.h = (int)Math.Round(bitmap.Height * (float)resizeParams.w / bitmap.Width);
                    else if (resizeParams.w == 0)
                        resizeParams.w = (int)Math.Round(bitmap.Width * (float)resizeParams.h / bitmap.Height);

                    // if we need to crop, crop the original before resizing
                    if (resizeParams.mode == "crop")
                        bitmap = Crop(bitmap, resizeParams);

                    // store padded height and width
                    var paddedHeight = resizeParams.h;
                    var paddedWidth = resizeParams.w;

                    // if we need to pad, or max, set the height or width according to ratio
                    if (resizeParams.mode == "pad" || resizeParams.mode == "max")
                    {
                        var bitmapRatio = (float)bitmap.Width / bitmap.Height;
                        var resizeRatio = (float)resizeParams.w / resizeParams.h;

                        if (bitmapRatio > resizeRatio) // original is more "landscape"
                            resizeParams.h = (int)Math.Round(bitmap.Height * ((float)resizeParams.w / bitmap.Width));
                        else
                            resizeParams.w = (int)Math.Round(bitmap.Width * ((float)resizeParams.h / bitmap.Height));
                    }

                    // resize
                    var resizedImageInfo = new SKImageInfo(resizeParams.w, resizeParams.h, SKImageInfo.PlatformColorType, bitmap.AlphaType);
                    var resizedBitmap = bitmap.Resize(resizedImageInfo, SKBitmapResizeMethod.Lanczos3);

                    // optionally pad
                    if (resizeParams.mode == "pad")
                        resizedBitmap = Pad(resizedBitmap, paddedWidth, paddedHeight, resizeParams.format != "png");

                    // encode
                    var resizedImage = SKImage.FromBitmap(resizedBitmap);
                    var encodeFormat = resizeParams.format == "png" ? SKEncodedImageFormat.Png : SKEncodedImageFormat.Jpeg;
                    imageData = resizedImage.Encode(encodeFormat, resizeParams.quality);

                    // cache the result
                    _memoryCache.Set<byte[]>(cacheKey, imageData.ToArray());

                    // cleanup
                    resizedImage.Dispose();
                    bitmap.Dispose();
                    resizedBitmap.Dispose();
                }

                File.Delete(tempFilePath);
                return imageData;
            }
            private static SKBitmap LoadBitmap(Stream stream, out SKCodecOrigin origin)
            {
                using (var s = new SKManagedStream(stream))
                {
                    using (var codec = SKCodec.Create(s))
                    {
                        origin = codec.Origin;
                        var info = codec.Info;
                        var bitmap = new SKBitmap(info.Width, info.Height, SKImageInfo.PlatformColorType, info.IsOpaque ? SKAlphaType.Opaque : SKAlphaType.Premul);

                        IntPtr length;
                        var result = codec.GetPixels(bitmap.Info, bitmap.GetPixels(out length));
                        if (result == SKCodecResult.Success || result == SKCodecResult.IncompleteInput)
                        {
                            return bitmap;
                        }
                        else
                        {
                            throw new ArgumentException("Unable to load bitmap from provided data");
                        }
                    }
                }
            }
            private static SKBitmap Crop(SKBitmap original, ResizeParams resizeParams)
            {
                var cropSides = 0;
                var cropTopBottom = 0;

                // calculate amount of pixels to remove from sides and top/bottom
                if ((float)resizeParams.w / original.Width < resizeParams.h / original.Height) // crop sides
                    cropSides = original.Width - (int)Math.Round((float)original.Height / resizeParams.h * resizeParams.w);
                else
                    cropTopBottom = original.Height - (int)Math.Round((float)original.Width / resizeParams.w * resizeParams.h);

                // setup crop rect
                var cropRect = new SKRectI
                {
                    Left = cropSides / 2,
                    Top = cropTopBottom / 2,
                    Right = original.Width - cropSides + cropSides / 2,
                    Bottom = original.Height - cropTopBottom + cropTopBottom / 2
                };

                // crop
                SKBitmap bitmap = new SKBitmap(cropRect.Width, cropRect.Height);
                original.ExtractSubset(bitmap, cropRect);
                original.Dispose();

                return bitmap;
            }
            private static SKBitmap Pad(SKBitmap original, int paddedWidth, int paddedHeight, bool isOpaque)
            {
                // setup new bitmap and optionally clear
                var bitmap = new SKBitmap(paddedWidth, paddedHeight, isOpaque);
                var canvas = new SKCanvas(bitmap);
                if (isOpaque)
                    canvas.Clear(new SKColor(255, 255, 255)); // we could make this color a resizeParam
                else
                    canvas.Clear(SKColor.Empty);

                // find co-ords to draw original at
                var left = original.Width < paddedWidth ? (paddedWidth - original.Width) / 2 : 0;
                var top = original.Height < paddedHeight ? (paddedHeight - original.Height) / 2 : 0;

                var drawRect = new SKRectI
                {
                    Left = left,
                    Top = top,
                    Right = original.Width + left,
                    Bottom = original.Height + top
                };

                // draw original onto padded version
                canvas.DrawBitmap(original, drawRect);
                canvas.Flush();

                canvas.Dispose();
                original.Dispose();

                return bitmap;
            }
            private static SKBitmap RotateAndFlip(SKBitmap original, SKCodecOrigin origin)
            {
                // these are the origins that represent a 90 degree turn in some fashion
                var differentOrientations = new SKCodecOrigin[]
                {
                    SKCodecOrigin.LeftBottom,
                    SKCodecOrigin.LeftTop,
                    SKCodecOrigin.RightBottom,
                    SKCodecOrigin.RightTop
                };

                // check if we need to turn the image
                bool isDifferentOrientation = differentOrientations.Any(o => o == origin);

                // define new width/height
                var width = isDifferentOrientation ? original.Height : original.Width;
                var height = isDifferentOrientation ? original.Width : original.Height;

                var bitmap = new SKBitmap(width, height, original.AlphaType == SKAlphaType.Opaque);

                // todo: the stuff in this switch statement should be rewritten to use pointers
                switch (origin)
                {
                    case SKCodecOrigin.LeftBottom:

                        for (var x = 0; x < original.Width; x++)
                            for (var y = 0; y < original.Height; y++)
                                bitmap.SetPixel(y, original.Width - 1 - x, original.GetPixel(x, y));
                        break;

                    case SKCodecOrigin.RightTop:

                        for (var x = 0; x < original.Width; x++)
                            for (var y = 0; y < original.Height; y++)
                                bitmap.SetPixel(original.Height - 1 - y, x, original.GetPixel(x, y));
                        break;

                    case SKCodecOrigin.RightBottom:

                        for (var x = 0; x < original.Width; x++)
                            for (var y = 0; y < original.Height; y++)
                                bitmap.SetPixel(original.Height - 1 - y, original.Width - 1 - x, original.GetPixel(x, y));

                        break;

                    case SKCodecOrigin.LeftTop:

                        for (var x = 0; x < original.Width; x++)
                            for (var y = 0; y < original.Height; y++)
                                bitmap.SetPixel(y, x, original.GetPixel(x, y));
                        break;

                    case SKCodecOrigin.BottomLeft:

                        for (var x = 0; x < original.Width; x++)
                            for (var y = 0; y < original.Height; y++)
                                bitmap.SetPixel(x, original.Height - 1 - y, original.GetPixel(x, y));
                        break;

                    case SKCodecOrigin.BottomRight:

                        for (var x = 0; x < original.Width; x++)
                            for (var y = 0; y < original.Height; y++)
                                bitmap.SetPixel(original.Width - 1 - x, original.Height - 1 - y, original.GetPixel(x, y));
                        break;

                    case SKCodecOrigin.TopRight:

                        for (var x = 0; x < original.Width; x++)
                            for (var y = 0; y < original.Height; y++)
                                bitmap.SetPixel(original.Width - 1 - x, y, original.GetPixel(x, y));
                        break;

                }

                original.Dispose();

                return bitmap;
            }
        }

        internal struct ResizeParams
        {
            public bool hasParams;
            public int w;
            public int h;
            public bool autorotate;
            public int quality; // 0 - 100
            public string format; // png, jpg, jpeg
            public string mode; // pad, max, crop, stretch

            public static string[] modes = new string[] { "pad", "max", "crop", "stretch" };

            public override string ToString()
            {
                var sb = new StringBuilder();
                sb.Append($"w: {w}, ");
                sb.Append($"h: {h}, ");
                sb.Append($"autorotate: {autorotate}, ");
                sb.Append($"quality: {quality}, ");
                sb.Append($"format: {format}, ");
                sb.Append($"mode: {mode}");

                return sb.ToString();
            }
        }
    */
}