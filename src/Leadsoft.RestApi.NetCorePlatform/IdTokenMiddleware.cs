﻿using LibOwin;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;

namespace Leadsoft.RestApi.NetCorePlatform
{
    public class IdTokenMiddleware
    {
        public static AppFunc Middleware(AppFunc next, string omnicasaEndUserKey)
        {
            return env =>
            {
                var ctx = new OwinContext(env);
                if (ctx.Request.Headers.ContainsKey(omnicasaEndUserKey))
                {
                    var tokenHandler = new JwtSecurityTokenHandler();
                    SecurityToken token;
                    var userPrincipal = tokenHandler.ValidateToken(ctx.Request.Headers[omnicasaEndUserKey], new TokenValidationParameters(), out token);
                    ctx.Set(omnicasaEndUserKey, userPrincipal);
                }
                return next(env);
            };
        }
    }
}
