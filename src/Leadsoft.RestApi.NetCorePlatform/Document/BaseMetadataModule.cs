﻿using Nancy.Swagger;
using Nancy.Swagger.Modules;
using Nancy.Swagger.Services;
using Nancy.Swagger.Services.RouteUtils;
using Swagger.ObjectModel;
using System.Collections.Generic;
using System.Linq;

namespace Leadsoft.RestApi.NetCorePlatform.Document
{
    public abstract class BaseMetadataModule<T> : BaseMetadataModule where T : class
    {
        public BaseMetadataModule(string resourceName, ISwaggerModelCatalog modelCatalog, ISwaggerTagCatalog tagCatalog)
            : base(resourceName, modelCatalog, tagCatalog)
        {
            //InitBaseDocuments<T>();
        }
    }

    public abstract class BaseMetadataModule : SwaggerMetadataModule
    {
        protected readonly string ResourceName;

        public BaseMetadataModule(string resourceName, ISwaggerModelCatalog modelCatalog, ISwaggerTagCatalog tagCatalog)
          : base(modelCatalog, tagCatalog)
        {
            ResourceName = resourceName;

            RouteDescriber.AddBaseTag(new Tag
            {
                Name = resourceName,
                Description = $@"Operations for {resourceName}",
            });

            //RouteDescriber.AddAdditionalModels(typeof(ModuleType));
        }

        /// <summary>
        /// default: Code(200) Message(OK)
        /// </summary>
        protected HttpResponseMetadata[] GetResponseMetadataDefault(params HttpResponseMetadata[] extends)
        {
            var response = new HttpResponseMetadata[] { new HttpResponseMetadata { Code = 200, Message = "OK" } };

            if (extends?.Length > 0)
            {
                response = response.Union(extends).ToArray();
            }

            return response;
        }

        protected Parameter GetParameterLanguage()
        {
            return new Parameter { Name = "Accept-Language", In = ParameterIn.Header, Required = false, Description = "Language code (NL, FR, EN)", Default = "NL", Type = "string" };
        }

        protected Parameter[] GetParameterInQueryForPaging()
        {
            return new[]
            {
                new Parameter{Name = "Fields", In = ParameterIn.Query, Required = false, Description = "Fields, * -> all fields", Default = "*", Type = "string" },
                new Parameter{Name = "OrderBy", In = ParameterIn.Query, Required = false, Description = "Order by fields", Default = "Id", Type = "string" },
                new Parameter{Name = "PageIndex", In = ParameterIn.Query, Required = false, Description = "PageIndex", Default = 1, Type = "integer" },
                new Parameter{Name = "PageSize", In = ParameterIn.Query, Required = false, Description = "PageSize", Default = 20, Type = "integer" },
            };
        }

        #region Common

        protected virtual void CreateDocument<T>(string functionName, string note, string description, IEnumerable<HttpResponseMetadata> responsesMetadatas, params Parameter[] parameters)
        {
            ProcessCreateDocument<T>(functionName, note, description, responsesMetadatas, parameters);
        }

        protected virtual void CreateDocument<T>(string functionName, string note, string description, IEnumerable<HttpResponseMetadata> responsesMetadatas, IEnumerable<Parameter> parameters = null)
        {
            ProcessCreateDocument<T>(functionName, note, description, responsesMetadatas, parameters);
        }

        private void ProcessCreateDocument<T>(string functionName, string note, string description, IEnumerable<HttpResponseMetadata> responsesMetadatas, IEnumerable<Parameter> parameters = null)
        {
            var lstParameters = GetParameters(parameters);
            RouteDescriber.DescribeRouteWithParams<T>(functionName, note, description, responsesMetadatas, lstParameters);
        }

        protected virtual void CreateDocument(string functionName, string note, string description, IEnumerable<HttpResponseMetadata> responsesMetadatas, IEnumerable<Parameter> parameters = null)
        {
            var lstParameters = GetParameters(parameters);
            RouteDescriber.DescribeRouteWithParams(functionName, note, description, responsesMetadatas, lstParameters);
        }

        private List<Parameter> GetParameters(IEnumerable<Parameter> extendParameters)
        {
            var parameters = new List<Parameter>
            {
                //new Parameter{Name = "Authorization", In = ParameterIn.Header, Required = true, Description = "Bearer token", Default = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjEwMSwiVXNlck5hbWUiOiJIdXkgTmdvIDMiLCJDdXN0b21lcklkIjo2MDYsIlNjb3BlcyI6WyJhZG1pbiJdLCJFeHBpcmF0aW9uRGF0ZVRpbWUiOiIyMDI4LTAxLTI0VDE1OjQ3OjMzLjYxODE4MSswNzowMCJ9.mv7YZKON9P4uqQPpXM5C5Iy6SjPfcI2YK9BQNQ2CWIA", Type = "string" }
                new Parameter{Name = "Authorization", In = ParameterIn.Header, Required = true, Description = "Bearer token", Default = "Place authentication token here", Type = "string" }
            };

            if (extendParameters?.Count() > 0)
            {
                parameters.AddRange(extendParameters);
            }

            return parameters;
        }

        #endregion Common
    }
}