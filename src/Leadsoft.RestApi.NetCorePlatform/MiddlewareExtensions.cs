﻿using Leadsoft.RestApi.Platform;
using Leadsoft.RestApi.Platform.Logging;
using Serilog;
using System;
using System.Threading.Tasks;
using BuildFunc = System.Action<System.Func<
    System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>,
    System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>>>;

namespace Leadsoft.RestApi.NetCorePlatform
{
    public static class MiddlewareExtensions
    {
        public static BuildFunc UseMonitorAndLogging(this BuildFunc buildFunc, ILogger logger, Func<Task<bool>> healthCheck)
        {
            buildFunc(next => GlobalErrorLoggingMiddleware.Middleware(next, logger));
            buildFunc(next => CorrelationTokenMiddleware.Middleware(next));
            buildFunc(next => RequestLoggingMiddleware.Middleware(next, logger));
            buildFunc(next => PerformanceLoggingMiddleware.Middleware(next, logger));
            buildFunc(next => new MonitoringMiddleware(next, healthCheck).Invoke);

            return buildFunc;
        }

        public static BuildFunc UseAuthPlatform(this BuildFunc buildFunc, string requireScope, string endUserKey)
        {
            buildFunc(next => AuthorizationMiddleware.Middleware(next, requireScope));
            buildFunc(next => IdTokenMiddleware.Middleware(next, endUserKey));

            return buildFunc;
        }

/*
        public static BuildFunc UseImageResizer(this BuildFunc buildFunc, ProgramRootStorageClient storageClient, ILogger logger)
        {
            buildFunc(next => ImageResizerMiddleware.Middleware(next, storageClient, logger));
            return buildFunc;
        }
*/
    }
}