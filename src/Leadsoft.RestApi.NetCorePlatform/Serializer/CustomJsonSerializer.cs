﻿using Newtonsoft.Json;

namespace Leadsoft.RestApi.NetCorePlatform.Serializer
{
    public class CustomJsonSerializer: JsonSerializer
    {
        public CustomJsonSerializer()
        {
            Formatting = Formatting.Indented;
            DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            DateFormatString = "yyyy-MM-ddTHH:mm:ssZ";
            NullValueHandling = NullValueHandling.Ignore;
            //ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}