﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Leadsoft.RestApi.NetCorePlatform.Serializer
{
    public class PartialJsonResolver : DefaultContractResolver
    {
        private List<string> lstFields = new List<string>();

        public PartialJsonResolver(string fieldNeedSerialize)
        {
            var arrs = fieldNeedSerialize.ToUpper().Split(Leadsoft.Common.Constants.SEPARATOR_CHARACTERS, StringSplitOptions.RemoveEmptyEntries);
            lstFields.AddRange(arrs);
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = base.CreateProperty(member, memberSerialization);
            if (lstFields.Contains(member.Name.ToUpper()) == false)
                property.ShouldSerialize = (Predicate<object>)(instance => false);//not serialize

            return property;
        }
    }
}