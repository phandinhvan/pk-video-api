﻿namespace Leadsoft.RestApi.NetCorePlatform.Serializer
{
    public class PartialJsonSerializer : CustomJsonSerializer
    {
        public PartialJsonSerializer(string fieldNeedSerialize)
            : base()
        {
            ContractResolver = new PartialJsonResolver(fieldNeedSerialize);
        }
    }
}