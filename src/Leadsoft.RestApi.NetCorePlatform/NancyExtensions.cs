﻿using Nancy;
using Nancy.Bootstrapper;
using Nancy.Responses.Negotiation;
using Leadsoft.RestApi.NetCorePlatform.Errors.Enums;
using Leadsoft.RestApi.NetCorePlatform.Errors.Model;
using Base.Interface.Dto;
using System.IO;
using System.Text.RegularExpressions;
using System;
using System.Globalization;

namespace Leadsoft.RestApi.NetCorePlatform
{
    public static class NancyExtensions
    {
        public static void EnableCors(this IPipelines pipelines)
        {
            pipelines.AfterRequest += ctx =>
            {
                EnableCors(ctx.Response);
            };
        }

        public static void EnableCors(this Response response)
        {
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            response.Headers.Add("Access-Control-Allow-Methods", "PATCH, PUT, GET, POST, DELETE, OPTIONS");
            response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, responseType, x-requested-with, Authorization, Accept, Origin");
            response.Headers.Add("Access-Control-Expose-Headers", "X-PAGINATION-TOTAL, X-PAGINATION-PAGE-COUNT, X-PAGINATION-PAGE-SIZE, X-PAGINATION-PAGE-INDEX");
        }

        public static Negotiator Exception(this NancyModule module, HttpStatusCode code, ServiceErrorEnum internalCode, string msg)
        {
            return module.Negotiate
                .WithStatusCode(code)
                //.WithModel(new ServiceErrorModel(internalCode, msg))
                .WithModel(new TaskResult<string>()
                {
                    Code = (int)internalCode,
                    Log = msg
                })
                .WithContentType("application/json");
        }

        public static Negotiator Exception(this NancyModule module, HttpStatusCode code, int errorCode, string errorMessage)
        {
            return module.Negotiate
                .WithStatusCode(code)
                .WithModel(new { Code = errorCode, Details = errorMessage })
                .WithContentType("application/json");
        }

        public static Response FromPartialFile(this IResponseFormatter f, Request req, string path, string contentType)
        {
            return f.FromPartialStream(req, new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read), contentType);
        }
        public static Response FromPartialStream(this IResponseFormatter f, Request req, Stream stream, string contentType)
        {
            // Store the len
            var len = stream.Length;
            // Create the response now
            var res = f.FromStream(stream, contentType).
                        WithHeader("connection", "keep-alive").
                WithHeader("accept-ranges", "bytes");
            // Use the partial status code
            res.StatusCode = HttpStatusCode.PartialContent;
            long startI = 0;
            long contentLen = 0;
            foreach (var s in req.Headers["Range"])//bytes=512176-524287
            {
                var start = s.Split('=')[1];//512176-524287
                var m = Regex.Match(start, @"(\d+)-(\d+)?");
                start = m.Groups[1].Value;
                var end = len - 1;
                if (m.Groups[2] != null && !string.IsNullOrWhiteSpace(m.Groups[2].Value))
                {
                    end = Convert.ToInt64(m.Groups[2].Value);
                }

                startI = Convert.ToInt64(start);
                var length = len - startI;

                contentLen = length;
                if(end == 1)//apply for SAFARY
                    contentLen = 2;

                res.WithHeader("content-range", "bytes " + start + "-" + end + "/" + len);
                res.WithHeader("content-length", contentLen.ToString(CultureInfo.InvariantCulture));
            }
            if(contentLen == 2)
                stream.Seek(len - 2, SeekOrigin.Begin);
            else
                stream.Seek(startI, SeekOrigin.Begin);
            return res;
        }
    }
}