﻿using System;
using System.Collections.Generic;

namespace Leadsoft.RestApi.NetCorePlatform.Auth
{
    public class AuthToken
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public IEnumerable<string> Scopes { get; set; }
        public DateTime ExpirationDateTime { get; set; }

        public AuthToken()
        {
            Scopes = new string[] { "admin"};
        }

        public AuthToken(int customerId, int userId): this()
        {
            UserId = userId;
        }
    }
}
