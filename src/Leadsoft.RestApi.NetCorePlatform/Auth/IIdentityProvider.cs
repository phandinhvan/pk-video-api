﻿using Nancy;
using System.Security.Claims;

namespace Leadsoft.RestApi.NetCorePlatform.Auth
{
    public interface IIdentityProvider
    {
        ClaimsPrincipal GetUserIdentity(NancyContext context);
        string GenerateToken(object payload);
    }
}