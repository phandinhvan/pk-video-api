﻿using Base.Services.Interface;
using Jose;
using Nancy;
using System;
using System.Linq;
using System.Security.Claims;

namespace Leadsoft.RestApi.NetCorePlatform.Auth
{
    public sealed class IdentityProvider : IIdentityProvider
    {
        private readonly AuthSettings _authSettings;
        private LeadsoftUserContext _userContext;
        private readonly ILeadsoftUserContextFactory _gandalfContextFactory;
        public const string BEARER_DECLARATION = "Bearer ";
        public const string BASIC_DECLARATION = "Basic ";

        public IdentityProvider(ILeadsoftUserContextFactory omnicasaContextFactory, AuthSettings authSettings, LeadsoftUserContext userContext)
        {
            _gandalfContextFactory = omnicasaContextFactory;
            _authSettings = authSettings;
            _userContext = userContext;
        }

        public ClaimsPrincipal GetUserIdentity(NancyContext context)
        {
            try
            {
                var authorizationHeader = context.Request.Headers.Authorization;
                var jwt = authorizationHeader.Replace(BEARER_DECLARATION, "");
                if (string.IsNullOrWhiteSpace(jwt))
                    return null;

                var authToken = Jose.JWT.Decode<AuthToken>(jwt, _authSettings.SecretKey, JwsAlgorithm.HS256);

                if (authToken.ExpirationDateTime < DateTime.UtcNow)
                    return null;

                var authUser = new AuthUser(authToken.UserId, authToken.UserName);//, authToken.UserId);
                //_omnicasaContext.UserId = authToken.UserId;

                _gandalfContextFactory.PopulateDataForContext(_userContext, context, authToken);

                return new ClaimsPrincipal(new ClaimsIdentity(authUser, authToken.Scopes?.Select(claim => new Claim("scope", claim))));
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GenerateToken(object payload)
        {
            return Jose.JWT.Encode(payload, _authSettings.SecretKey, JwsAlgorithm.HS256);
        }
    }
}