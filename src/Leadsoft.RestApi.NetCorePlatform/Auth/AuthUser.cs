﻿using System.Security.Principal;

namespace Leadsoft.RestApi.NetCorePlatform.Auth
{
    internal class AuthUser : IIdentity
    {
        public string AuthenticationType => "Basic";
        public bool IsAuthenticated { get; }
        public string Name { get; }
        public int UserId { get; }

        public AuthUser(int userId, string name)//, Guid id)
        {
            Name = name;
            UserId = userId;
            IsAuthenticated = true;
        }
    }
}