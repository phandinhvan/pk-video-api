﻿using LibOwin;
using Serilog.Context;
using System;
using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;

namespace Leadsoft.RestApi.Platform
{
    public class CorrelationTokenMiddleware
    {
        public static AppFunc Middleware(AppFunc next)
        {
            return async env =>
            {
                Guid correlationToken;

                //try get correlation from header and set to Owin environment
                var owinContext = new OwinContext(env);
                if (!(owinContext.Request.Headers["Correlation-Token"] != null && Guid.TryParse(owinContext.Request.Headers["Correlation-Token"], out correlationToken)))
                    correlationToken = Guid.NewGuid();

                owinContext.Set("correlationToken", correlationToken.ToString());
                using (LogContext.PushProperty("CorrelationToken", correlationToken))
                    await next(env).ConfigureAwait(false);
            };
        }
    }
}