﻿using Nancy.Validation;
using System.Linq;

namespace Leadsoft.RestApi.NetCorePlatform.Validation.Nancy
{
    public static class ModelValidationResultExtensions
    {
        public static string GetDetailedErrorMessage(this ModelValidationResult modelValidationResult)
        {
            if (modelValidationResult == null) return string.Empty;

            var formattedErrors = modelValidationResult.Errors
                .Select(x => new { Key = x.Key, Errors = x.Value.Select(y => y.ErrorMessage) })
                .Select(x => string.Format("Parameter = {0}, Errors = ({1})", x.Key, string.Join(", ", x.Errors)));

            return string.Format("Validation failed for Request Parameters: ({0})",
                string.Join(", ", formattedErrors));
        }
    }
}