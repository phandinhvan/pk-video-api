﻿using Nancy;
using Nancy.ModelBinding;
using Nancy.Validation;
using Leadsoft.RestApi.NetCorePlatform.Errors.Specification.Exceptions;

namespace Leadsoft.RestApi.NetCorePlatform.Validation.Nancy
{
    public static class ModuleExtensions
    {
        public static TModel CustomBindAndValidate<TModel>(this NancyModule module, params string[] blacklistedProperties)
        {
            TModel model = module.Bind<TModel>(blacklistedProperties);

            InternalValidate(module, model);

            return model;
        }

        public static void InternalValidate<TModel>(this NancyModule module, TModel model)
        {
            if (model != null)
            {
                var modelValidationResult = module.Validate(model);
                if (!modelValidationResult.IsValid)
                {
                    throw new ValidationServiceErrorException(modelValidationResult);
                }
            }
        }
    }
}