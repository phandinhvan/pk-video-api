﻿using Nancy;
using Nancy.Owin;
using Nancy.TinyIoc;
using System.Security.Claims;

namespace Leadsoft.RestApi.NetCorePlatform
{
    public static class PlatformHelper
    {
        private static string _tokenUrl;
        private static string _clientName;
        private static string _clientSecret;

        public static void Config(string tokenUrl, string clientName, string clientSecret)
        {
            _tokenUrl = tokenUrl;
            _clientName = clientName;
            _clientSecret = clientSecret;
        }

        //public static TinyIoCContainer UseHttpClientFactory(this TinyIoCContainer self, NancyContext context)
        //{
        //    var correlationToken = string.Empty;
        //    if (context.GetOwinEnvironment().ContainsKey("correlationToken"))
        //        correlationToken = context.GetOwinEnvironment()["correlationToken"] as string;
                
        //    if (context.GetOwinEnvironment().ContainsKey("owin.RequestUser"))
        //    {
        //        var principal = context.GetOwinEnvironment()["owin.RequestUser"] as ClaimsPrincipal;
        //        if (principal != null)
        //        {
        //            var idToken = principal.FindFirst("id_token").Value;

        //            //need change to net standard
        //            self.Register<Leadsoft.Consumer.RestApiClient.IRestClientFactory>(new Leadsoft.Consumer.RestApiClient.RestClientFactory(_tokenUrl, _clientName, _clientSecret, correlationToken, idToken));
        //        }
        //    }

        //    return self;
        //}
    }
}