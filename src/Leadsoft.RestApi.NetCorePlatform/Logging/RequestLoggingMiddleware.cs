﻿using LibOwin;
using Serilog;
using System;
using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;

namespace Leadsoft.RestApi.Platform.Logging
{
    public class RequestLoggingMiddleware
    {
        public static AppFunc Middleware(AppFunc next, ILogger log)
        {
            return async env =>
            {
                var owinContext = new OwinContext(env);
                //Console.WriteLine($@"Incoming request: {owinContext.Request.Method}, {owinContext.Request.Path}, {owinContext.Request.Headers}");
                log.Information("Incoming request: {@Method}, {@Path}, {@Headers}",
                                owinContext.Request.Method,
                                owinContext.Request.Path,
                                owinContext.Request.Headers);
                await next(env).ConfigureAwait(false);
                log.Information("Outgoing response: {@StatusCode}, {@Headers}",
                                owinContext.Response.StatusCode,
                                owinContext.Response.Headers);
            };
        }
    }
}