﻿using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;
using System.Threading.Tasks;
using LibOwin;

namespace Leadsoft.RestApi.NetCorePlatform
{
    public class AuthorizationMiddleware
    {
        public static AppFunc Middleware(AppFunc next, string requiredScope)
        {
            return env =>
            {
                var ctx = new OwinContext(env);
                var principal = ctx.Request.User;
                if (principal != null && principal.HasClaim("scope", requiredScope))
                    return next(env);
                ctx.Response.StatusCode = 403;
                return Task.FromResult(0);
            };
        }
    }
}
