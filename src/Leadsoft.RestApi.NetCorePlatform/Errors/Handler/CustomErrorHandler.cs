﻿// Copyright (c) Philipp Wagner. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using Nancy;
using Nancy.Bootstrapper;
using Nancy.Responses.Negotiation;
using Leadsoft.RestApi.NetCorePlatform.Errors.Exceptions;
using Leadsoft.RestApi.NetCorePlatform.Errors.Extensions;
using Leadsoft.RestApi.NetCorePlatform.Errors.Model;
using Serilog;
using System;

namespace Leadsoft.RestApi.NetCorePlatform.Errors.Handler
{
    public static class CustomErrorHandler
    {
        //private static readonly NLog.Logger _logger = NLog.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.ToString());
        private static readonly ILogger _logger = Log.Logger;

        public static void Enable(IPipelines pipelines, IResponseNegotiator responseNegotiator, HttpServiceError defaultError, Func<NancyContext, IResponseNegotiator, Exception, Response> customHandleException = null)
        {
            if (pipelines == null)
                throw new ArgumentNullException("pipelines");

            if (responseNegotiator == null)
                throw new ArgumentNullException("responseNegotiator");

            if (defaultError == null)
                throw new ArgumentNullException("defaultError");

            pipelines.OnError += (context, exception) =>
            {
                _logger.Error(exception, "An exception occured during processing a request.");

                var response = customHandleException?.Invoke(context, responseNegotiator, exception)
                    ?? HandleGlobalResponse(context, responseNegotiator, exception, defaultError);

                response.EnableCors();

                return response;
            };
        }

        private static Response HandleGlobalResponse(NancyContext context, IResponseNegotiator responseNegotiator, Exception exception, HttpServiceError defaultError)
        {
            HttpServiceError httpServiceError = defaultError;

            if (exception != null)
            {
                if (exception is HttpServiceErrorException exceptionWithServiceError)
                {
                    httpServiceError = exceptionWithServiceError.HttpServiceError;
                }
                else
                {
                    httpServiceError.ServiceError.Details = exception.Message;
                }
            }

            var negotiator = new Negotiator(context).WithServiceError(httpServiceError);
            var response = responseNegotiator.NegotiateResponse(negotiator, context);

            return response;
        }
    }
}