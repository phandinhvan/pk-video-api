﻿using Nancy.Validation;
using Leadsoft.RestApi.NetCorePlatform.Errors.Model;
using Leadsoft.RestApi.NetCorePlatform.Validation.Nancy;

namespace Leadsoft.RestApi.NetCorePlatform.Errors.Specification.Errors
{
    public class ValidationServiceError : HttpServiceError
    {
        public ValidationServiceError(ModelValidationResult modelValidationResult)
        {
            HttpStatusCode = Nancy.HttpStatusCode.BadRequest;
            ServiceError = new ServiceErrorModel()
            {
                Code = Enums.ServiceErrorEnum.ValidationError,
                Details = modelValidationResult.GetDetailedErrorMessage()
            };
        }
    }
}