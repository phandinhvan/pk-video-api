﻿using Leadsoft.RestApi.NetCorePlatform.Errors.Model;

namespace Leadsoft.RestApi.NetCorePlatform.Errors.Specification.Errors
{
    public class GeneralServiceError : HttpServiceError
    {
        public GeneralServiceError()
        {
            HttpStatusCode = Nancy.HttpStatusCode.InternalServerError;
            ServiceError = new ServiceErrorModel()
            {
                Code = Enums.ServiceErrorEnum.InternalServerError,
                Details = "An error occured"
            };
        }
    }
}