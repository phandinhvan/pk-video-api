﻿using Nancy.Validation;
using Leadsoft.RestApi.NetCorePlatform.Errors.Exceptions;
using Leadsoft.RestApi.NetCorePlatform.Errors.Specification.Errors;
using System;

namespace Leadsoft.RestApi.NetCorePlatform.Errors.Specification.Exceptions
{
    public class ValidationServiceErrorException : HttpServiceErrorException
    {
        public ValidationServiceErrorException(ModelValidationResult modelValidationResult)
            : base(new ValidationServiceError(modelValidationResult))
        {
        }

        public ValidationServiceErrorException(ModelValidationResult modelValidationResult, string message)
            : base(new ValidationServiceError(modelValidationResult), message)
        {
        }

        public ValidationServiceErrorException(ModelValidationResult modelValidationResult, string message, Exception innerException)
            : base(new ValidationServiceError(modelValidationResult), message, innerException)
        {
        }
    }
}