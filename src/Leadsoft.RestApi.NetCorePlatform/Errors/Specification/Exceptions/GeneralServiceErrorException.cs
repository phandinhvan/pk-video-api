﻿using Leadsoft.RestApi.NetCorePlatform.Errors.Exceptions;
using Leadsoft.RestApi.NetCorePlatform.Errors.Specification.Errors;
using System;

namespace Leadsoft.RestApi.NetCorePlatform.Errors.Specification.Exceptions
{
    public class GeneralServiceErrorException : HttpServiceErrorException
    {
        public GeneralServiceErrorException()
            : base(new GeneralServiceError())
        {
        }

        public GeneralServiceErrorException(string message)
            : base(new GeneralServiceError(), message)
        {
        }

        public GeneralServiceErrorException(string message, Exception innerException)
            : base(new GeneralServiceError(), message, innerException)
        {
        }
    }
}