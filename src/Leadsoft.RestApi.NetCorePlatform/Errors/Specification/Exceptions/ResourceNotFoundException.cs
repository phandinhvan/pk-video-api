﻿using Leadsoft.RestApi.NetCorePlatform.Errors.Exceptions;
using Leadsoft.RestApi.NetCorePlatform.Errors.Model;

namespace Leadsoft.RestApi.NetCorePlatform.Errors.Specification.Exceptions
{
    public class ResourceNotFoundException : HttpServiceErrorException
    {
        public ResourceNotFoundException(string msg) 
            : base(new HttpServiceError() {
                HttpStatusCode = Nancy.HttpStatusCode.NotFound,
                ServiceError = new ServiceErrorModel()
                {
                    Code = Enums.ServiceErrorEnum.NotFound,
                    Details = msg
                }
            })
        {
        }
    }
}
