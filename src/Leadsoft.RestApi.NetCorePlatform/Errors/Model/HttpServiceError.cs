﻿using Nancy;

namespace Leadsoft.RestApi.NetCorePlatform.Errors.Model
{
    public class HttpServiceError
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public ServiceErrorModel ServiceError { get; set; }
    }
}
