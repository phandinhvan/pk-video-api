﻿using Leadsoft.RestApi.NetCorePlatform.Errors.Enums;

namespace Leadsoft.RestApi.NetCorePlatform.Errors.Model
{
    public class ServiceErrorModel
    {
        public ServiceErrorModel()
        {
        }

        public ServiceErrorModel(ServiceErrorEnum code, string details)
        {
            Code = code;
            Details = details;
        }

        public ServiceErrorEnum Code { set; get; }
        public string Details { get; set; }
    }
}