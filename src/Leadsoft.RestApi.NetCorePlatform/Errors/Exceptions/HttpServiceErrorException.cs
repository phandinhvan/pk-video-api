﻿using Leadsoft.RestApi.NetCorePlatform.Errors.Model;
using System;

namespace Leadsoft.RestApi.NetCorePlatform.Errors.Exceptions
{
    public class HttpServiceErrorException : Exception
    {
        public readonly HttpServiceError HttpServiceError;

        public HttpServiceErrorException(HttpServiceError serviceError)
            : base()
        {
            HttpServiceError = serviceError;
        }

        public HttpServiceErrorException(HttpServiceError serviceError, string message)
            : base(message)
        {
            HttpServiceError = serviceError;
        }

        public HttpServiceErrorException(HttpServiceError serviceError, string message, Exception innerException)
            : base(message, innerException)
        {
            HttpServiceError = serviceError;
        }
    }
}