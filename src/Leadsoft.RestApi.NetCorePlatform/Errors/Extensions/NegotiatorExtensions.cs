﻿using Nancy;
using Nancy.Responses.Negotiation;
using Leadsoft.RestApi.NetCorePlatform.Errors.Model;

namespace Leadsoft.RestApi.NetCorePlatform.Errors.Extensions
{
    public static class NegotiatorExtensions
    {
        public static Negotiator WithServiceError(this Negotiator negotiator, HttpServiceError httpServiceError)
        {
            return negotiator
                .WithStatusCode(httpServiceError.HttpStatusCode)
                .WithContentType("application/json")
                .WithModel(httpServiceError.ServiceError);
        }
    }
}
