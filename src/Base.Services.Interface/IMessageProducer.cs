﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Base.Services.Interface
{
    public interface IMessageProducer
    {
        Task SendAsync(object message, string sendEndpoint);
        Task SendAsync(object message, string sendEndpoint, TimeSpan ttl);

        Task PublishAsync(object message);
        Task PublishAsync(object message, TimeSpan ttl);

        IRequestClient<TRequest, TResponse> CreateRequestClient<TRequest, TResponse>(string endpoint, TimeSpan tp) where TRequest : class where TResponse : class;
    }
}
