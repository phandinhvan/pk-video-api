﻿using Base.Interface.Dto;
using System.Threading.Tasks;

namespace Base.Services.Interface.Services
{
    public interface ILDAPService
    {
        Task<TaskResult<bool>> AuthenticateLdapAsync(string directorySite, string userName, string password);
    }
}