﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace Base.Services.Interface
{
    public interface ITelegramService
    {
        Task SendChatTypingAsync(string botToken, long chatId);
        Task<Message> SendTextMessageAsync(string botToken, long chatId, string text, ParseMode parseMode = ParseMode.Default, bool disableWebPagePreview = false, bool disableNotification = false, int replyToMessageId = 0, IReplyMarkup replyMarkup = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> IsChatFromAdminOfGroupAsync(string botToken, Telegram.Bot.Types.Message message);

        Task LogTelegramAsync(string text);
    }
}
