﻿using Base.Interface.Dto;
using System.Threading.Tasks;

namespace Base.Services.Interface
{
    public interface IBaseResourceService<T>
    {
        Task<TaskResult<T>> GetAsync(object id);
        Task<PagingResult<T>> GetListAsync(PagingRequest request);
        Task<TaskResult<long>> AddAsync(T entity);

        Task<TaskResult<bool>> UpdateAsync(T entity);

        Task<TaskResult<bool>> DeleteAsync(long id);
    }
}