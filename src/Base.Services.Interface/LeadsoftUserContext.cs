﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Services.Interface
{
    public class LeadsoftUserContext
    {
        public int UserId { get; set; }
        
        public string UserName { get; set; }

        public bool IsValid()
        {
            return UserId > 0 || string.IsNullOrWhiteSpace(UserName) == false;
        }
    }
}
