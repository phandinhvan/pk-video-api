﻿using System.IO;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface ILocalFileService
    {
        Task WriteAsync(Stream stream, string absoluteFilePath);
        Task<Stream> ReadAsync(string absoluteFilePath);
        void TryDelete(string absoluteFilePath);
    }
}