﻿using System.IO;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface IFtpFileService
    {
        Task WriteAsync(Stream stream, string relativeFilePath);
        Task<Stream> ReadAsync(string relativeFilePath);
        void TryDelete(string relativeFilePath);

        Task UploadFileAsync(string localFilePath, string remoteFilePath);
    }
}