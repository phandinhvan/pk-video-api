﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Base.Interface.Dto;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface.Dto.Requests;
using System.IO;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface IObjectStorageService
    {
        Task<TaskResult<ObjectStorage>> AddAsync(AddObjectStorageRequest request);
        Task<TaskResult<ObjectStorage>> AddChunkAsync(AddChunkObjectStorageRequest request);

        Task<TaskResult<ObjectStorage>> GetObjectInfoByKeyAsync(string bucketKey, string key);

        Task<TaskResult<ObjectStorage>> GetObjectInfoByIdAsync(string bucketKey, long id);
        Task<TaskResult<Stream>> GetObjectByKeyAsync(string bucketKey, string key);

        Task<TaskResult<Stream>> GetObjectByIdAsync(string bucketKey, long id);
        Task<TaskResult<IEnumerable<ObjectStorage>>> SearchAsync(string bucketKey, string keyword);

        Task<TaskResult<int>> DeleteObjectByKeyAsync(string bucketKey, string key);
    }
}