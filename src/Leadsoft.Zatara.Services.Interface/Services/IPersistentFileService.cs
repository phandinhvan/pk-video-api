﻿using System.IO;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface IPersistentFileService
    {
        string GetAbsoluteFilePath(string bucketKey, string relativeFilePath);
        string GetRelativeFilePath(string bucketKey, string relativeFilePath);
        Task WriteAsync(Stream stream, string bucketKey, string relativeFilePath);
        Task<Stream> ReadAsync(string bucketKey, string relativeFilePath);
        Task<Stream> ReadAsync(string absoluteFilePath);
        void TryDelete(string bucketKey, string relativeFilePath);
    }
}