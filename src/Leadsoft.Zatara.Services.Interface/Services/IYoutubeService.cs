﻿using System.Threading.Tasks;
using Base.Interface.Dto;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface.Dto;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface IYoutubeService
    {
        Task<TaskResult<YoutubeVideoInfo>> GetVideoInfoAsync(string youtubeLink);
    }
}