﻿using System.Threading.Tasks;
using Base.Interface.Dto;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface IObjectBucketService : IBaseResourceService<ObjectBucket>
    {
        Task<TaskResult<ObjectBucket>> GetObjectBucketByKeyAsync(string bucketKey);
        Task<ObjectBucket> CheckExistAndCreateBucketAsync(string bucketKey, bool isPublic = true);
    }
}