﻿using Base.Services.Interface;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface IDealService : IBaseResourceService<Deal>
    {
    }
}