﻿using Base.Interface.Dto;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;
using System.IO;
using System.Threading.Tasks;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface IPKFileService
    {
        Task<PagingResult<PKFileDto>> GetListPagingAsync(GetVideoPagingRequest request);
        Task<PagingResult<PKFileComment>> GetListCommentPagingAsync(GetCommentPagingRequest request);


        Task<TaskResult<PKFileDto>> AddChunkObjectAsync(AddChunkObjectStorageRequest request);
        Task<TaskResult<Stream>> GetFileDataByIdAsync(string fileId);
        Task<TaskResult<PKFile>> GetFileInfoByIdAsync(string fileId);
        Task<TaskResult<PKFileDto>> GetDtoAsync(string fileId);
        Task<TaskResult<string>> DeleteAsync(string fileId);
        Task<TaskResult<string>> UpdateAsync(UpdateFileRequest request);
        Task<TaskResult<PKFileDto>> ShareLinkAsync(ShareLinkRequest request);

        Task<PagingResult<ManageStyleDto>> GetListManageStylePagingAsync(GetVideoManageStylePagingRequest request);
        Task<PagingResult<ManageStyleVideoDto>> GetListVideoManageStyleAsync(GetListVideoManageStylePagingRequest request);
        Task<PagingResult<SetupDto>> GetListProjectSetupAsync(GetListProjectSetupPagingRequest request);
        Task<PagingResult<SetupDto>> GetListItemProjectAsync(GetListItemProjectNamePagingResult request);
        Task<TaskResult<PKFileDto>> AddMoreVideoAsync(AddChunkObjectStorageRequest request);
        Task<PagingResult<PKFileDto>> GetNewVideoPagingAsync();


    }
}