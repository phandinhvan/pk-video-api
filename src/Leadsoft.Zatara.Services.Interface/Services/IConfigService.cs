﻿using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface.Dto;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface IConfigService
    {
        bool UsingFtpStorageOnly {get;}
        string GetObjectStorageRootPath();
        string GetTempUploadFolder();
        string GetFFMPEGPath();
        FtpSetting GetFtpSetting();
    }
}