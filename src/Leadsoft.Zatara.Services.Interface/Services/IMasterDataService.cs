﻿using System.Threading.Tasks;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;
using Leadsoft.Zatara.Interface.Dto.Requests;
using Leadsoft.Zatara.Interface.Dto.Responses;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface IMasterDataService
    {
        Task<MasterDataDto> GetMasterDataAsync(GetMasterDataRequest request);
    }
}