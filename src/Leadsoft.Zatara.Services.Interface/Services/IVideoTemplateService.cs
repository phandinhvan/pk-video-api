﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Base.Interface.Dto;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface IVideoTemplateService
    {
        List<VideoTemplateItem> LoadDesignerTemplateFromFile(string templateFilePath);
        Task<TaskResult<string>> UploadFileAsync(long typeId, string templateFilePath);
    }
}