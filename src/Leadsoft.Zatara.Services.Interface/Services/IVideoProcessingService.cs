﻿using System.Threading.Tasks;
using Base.Services.Interface;
using Leadsoft.Zatara.Entities;

namespace Leadsoft.Zatara.Services.Interface
{
    public interface IVideoProcessingService
    {
        void ConvertVideo(string inputFilePath, string outputFilePath);
    }
}