﻿using MySql.Data.MySqlClient;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Base.Infrastructure.Data.Dapper
{
    public abstract class OracleConnectionFactory : IDisposable
    {
        private string _connectionString;
        public string ConnectionString { get { return _connectionString; } }

        public OracleConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public virtual IDbConnection GetDbConnection()
        {
            var con = new OracleConnection(_connectionString);
            return con; 
        }

        public void Dispose()
        {
        }
    }
}