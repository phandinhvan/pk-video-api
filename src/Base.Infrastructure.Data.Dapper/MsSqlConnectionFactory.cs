﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Base.Infrastructure.Data.Dapper
{
    public abstract class MsSqlConnectionFactory : IDisposable
    {
        private string _connectionString;
        public string ConnectionString { get { return _connectionString; } }

        public MsSqlConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public virtual IDbConnection GetDbConnection()
        {
            return new SqlConnection(_connectionString);
        }

        public void Dispose()
        {
        }
    }
}