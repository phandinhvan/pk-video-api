﻿using Base.Interface;
using Dapper;
using Dommel;
using Leadsoft.FaultHandling;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using static Dommel.DommelMapper;

namespace Base.Infrastructure.Data.Dapper
{
    public partial class DapperGenericRepository<T> : IGenericRepository<T> where T : class
    {
        private IConnectionFactory _connectionFactory;
        protected string oracleDbLinkSuffix = "@salary";//we can change it to empy for default
        protected string _tableName;

        public DapperGenericRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
            _tableName = Resolvers.Table(typeof(T));
        }

        protected IDbConnection GetDbConnection()
        {
            return _connectionFactory.GetDbConnection();
        }

        public virtual async Task<T> AddAsync(T obj)
        {
            using (var connection = GetDbConnection())
            {
                object id = null;
                var connectionName = connection.GetType().Name.ToLower();
                if (connectionName.Contains("oracle"))
                    id = await connection.InsertOracleAsync<T>(obj);
                else
                    id = await connection.InsertAsync<T>(obj);

                return await FindByIdAsync(id);
            }
        }

        public virtual async Task<bool> DeleteByIdAsync(object id)
        {
            //var obj = await FindByIdAsync(id);
            //if (obj == null) return false;

            using (var connection = GetDbConnection())
            {
                return await RetryPolicy.DoRetryAsync<bool>(async () =>
                {
                    return await connection.DeleteAsync<T>(id);
                }, 3);
            }
        }

        public virtual async Task<bool> UpdateAsync(T obj)
        {
            using (var connection = GetDbConnection())
            {
                return await RetryPolicy.DoRetryAsync<bool>(async () =>
                {
                    return await connection.UpdateAsync<T>(obj);
                }, 3);
            }
        }

        public virtual async Task<T> FindByIdAsync(object id)
        {
            using (var connection = GetDbConnection())
            {
                return await RetryPolicy.DoRetryAsync<T>(async () =>
                {
                    return await connection.GetAsync<T>(id);
                }, 3);
                ///return await connection.GetAsync<T>(id);
            }
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            using (var connection = GetDbConnection())
            {
                return await connection.GetAllAsync<T>();
            }
        }

        public async Task<IEnumerable<TResult>> QueryAsync<TResult>(string sql, object parameter = null)
        {
            using (var connection = GetDbConnection())
            {
                return await RetryPolicy.DoRetryAsync<IEnumerable<TResult>>(async () =>
                {
                    return await connection.QueryAsync<TResult>(sql, parameter);
                }, 3);
            }
        }

        public async Task<TResult> QueryFirstOrDefaultAsync<TResult>(string sql, object parameter = null)
        {
            using (var connection = GetDbConnection())
            {
                return await RetryPolicy.DoRetryAsync<TResult>(async () =>
                {
                    return await connection.QueryFirstOrDefaultAsync<TResult>(sql, parameter);
                }, 3);
            }
        }

        public async Task<int> ExecuteAsync(string sql, object parameter = null)
        {
            using (var connection = GetDbConnection())
            {
                return await RetryPolicy.DoRetryAsync<int>(async () =>
                {
                    return await connection.ExecuteAsync(sql, parameter);
                }, 3);
            }
        }

        public async Task<T> GetObjectByNoAsync(string noValue, string noColumnName = "NO")
        {
            return await QueryFirstOrDefaultAsync<T>($@"SELECT * FROM {_tableName} WHERE {noColumnName} = '{noValue}'");
        }

        public async Task<TResult> ExecuteScalarAsync<TResult>(string sql, object parameter = null)
        {
            using (var connection = GetDbConnection())
            {
                return await RetryPolicy.DoRetryAsync<TResult>(async () =>
                {
                    return await connection.ExecuteScalarAsync<TResult>(sql, parameter);
                }, 3);
            }
        }
    }
}