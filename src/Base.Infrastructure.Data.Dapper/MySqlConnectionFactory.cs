﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Base.Infrastructure.Data.Dapper
{
    public abstract class MySqlConnectionFactory : IDisposable
    {
        private string _connectionString;
        public string ConnectionString { get { return _connectionString; } }

        public MySqlConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public virtual IDbConnection GetDbConnection()
        {
            var con = new MySqlConnection(_connectionString);
            return con; 
        }

        public void Dispose()
        {
        }
    }
}