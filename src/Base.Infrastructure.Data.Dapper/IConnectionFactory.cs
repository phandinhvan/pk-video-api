﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Base.Infrastructure.Data.Dapper
{
    public interface IConnectionFactory
    {
        string ConnectionString { get; }

        IDbConnection GetDbConnection();
    }
}
