﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gandalf.Common
{
    public class StreamHelper
    {
        public static async Task<byte[]> GetBytesAsync(Stream stream)
        {
            using(var memoryStream = new MemoryStream())
            {
                stream.Seek(0, SeekOrigin.Begin);
                await stream.CopyToAsync(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public static async Task SaveStreamToFileAsync(Stream stream, string filePath)
        {
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            using (var fileStream = File.Create(filePath))
            {
                await stream.CopyToAsync(fileStream);
                fileStream.Close();
            }
        }
    }
    
}
