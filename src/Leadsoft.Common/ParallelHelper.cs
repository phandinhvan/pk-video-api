﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leadsoft.Common
{
    public class ParallelHelper
    {
        public static async Task<IEnumerable<TOut>> ForEachAsync<Tin, TOut>(IEnumerable<Tin> source, Func<Tin, Task<TOut>> func)
        {
            var lstTask = new List<Task<TOut>>();
            foreach (var item in source)
            {
                lstTask.Add(func(item));
            }

            await Task.WhenAll(lstTask);

            return lstTask.Where(t => t.Result != null).Select(t => t.Result);
        }

        public static async Task ForEachAsync<Tin>(IEnumerable<Tin> source, Func<Tin, Task> func)
        {
            var lstTask = new List<Task>();
            foreach (var item in source)
            {
                lstTask.Add(func(item));
            }

            await Task.WhenAll(lstTask);
        }
    }
}
