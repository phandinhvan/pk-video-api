﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gandalf.Common
{
    public class ConverHelper
    {
        public static decimal StringToDecimal(string text)
        {
            decimal value = 0;
            if (string.IsNullOrWhiteSpace(text) || text.ToUpper().Contains("NULL"))
                return value;

            decimal.TryParse(text, out value);

            return value;
        }
    }
}
