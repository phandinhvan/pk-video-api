﻿using FastMember;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Leadsoft.Common
{
    public class ReflectionHelper
    {
        public static object GetPropertyValue(object obj, string propName)
        {
            var wrapped = ObjectAccessor.Create(obj);
            return wrapped[propName];
        }

        public static object SetPropertyValue(object obj, string propName, object propValue)
        {
            var wrapped = ObjectAccessor.Create(obj);
            return wrapped[propName] = propValue;
        }

        public static void FormatObjectFieldCurrency(object obj)
        {
            var wrapped = ObjectAccessor.Create(obj);

            var type = obj.GetType();
            var lstProps = type.GetProperties();
            var dicPropName = new Dictionary<string, PropertyInfo>(StringComparer.OrdinalIgnoreCase);
            foreach (var prop in lstProps)
            {
                if (dicPropName.ContainsKey(prop.Name) == false)
                    dicPropName.Add(prop.Name, prop);
            }
            foreach (var prop in lstProps)
            {
                if(prop.PropertyType == typeof(decimal))
                {
                        var textPropName = prop.Name + "_TEXT";
                        if (dicPropName.ContainsKey(textPropName))
                            wrapped[textPropName] = NumberHelper.NumberToString((decimal)wrapped[prop.Name], 0);
                }
            }
        }
    }
}