﻿namespace Leadsoft.Common
{
    public class ConvertHelper
    {
        public static bool GetBool(string boolText, bool defaultValue = false)
        {
            var boolValue = defaultValue;

            bool.TryParse(boolText, out boolValue);

            return boolValue;
        }

        public static int GetInt(string intText, int defaultValue = 0)
        {
            var intValue = defaultValue;

            int.TryParse(intText, out intValue);

            return intValue;
        }
    }
}