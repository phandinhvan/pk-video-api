namespace Leadsoft.Common
{
    public class NumberHelper
    {
        private static readonly System.Random random = new System.Random();

        public static decimal RandomNumberBetween(decimal minValue, decimal maxValue)
        {
            var next = (decimal)random.NextDouble();

            return minValue + (next * (maxValue - minValue));
        }

        public static decimal GetMin(params decimal[] numberlist)
        {
            decimal dMin = decimal.MaxValue;

            foreach (var num in numberlist)
            {
                if (num < dMin && num > 0)
                    dMin = num;
            }

            if (dMin == decimal.MaxValue)
                dMin = 0;

            return dMin;


        }

        public static decimal GetMax(params decimal[] numberlist)
        {
            decimal dMax = 0;

            foreach (var num in numberlist)
            {
                if (num > dMax && num > 0)
                    dMax = num;
            }

            return dMax;
        }

        public static decimal GetMiddle(decimal price1, decimal price2, decimal price3)
        {
            decimal dMin = GetMin(price1, price2, price3);
            decimal dMax = GetMin(price1, price2, price3);

            if (price1 != dMin && price1 != dMax)
                return price1;

            if (price2 != dMin && price2 != dMax)
                return price2;

            if (price3 != dMin && price3 != dMax)
                return price3;

            return 0;
        }

        public static string NumberToString(decimal number, int numberDecimal)
        {
            string text = number.ToString($@"N{numberDecimal}");
            text = text.Replace(".", "Z");
            text = text.Replace(",", ".");
            text = text.Replace("Z", ",");
            if (text.Contains(","))
                text = text.TrimEnd('0');

            return text.TrimEnd(',');
        }

        public static int CalcPageCount(int totalRecord, int pageSize)
        {
            int pageCount = 1;
            if (totalRecord == 0 || pageSize == 0) return pageCount;

            if (totalRecord % pageSize == 0)
                pageCount = totalRecord / pageSize;
            else
                pageCount = totalRecord / pageSize + 1;

            return pageCount;
        }

        public static int GetPagingStartIndex(int pageIndex, int pageSize)
        {
            int startIndex = 0;
            if (pageIndex <= 0)
                pageIndex = 1;

            startIndex = (pageIndex - 1) * pageSize + 1;

            return startIndex;
        }

        public static int GetPagingEndIndex(int pageIndex, int pageSize)
        {
            return pageIndex * pageSize;
        }
    }
}