﻿using System;

namespace Leadsoft.Common
{
    public class Constants
    {
        /// <summary>
        /// { ";", "," }
        /// </summary>
        public static readonly string[] SEPARATOR_CHARACTERS = { ";", "," };

        public const int DEFAULT_PAGE_SIZE = 20;

        public const string FORMAT_DATE_EN = "MM/dd/yyyy";

        public const string DATE_FORMAT = "yyyy-MM-dd";

        public const string CURRENCY_SYMBOL_EURO = "€";

        public const string AREA_SYMBOL = "m²";

        public class RegexPattern
        {
            public const string Sleeping = @"(\s*((\d{1,9}\s+){0,1}))";
        }

        public class CacheName
        {
            public const string NoCache = "NoCache";

            public const string MemoryCache = "MemoryCache";

            public const string StaticCache = "StaticCache";
        }
    }
}
