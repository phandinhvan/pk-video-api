﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Leadsoft.Common
{
    public class StringHelper
    {
        public static IEnumerable<string> SplitTextToMultiPart(string text, int numberOfLine)
        {
            var arrLines = SplitTextToMultiLine(text);

            List<string> lstParts = new List<string>();

            int iLineIndex = 1;
            StringBuilder builder = new StringBuilder();
            foreach (var line in arrLines)
            {
                if (iLineIndex <= numberOfLine && iLineIndex <= arrLines.Length)
                {
                    iLineIndex++;
                    builder.AppendLine(line);
                }
                else
                {
                    iLineIndex = 1;
                    lstParts.Add(builder.ToString().Trim());
                    builder = new StringBuilder();
                    builder.AppendLine(line);
                }
            }

            string lastPart = builder.ToString();
            if (string.IsNullOrWhiteSpace(lastPart) == false)
                lstParts.Add(lastPart);

            for (int i = 0; i < lstParts.Count - 1; i++)
            {
                builder = new StringBuilder();
                builder.AppendLine(lstParts[i]);
                builder.Append("...");

                lstParts[i] = builder.ToString();
            }

            return lstParts;
        }

        public static string[] SplitTextToMultiLine(string text)
        {
            return text.Trim().Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] SplitTextByCompleteNewLine(string text)
        {
            return text.Split(new string[] { "\r\n\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string RemoveIndexAtBeginString(string text)
        {
            text = text.Trim();
            int iFirstIndexOfDot = text.IndexOf('.');
            if(iFirstIndexOfDot > 0)
            {
                text = text.Substring(iFirstIndexOfDot + 1, text.Length - iFirstIndexOfDot - 1);
            }

            return text.Trim();
        }

        public static string RemoveMarkdownChar(string text)
        {
            return text.Replace("_", " ").Replace("*", " ").Replace("[", " ").Replace("]", " ");
        }

        private static Random random = new Random();
        public static string GenerateRandomString(int length)
        {
            const string chars = "abdefghijklmnopqrstuvwxyz0235678";//remove 1,4,9
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string ReverseText(string text, char split)
        {
            var arr = text.Split(new char[] { split }, StringSplitOptions.RemoveEmptyEntries);

            return $@"{arr[1]}{split}{arr[0]}";
        }

        public static string CreateMessText(string text)
        {
            if (text.Length <= 10) return text;

            string firstPart = text.Substring(0, 3);
            string lastPart = text.Substring(text.Length - 3, 3);

            return $@"{firstPart}***{lastPart}";
        }

        public static string ConvertToBase64(byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
        }

        public static Stream Base64ToStream(string base64)
        {
            var bytes =  Convert.FromBase64String(base64);
            return new MemoryStream(bytes);
        } 

        public static string GetStringFromStream(System.IO.Stream stream)
        {
            using (StreamReader reader = new System.IO.StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public static string RemoveNonPrinableCharacters(string text)
        {
            return Regex.Replace(text, @"\p{C}+", string.Empty);
        }
    }
}
