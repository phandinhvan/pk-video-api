﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Leadsoft.Common
{
    public class DateTimeHelper
    {
        public static DateTime GetPreviousDate(DateTime dt)
        {
            return dt.AddDays(-1);
        }

        public static DateTime GetLastDatePreviousWeek(DateTime dt)
        {
            DateTime dtPrevious = dt.AddDays(-7);
            return DateTimeExtensions.GeneralDateTimeExtensions.LastDayOfWeek(dtPrevious, DayOfWeek.Sunday);
        }

        public static DateTime GetStartOfDay(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 1);
        }

        public static DateTime GetEndOfDay(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59);
        }

        public static DateTime StartOfMonth(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, 1, 1, 1, 1);
        }

        public static DateTime StartOfMonth(int year, int month)
        {
            return new DateTime(year, month, 1, 1, 1, 1);
        }

        public static DateTime EndOfMonth(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month,DateTime.DaysInMonth(dt.Year, dt.Month), 23, 29, 59);
        }

        public static string GetDayOfWeekVi(DateTime dt)
        {
            var dayOfWeekVi = "";
            var dayOfWeekEn = dt.DayOfWeek;
            switch (dayOfWeekEn)
            {
                case DayOfWeek.Friday:
                    dayOfWeekVi = "Thứ 6";
                    break;
                case DayOfWeek.Monday:
                    dayOfWeekVi = "Thứ hai";
                    break;
                case DayOfWeek.Saturday:
                    dayOfWeekVi = "Thứ bảy";
                    break;
                case DayOfWeek.Sunday:
                    dayOfWeekVi = "Chủ nhật";
                    break;
                case DayOfWeek.Thursday:
                    dayOfWeekVi = "Thứ năm";
                    break;
                case DayOfWeek.Tuesday:
                    dayOfWeekVi = "Thứ ba";
                    break;
                case DayOfWeek.Wednesday:
                    dayOfWeekVi = "Thứ tư";
                    break;
                default:
                    break;
            }

            return dayOfWeekVi;

        }

        public static string GetShortDayOfWeekVi(DateTime dt)
        {
            var dayOfWeekVi = "";
            var dayOfWeekEn = dt.DayOfWeek;
            switch (dayOfWeekEn)
            {
                case DayOfWeek.Friday:
                    dayOfWeekVi = "T6";
                    break;
                case DayOfWeek.Monday:
                    dayOfWeekVi = "T2";
                    break;
                case DayOfWeek.Saturday:
                    dayOfWeekVi = "T7";
                    break;
                case DayOfWeek.Sunday:
                    dayOfWeekVi = "CN";
                    break;
                case DayOfWeek.Thursday:
                    dayOfWeekVi = "T5";
                    break;
                case DayOfWeek.Tuesday:
                    dayOfWeekVi = "T3";
                    break;
                case DayOfWeek.Wednesday:
                    dayOfWeekVi = "T4";
                    break;
                default:
                    break;
            }

            return dayOfWeekVi;

        }


    }
}
