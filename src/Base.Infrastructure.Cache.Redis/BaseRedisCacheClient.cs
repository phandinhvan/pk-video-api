﻿using StackExchange.Redis;

namespace Base.Infrastructure.Cache.Redis
{
    public abstract class BaseRedisCacheClient
    {
        private readonly ConnectionMultiplexer _redis;

        public BaseRedisCacheClient(ConnectionMultiplexer redis)
        {
            _redis = redis;
        }

        protected virtual IDatabase GetDatabase()
        {
            return GetDatabase(0);
        }

        protected virtual IDatabase GetDatabase(int dbIndex)
        {
            return _redis.GetDatabase(dbIndex);
        }
    }
}