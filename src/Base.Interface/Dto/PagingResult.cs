﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Interface.Dto
{
    public class PagingResult<T>: TaskResult<IEnumerable<T>>
    {
        public int TotalRecord { get; set; }
        public int PageSize { get; set; }
        public int TotalPageCount { get; set; }
        public int CurrentPageIndex { get; set; }


        //
        public PagingResult<T> Clone()
        {
            return new PagingResult<T>()
            {
                CurrentPageIndex = this.CurrentPageIndex,
                IsSuccess = this.IsSuccess,
                Log = this.Log,
                PageSize = this.PageSize,
                Result = this.Result,
                TotalPageCount = this.TotalPageCount,
                TotalRecord = this.TotalRecord,
                Code = this.Code
            };
        }
    }
}
