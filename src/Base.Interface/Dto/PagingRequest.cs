﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Interface.Dto
{
    public class PagingRequest
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }

        public T CloneTo<T>() where T : PagingRequest, new()
        {
            var newObj = new T();
            newObj.PageIndex = this.PageIndex;
            newObj.PageSize = this.PageSize;

            return newObj;
        }

    }
}
