﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Interface.Dto
{
    public class FailTaskResult<T> : TaskResult<T>
    {
        public FailTaskResult(string log)
        {
            IsSuccess = false;
            Log = log;
        }
    }
}
