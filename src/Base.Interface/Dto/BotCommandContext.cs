﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Interface.Dto
{
    public class BotCommandContext
    {
        public string TelegramMessageText { get; set; }
        public int TelegramMessageId { get; set; }
        public int TelegramUserId { get; set; }
        public long TelegramChatId { get; set; }
        public string TelegramUserName { get; set; }
        public bool IsGroup { get; set; }
        public string TelegramBotToken { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
