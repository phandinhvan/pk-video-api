﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Interface.Dto
{
    public class WithdrawDto
    {
        public string Asset { get; set; }
        public string Address { get; set; }
        public decimal Amount { get; set; }
        public string TransactionId { get; set; }
        public DateTime ApplyTime { get; set; }
        public decimal TransactionCost { get; set; }
    }
}
