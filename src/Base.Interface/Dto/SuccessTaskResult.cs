﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Interface.Dto
{
    public class SuccessTaskResult<T> : TaskResult<T>
    {

        public SuccessTaskResult()
        {
            IsSuccess = true;
        }
        public SuccessTaskResult(T result)
        {
            IsSuccess = true;
            Result = result;
        }
    }
}
