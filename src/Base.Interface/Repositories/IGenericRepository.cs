﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Base.Interface
{
    public interface IGenericRepository<T>
    {
        Task<T> AddAsync(T obj);

        Task<bool> DeleteByIdAsync(object id);

        Task<bool> UpdateAsync(T obj);

        Task<T> FindByIdAsync(object id);

        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetObjectByNoAsync(string noValue, string noColumnName = "NO");
    }
}