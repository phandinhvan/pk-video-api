﻿using Omnicasa.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gandalf.Interface.Dto.QueryParameter
{
    public class QueryParameter : BaseParameter
    {
        public List<ColumnMap> Fields { get; set; }

        public PagingParameter Paging { get; set; }

        public List<OrderByParameter> OrderBy { set; get; }

        #region Common

        public bool HasPaging() => Paging != null && Paging.PageSize != int.MaxValue;

        //public List<string> GetJoinKeys()
        //{
        //    var allFields = new List<ColumnMap>(Fields.Where(a => !string.IsNullOrWhiteSpace(a.JoinKeys)));
        //    if (OrderBy != null)
        //    {
        //        allFields.AddRange(OrderBy.Where(a => a.Field != null && !string.IsNullOrWhiteSpace(a.Field.JoinKeys)).Select(a => a.Field));
        //    }

        //    return allFields                
        //        .SelectMany(a => a.JoinKeys.Split(Constants.SEPARATOR_CHARACTERS, StringSplitOptions.RemoveEmptyEntries))
        //        .Where(a => !string.IsNullOrWhiteSpace(a))
        //        .Select(a => a.Trim())
        //        .Distinct()
        //        .ToList();
        //}

        #endregion

    }
}
