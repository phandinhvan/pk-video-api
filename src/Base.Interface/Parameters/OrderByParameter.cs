﻿namespace Gandalf.Interface.Dto.QueryParameter
{
    public class OrderByParameter
    {
        public OrderByType Type { set; get; }
        public ColumnMap Field { set; get; }

        public OrderByParameter()
        {
        }

        public OrderByParameter(OrderByType direction) 
            : this()
        {
            Type = direction;
        }

        public OrderByParameter(OrderByType direction, string name) 
            : this(direction)
        {
            Field = new ColumnMap(name);
        }
    }

    public enum OrderByType
    {
        ASC = 0,
        DESC = 1
    }
}
