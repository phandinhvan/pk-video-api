﻿using Castle.DynamicProxy;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Leadsoft.Common.Interceptors
{
    public class MethodCallInterceptor : IInterceptor
    {
        private readonly ILogger _logger;
        public MethodCallInterceptor(ILogger logger)
        {
            _logger = logger;
        }
        public void Intercept(IInvocation invocation)
        {
            string guid = Guid.NewGuid().ToString("N");

            Type targetType = invocation.InvocationTarget.GetType();

            _logger.ForContext(targetType).Information("INVOKE {guid} {MethodName} with parameters {@arg}", invocation.Method.Name, guid, invocation.Arguments);
            Stopwatch watch = new Stopwatch();
            watch.Start();
            invocation.Proceed();
            watch.Stop();

            _logger.ForContext(targetType).Information("INVOKE {guid} DONE {MethodName} with result was {$Result}. ExecutedTime {Duration} ms", invocation.Method.Name, guid, invocation.ReturnValue, watch.ElapsedMilliseconds);
        }
    }
}
